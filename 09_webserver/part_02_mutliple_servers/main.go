// HTTP server example
package main

import (
	"fmt"
	"log"
	"net/http"
)

func server1func(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Running First Server")
}

func server2func(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Running Second Server")
}

func main() {
	// Show on console the application stated
	log.Println("Server started on: http://localhost:9000")
	main_server := http.NewServeMux()

	//Creating sub-domain
	server1 := http.NewServeMux()
	server1.HandleFunc("/", server1func)

	server2 := http.NewServeMux()
	server2.HandleFunc("/", server2func)
	//Running First Server
	go func() {
		log.Println("Server started on: http://localhost:9001")
		http.ListenAndServe("localhost:9001", server1)
	}()

	//Running Second Server
	go func() {
		log.Println("Server started on: http://localhost:9002")
		http.ListenAndServe("localhost:9002", server2)
	}()

	//Running Main Server
	http.ListenAndServe("localhost:9000", main_server)
}

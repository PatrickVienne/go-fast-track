package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	_ "net/http/pprof" // this import has side effects, adding debug pprof endpoints
	"sync"
	"text/template"
	"time"

	"github.com/gorilla/mux"
)

// Article ...
type Article struct {
	ID     string `json:"Id"`
	Title  string `json:"Title"`
	Author string `json:"author"`
	Link   string `json:"link"`
}

// Articles ...
var (
	Articles []Article
	mu       sync.Mutex
)

func homePage(w http.ResponseWriter, r *http.Request) {
	t := template.Must(template.ParseFiles("homepage.gohtml"))

	var body bytes.Buffer
	if err := t.Execute(&body, Articles); err != nil {
		log.Fatal(err)
	}
	t.Execute(w, Articles)
}

func returnSingleArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	key := vars["id"]

	for _, article := range Articles {
		if article.ID == key {
			json.NewEncoder(w).Encode(article)
		}
	}
}

// Step 3 CREATE
func createNewArticle(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	time.Sleep(10 * time.Second)
	var article Article
	json.Unmarshal(reqBody, &article)
	mu.Lock()
	Articles = append(Articles, article)
	mu.Unlock()

	json.NewEncoder(w).Encode(article)
}

// Step 3 DELETE
func deleteArticle(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	mu.Lock()
	for index, article := range Articles {
		if article.ID == id {
			Articles = append(Articles[:index], Articles[index+1:]...)
		}
	}
	mu.Unlock()

}

// with mux
func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/articles", returnAllArticles)
	myRouter.HandleFunc("/article/{id}", returnSingleArticle)

	// Step 3 CREATE & DELETE
	// curl -X POST -H "Content-Type: application/json" -d "@postArticle.json" http://localhost:8000/article
	myRouter.HandleFunc("/article", createNewArticle).Methods("POST")
	myRouter.HandleFunc("/article/{id}", deleteArticle).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":8000", myRouter))
}

func returnAllArticles(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Endpoint Hit: returnAllArticles")
	json.NewEncoder(w).Encode(Articles)
}

func main() {
	Articles = []Article{
		Article{Title: "Python Intermediate and Advanced 101",
			Author: "Arkaprabha Majumdar",
			Link:   "https://www.amazon.com/dp/B089KVK23P"},
		Article{Title: "R programming Advanced",
			Author: "Arkaprabha Majumdar",
			Link:   "https://www.amazon.com/dp/B089WH12CR"},
		Article{Title: "R programming Fundamentals",
			Author: "Arkaprabha Majumdar",
			Link:   "https://www.amazon.com/dp/B089S58WWG"},
	}

	// start profiling endpoint
	go func() {
		log.Fatal(http.ListenAndServe("localhost:6060", nil))
	}()

	handleRequests()
}

// send data with:
// curl -X POST -H "Content-Type: application/json" -d "@postArticle.json" http://localhost:8000/article

// check profiling with:
// go tool pprof http://localhost:6060/debug/pprof/heap
// go tool pprof http://localhost:6060/debug/pprof/profile?seconds=30
// go tool pprof http://localhost:6060/debug/pprof/block
// go tool pprof http://localhost:6060/debug/pprof/mutex

// get trace and analyse work done by goroutines
// curl -o trace.out http://localhost:6060/debug/pprof/trace?seconds=30
// go tool trace trace.out

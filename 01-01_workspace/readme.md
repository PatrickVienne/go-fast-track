Example by the official docs on:

https://go.dev/doc/tutorial/workspaces

```sh
$ mkdir hello
$ cd hello
$ go mod init example.com/hello
$ go get golang.org/x/example
$ go run example.com/hello
$ cd ..
$ go run example.com/hello  # this will not run, due to missing go.mod file
$ go work init ./hello
$ go run example.com/hello
```
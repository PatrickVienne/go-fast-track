package fizzbuzz

import (
	"fmt"
	"reflect"
	"testing"
)

var (
	in1a = 15
	out1 = []string{"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"}
)

func TestCase1(t *testing.T) {
	res := fizzBuzz(in1a)
	if !reflect.DeepEqual(res, out1) {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}

func BenchmarkFizzbuzz(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000412_fizzbuzz
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkFizzbuzz-8   	  404964	      2571 ns/op	     502 B/op	       8 allocs/op
		PASS
		ok  	000412_fizzbuzz	1.894s

	*/
	for n := 0; n < b.N; n++ {
		fizzBuzz(in1a)
	}
}

func BenchmarkFastbuzz(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000412_fizzbuzz
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkFastbuzz-8   	  267660	      4246 ns/op	     248 B/op	       2 allocs/op
		PASS
		ok  	000412_fizzbuzz	2.829s
	*/
	for n := 0; n < b.N; n++ {
		fastBuzz(in1a)
	}
}

package count_primes

import (
	"os"
	"runtime/trace"
)

/*
Given an integer n, return the number of prime numbers that are strictly less than n.

Example 1:

Input: n = 10
Output: 4
Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.
Example 2:

Input: n = 0
Output: 0
Example 3:

Input: n = 1
Output: 0

Constraints:

0 <= n <= 5 * 10**6
*/
func countMore() string {
	end := ""
	for i := 0; i < 1e6; i++ {
		end += "more data... "
	}
	return end
}

func countPrimes(n int) int {
	f, _ := os.Create("trace.out")
	trace.Start(f)
	defer f.Close()

	defer trace.Stop()
	return countPrimesJob(n)
}
func countPrimesJob(n int) int {

	if n == 0 || n == 1 || n == 2 {
		return 0
	}
	excluded := make([]bool, n)
	excluded[0] = true
	excluded[1] = true
	found := 0
	for idx := range excluded {
		go countMore()
		if !excluded[idx] {
			found++
			for i := idx; i*idx < n; i++ {
				excluded[i*idx] = true
			}
		}
	}
	return found
}

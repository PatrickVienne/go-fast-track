package count_primes

import (
	"fmt"
	"testing"
)

var (
	in1a = 1000
	out1 = 4 // 2,3,5,7
)

func TestCase1(t *testing.T) {
	res := countPrimes(in1a)
	if res != out1 {
		t.Fatalf(fmt.Sprintf("Input: %v, Expected: %v, but was: %v", in1a, out1, res))
	}
}

func BenchmarkCountPrimes(b *testing.B) {
	/*
		goos: windows
		goarch: amd64
		pkg: 000412_fizzbuzz
		cpu: Intel(R) Core(TM) i7-8550U CPU @ 1.80GHz
		BenchmarkFizzbuzz-8   	  404964	      2571 ns/op	     502 B/op	       8 allocs/op
		PASS
		ok  	000412_fizzbuzz	1.894s

	*/
	for n := 0; n < b.N; n++ {
		countPrimes(in1a)
	}
}

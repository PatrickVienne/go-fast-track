package server

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"text/template"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/PatrickVienne/go-advanced/13_webserver_gomigrate/models"
	"go.uber.org/zap"
)

const (
	PORT = 8000
)

func trackDuration(name string, start time.Time) {
	zap.S().Infof("execution '%s' took '%s'", name, time.Since(start))
}

func (s Server) getTemplateData(ctx context.Context) models.TemplateData {
	books := s.DBApi.GetBooks(ctx)

	return models.TemplateData{
		NavbarData: models.NavbarData{VERSION: "1.0.0"},
		Books:      books,
	}
}

func (s Server) helloHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello Gophers!")
}

func (s Server) indexHandler(w http.ResponseWriter, r *http.Request) {
	defer trackDuration("indexHandler", time.Now())
	tpl, err := template.ParseGlob("templates/*.html")

	if err != nil {
		zap.S().Warnf("FATAL: could not find templates. %v", err)
		bytes := []byte(`could not find data for webpage`)
		w.Write(bytes)
		return
	}

	data := s.getTemplateData(r.Context())
	err = tpl.ExecuteTemplate(w, "mainpage", data)

	if err != nil {
		zap.S().Warnf("FATAL: could not find data for webpage. %v", err)
		bytes := []byte(`could not find data for webpage`)
		w.Write(bytes)
		return
	}
}

type DBApi interface {
	GetBooks(context.Context) []models.Book
	CreateBook(context.Context, models.Book) models.Book
}

type Server struct {
	DBApi
}

func NewServer(db DBApi) Server {
	return Server{DBApi: db}
}

func (s Server) NewRouter() *mux.Router {
	// define router
	router := mux.NewRouter()
	router.HandleFunc("/", s.indexHandler).Methods("GET")
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	// setup api routes
	apiV1 := router.PathPrefix("/api/v1").Subrouter()
	apiV1.HandleFunc("/hello", s.helloHandler).Methods("GET")
	return router
}

func (s Server) Start() {
	router := s.NewRouter()

	// get address with hosting port
	hostaddr := fmt.Sprintf(":%d", PORT)

	// start server
	zap.S().With(zap.String("domain", "server"), zap.String("activity", "init")).Infof("Starting server on '%s'", hostaddr)
	zap.S().Fatal(http.ListenAndServe(hostaddr, router))

	// catch cancel signal
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM, syscall.SIGTERM)
	<-c
}

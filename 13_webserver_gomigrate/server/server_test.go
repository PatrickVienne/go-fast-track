package server

import (
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/PatrickVienne/go-advanced/13_webserver_gomigrate/persistence"
)

func TestIndex(t *testing.T) {
	// Create server using the a router initialized elsewhere. The router
	// can be a Gorilla mux as in the question, a net/http ServeMux,
	// http.DefaultServeMux or any value that statisfies the net/http
	// Handler interface.
	db := persistence.FakeDB{}
	router := NewServer(db).NewRouter()

	ts := httptest.NewServer(router)
	defer ts.Close()

	newreq := func(method, url string, body io.Reader) *http.Request {
		r, err := http.NewRequest(method, url, body)
		if err != nil {
			t.Fatal(err)
		}
		return r
	}

	tests := []struct {
		name string
		r    *http.Request
		w    string
	}{
		{name: "1: testing get", r: newreq("GET", ts.URL+"/api/v1/hello", nil), w: "Hello Gophers!"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			resp, err := http.DefaultClient.Do(tt.r)
			defer resp.Body.Close()
			if err != nil {
				t.Fatal(err)
			}

			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				t.Errorf("failed request")
			}

			if string(body) != tt.w {
				t.Fail()
			}
			// check for expected response here.
		})
	}
}

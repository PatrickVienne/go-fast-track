package main

import (
	"gitlab.com/PatrickVienne/go-advanced/13_webserver_gomigrate/loggerinit"
	"gitlab.com/PatrickVienne/go-advanced/13_webserver_gomigrate/persistence"
	"gitlab.com/PatrickVienne/go-advanced/13_webserver_gomigrate/server"
)

func main() {
	loggerinit.Logger()

	db := persistence.GetDb()
	// db := persistence.FakeDB{}

	myServer := server.NewServer(db)
	myServer.Start()
}

package models

type NavbarData struct {
	VERSION string
}

type Book struct {
	Title  string
	Author string
}

type TemplateData struct {
	NavbarData
	Books []Book
}

# SQL-C

## Generate Golang Code
Using  the queries and schema, we can generate the golang codes for the models and query functions

```shell
cd persistence
docker-compose -f docker-compose-sqlc-gen.yml up --remove-orphans
```


## startup postgres server

```shell
cd persistence
docker-compose -f docker-compose.yml up --remove-orphans -d
```

## clean docker images

Stop all the containers
```bash
docker stop $(docker ps -a -q)
```
Remove all the containers
```bash
docker rm $(docker ps -a -q)
```


# Go migrate

Source:
https://github.com/MarioCarrion/videos/tree/main/2021/01/09-go-tools-database-migrations

## Commands

Install go migrate:
```sh
go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest
```

### Run migration

!!Please recheck in each command the given path!!

```sh
migrate -path migrations/ -database "postgres://user:password@localhost:5432/dbname?sslmode=disable" up
```

### Apply latest migrations

Applying the migration now, will only add this one entry:
```sh
$ migrate -path migrations/ -database "postgres://user:password@localhost:5432/dbname?sslmode=disable" -verbose up
20220808202236/u add_title_to_books (37.2786ms)
```

### Revert migrate
After we down migrate, all these tables will be gone

You can revert it with
```sh
migrate -path migrations/ -database "postgres://user:password@localhost:5432/dbname?sslmode=disable" down
```


create schema file for migration
```shell
migrate create -ext sql -format unix -dir ./persistence/migrations create_books_table
```

## migrate manually

```bash
cd persistence
migrate -path ./migrations -database postgres://user:password@localhost:5432/dbname?sslmode=disable up
```

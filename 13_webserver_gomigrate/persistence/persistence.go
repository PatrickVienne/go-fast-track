package persistence

import (
	"context"

	"gitlab.com/PatrickVienne/go-advanced/13_webserver_gomigrate/models"
	"go.uber.org/zap"
)

type PGDB struct {
	queries *Queries
}

func GetDb() PGDB {
	return PGDB{queries: getDb()}
}

func (pg PGDB) CreateBooks(ctx context.Context, book models.Book) models.Book {

}
func (pg PGDB) GetBooks(ctx context.Context) []models.Book {

	books, err := pg.queries.GetBooks(ctx)
	resbooks := make([]models.Book, len(books))
	if err != nil {
		zap.S().With(zap.String("domain", "db"), zap.String("activity", "load-books")).Warnf("Load books failed", len(books))
		return resbooks
	}

	zap.S().With(zap.String("domain", "db"), zap.String("activity", "load-books")).Infof("Load '%d' books", len(books))

	for idx := range books {
		resbooks[idx] = models.Book{Author: books[idx].Author, Title: books[idx].Title}
	}
	return resbooks
}

type FakeDB struct {
}

func (f FakeDB) GetBooks(ctx context.Context) []models.Book {
	return []models.Book{
		{Author: "Stanisław Lem", Title: "The Star Diaries"},
		{Author: "Michael Ende", Title: "Momo"},
		{Author: "Fyodor Dostoevsky", Title: "The Idiot"},
		{Author: "The Invention of Nature: Alexander von Humboldt's New World", Title: "Andrea Wulf"},
	}
}

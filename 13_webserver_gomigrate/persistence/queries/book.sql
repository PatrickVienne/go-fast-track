-- name: GetBookById :one
SELECT *
FROM books
WHERE id = $1;

-- name: GetBooks :many
SELECT *
FROM books;


-- name: CreateBooks :copyfrom
INSERT INTO books (author, title)  VALUES (
    $1, 
    $2
);

-- name: CreateBook :one
INSERT INTO books (author, title)  VALUES (
    $1, 
    $2
)
RETURNING *;

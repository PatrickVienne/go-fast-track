-- CREATE EXTENSION 'uuid-ossp';
-- https://www.postgresql.org/docs/9.1/datatype-numeric.html

CREATE TABLE books (
  id                    SERIAL                    PRIMARY KEY,
  author                VARCHAR                   NOT NULL,
  title                 VARCHAR                   NOT NULL,
  created_at            TIMESTAMP WITH TIME ZONE  NOT NULL DEFAULT NOW()
);

INSERT INTO books (author, title)  VALUES ('Stanisław Lem',  'The Star Diaries');
INSERT INTO books (author, title)  VALUES ('Michael Ende',  'Momo');
INSERT INTO books (author, title)  VALUES ('Fyodor Dostoevsky',  'The Idiot');
INSERT INTO books (author, title)  VALUES ('Andrea Wulf', 'The Invention of Nature: Alexander von Humboldts New World');

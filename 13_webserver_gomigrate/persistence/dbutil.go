package persistence

import (
	"context"
	"fmt"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"
)

const (
	CONNETION_RETRIES = 10
	CONNECT_WAIT      = 2 * time.Second
)

const PostgresConnectionString = "postgres://user:password@localhost:5432/dbname?sslmode=disable"

var conn *pgxpool.Pool
var db *Queries

var ErrDBNotUp = fmt.Errorf("database not up")
var PingDBNotUp = fmt.Errorf("could not ping the database")

func getDb() *Queries {
	if db == nil {
		Init()
	}
	return db
}

func initDb(url string) *pgxpool.Pool {
	zap.S().With(zap.String("domain", "db"), zap.String("activity", "init")).Infof("Connecting to db on '%s'", url)
	connDB, err := ConnectPg(url, context.Background())
	if err != nil {
		zap.L().Warn(fmt.Sprintf("failed to connect to database at: '', with error: %s", url))
		zap.S().Error(err)
		return nil
	}
	return connDB
}

func Init() *Queries {
	conn = initDb(PostgresConnectionString)
	if conn == nil {
		zap.S().Errorf("failed to get connection")
		return nil
	}
	db = New(conn)
	zap.S().Info("initialized database")
	return db
}

func CheckPg(dbPool *pgxpool.Pool, ctx context.Context) (success bool) {
	conn, err := dbPool.Acquire(ctx)
	if err != nil {
		zap.S().Debugf("cannot acquire conn: %v", err.Error())
		return
	}
	defer conn.Release()

	err = conn.Conn().Ping(ctx)
	if err != nil {
		zap.S().Debugf("pgx ping error: %v", err.Error())
		return
	}
	success = true
	zap.S().Infof("Connection success to database: %#v", PostgresConnectionString)
	return success
}

func ConnectPg(connectionUrl string, ctx context.Context) (*pgxpool.Pool, error) {
	zap.S().Debugf("connecting to postgres: %s", connectionUrl)

	for i := 0; i < CONNETION_RETRIES; i++ { // start of the execution block
		db, err := pgxpool.Connect(ctx, connectionUrl)
		if err != nil {
			zap.S().Warnf("could not connect to pg: %v", err.Error())
			time.Sleep(CONNECT_WAIT)
			continue
		}
		if !CheckPg(db, ctx) {
			continue
		} else {
			return db, nil
		}

	} // end
	return nil, fmt.Errorf("could not connect to database at '%s' after retries. %w", connectionUrl, ErrDBNotUp)
}

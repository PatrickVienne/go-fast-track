# Run Example
After building the application, start them

## Start Server
```shell
GRPC_PORT=50051 ./server/server.exe
```
OUTPUT:
```shell
Starting server on port :50051...
Server successfully started on port :50051...
```

## Start Client
```shell
HOST_ADDR=0.0.0.0:8080 GRPC_CONNECT_ADDR=localhost:50051 ./client/client.exe
```
OUTPUT:
```shell
2023/08/06 19:35:03 Listening on: 0.0.0.0:8080
```

## KREYA Setup

Project>Environment>Default Setting

- Endpoint:
```
http://localhost:50051
```
- TLS:
```
Disabled
```
- Mode:
```
gRPC
```

## Send Requests
Make sure to set to send multiple requests to keep the stream open and send more data.
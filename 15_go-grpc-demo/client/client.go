package main

import (
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	stationpb "gitlab.com/PatrickVienne/grpc-demo/proto/station"
	"google.golang.org/grpc"
)

type Server struct {
	client stationpb.StationServiceClient
}

func (s *Server) GetMaxDelay(w http.ResponseWriter, r *http.Request) {

	stream, err := s.client.MaxDelay(r.Context())
	ctx := stream.Context()

	var max int32
	done := make(chan bool)

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	go func(stream stationpb.StationService_MaxDelayClient) {
		delay := int32(0)
		for i := 0; i < 10; i++ {
			// simulate train delay at certain station,
			// up 5min faster or behind planned travel time each station.
			currentDelay := int32(rand.Intn(11) - 5) //int32(rand.Intn(i))
			delay += currentDelay
			if delay < 0 {
				// train must never leave earlier than schedule.
				// so negativ delays are reset to 0
				delay = 0
			}
			req := stationpb.Request{Num: delay}

			if err := stream.Send(&req); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			log.Printf("sent: %d\n", delay)
			time.Sleep(200 * time.Millisecond)
		}

		if err := stream.CloseSend(); err != nil {
			log.Println("stream ended")
		}
	}(stream)

	go func(stream stationpb.StationService_MaxDelayClient) {
		for {
			resp, err := stream.Recv()
			if err == io.EOF {
				log.Println("done")
				return
			}
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}

			max = resp.Num

			log.Printf("received current highest delay: %d\n", max)
		}
	}(stream)

	go func() {

		<-ctx.Done()
		if err := ctx.Err(); err != nil {
			log.Println("context error:", err)
			close(done)
		}

	}()

	<-done
	log.Println("max done")
}

func NewServer() *Server {
	listenaddr := os.Getenv("GRPC_CONNECT_ADDR")
	conn, err := grpc.Dial(listenaddr, grpc.WithInsecure())

	if err != nil {
		log.Fatalf("can not connect to grpc server via connection: %s. %v", listenaddr, err)
	}

	client := stationpb.NewStationServiceClient(conn)

	return &Server{client: client}
}

func main() {
	server := NewServer()

	router := mux.NewRouter()

	apiV1Router := router.PathPrefix("/api/v1").Subrouter()
	apiV1Router.HandleFunc("/getmax", server.GetMaxDelay).Methods("GET")

	hostaddr := os.Getenv("HOST_ADDR")
	log.Printf("Listening on: %s\n", hostaddr)
	http.ListenAndServe(hostaddr, apiV1Router)
}

// HOST_ADDR=0.0.0.0:8080 GRPC_CONNECT_ADDR=localhost:50051 ./client/client.exe
// HOST_ADDR=0.0.0.0:8080 GRPC_CONNECT_ADDR=localhost:50051 go run ./client/client.go

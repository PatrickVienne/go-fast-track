package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/signal"

	"google.golang.org/grpc"

	stationpb "gitlab.com/PatrickVienne/grpc-demo/proto/station"
)

type StationServiceServer struct {
	stationpb.UnimplementedStationServiceServer
}

func (s *StationServiceServer) MaxDelay(srv stationpb.StationService_MaxDelayServer) error {

	log.Println("start max server")
	ctx := srv.Context()
	var max int32

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
		}

		req, err := srv.Recv()

		if err == io.EOF {
			log.Println("exit")
			return nil
		}

		if err != nil {
			log.Printf("could not interpret package. %v", err)
			continue
		}

		if req.Num <= max {
			continue
		}

		max = req.Num

		resp := stationpb.Response{Num: max}
		if err := srv.Send(&resp); err != nil {
			log.Printf("could not send package: %v", err)
		} else {
			log.Printf("sent new max: %d", max)
		}
	}

}

func main() {

	log.SetFlags(log.LstdFlags | log.Lshortfile)
	port := os.Getenv("GRPC_PORT")
	if port == "" {
		port = "50051"
	}
	fmt.Printf("Starting server on port :%s...\n", port)

	// Start our listener, 50051 is the default gRPC port
	listener, err := net.Listen("tcp", ":"+port)
	// Handle errors if any
	if err != nil {
		log.Fatalf("Unable to listen on port :%s. %v", port, err)
	}

	// Create new gRPC server with (blank) options
	s := grpc.NewServer([]grpc.ServerOption{}...)

	// Create StationService type
	srv := &StationServiceServer{}

	// Register the service with the server
	stationpb.RegisterStationServiceServer(s, srv)

	go func() {
		if err := s.Serve(listener); err != nil {
			log.Fatalf("Failed to serve: %v", err)
		}
	}()
	fmt.Printf("Server successfully started on port :%s...\n", port)

	// go routine does not block,
	// so add blocking channel waiting for signal.
	c := make(chan os.Signal)
	// only care about interrupt
	signal.Notify(c, os.Interrupt)
	// block until user stops with CTRL-C
	<-c

}

// GRPC_PORT=50051 ./server/server.exe
// GRPC_PORT=50051 go run ./server/server.go

package server

import (
	"log"
	"net/http"
	"postgresexample/controller"
	"postgresexample/middleware"
	prometheus_setup "postgresexample/prometheus"

	"github.com/gorilla/mux"
	"go.uber.org/zap"
)

func StartServer() {
	PORT := "8080"
	PROMETHEUS_METRICS_PORT := "9001"
	ENDPOINT_METRICS := "/metrics"

	prometheus_setup.InitPrometheus(PROMETHEUS_METRICS_PORT, ENDPOINT_METRICS)

	router := mux.NewRouter()
	apiV1 := router.PathPrefix("/api/v1/").Subrouter()

	// CORS handler
	apiV1.Use(middleware.AccessControlMiddleware)
	// Throttle max requests
	apiV1.Use(middleware.ThrottleMiddleware)

	// Calculation
	router.HandleFunc("/calc/{op}", controller.Calculator).Methods("GET")

	// Handle API routes

	// get books
	apiV1.HandleFunc("/books", controller.GetBooks).Methods("GET")
	apiV1.HandleFunc("/books/new", controller.CreateBooks).Methods("GET")
	// delete books
	apiV1.HandleFunc("/books", controller.DeleteBooks).Methods("DELETE")

	// Page
	router.HandleFunc("/", controller.Index).Methods("GET")
	// add routing for static content
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static/"))))

	// listen and server the router with the port
	log.Default().Println("Serving on Port: " + PORT)
	err := http.ListenAndServe(":"+PORT, router)
	if err != nil {
		log.Default().Println("ERROR: Stopped Serving with Error", zap.Error(err))
	}
}

package prometheus_setup

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.uber.org/zap"
)

var (
	Requests prometheus.Counter
	Latency  *prometheus.GaugeVec
)

func InitPrometheus(prometheusPort, prometheusEntry string) {
	Requests = promauto.NewCounter(prometheus.CounterOpts{
		Name: "num_total_requests",
		Help: "The total number of requests received",
	})
	Latency = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: "api",
		Subsystem: "dev",
		Name:      "latency_ms",
		Help:      "Seconds of latency for specific endpoint",
	}, []string{"endpoint", "method"})

	promrouter := mux.NewRouter()
	promrouter.Handle(prometheusEntry, promhttp.Handler())

	go func() {
		log.Default().Printf(fmt.Sprintf("Starting Prometheus on :%v, entry point is %v", prometheusPort, prometheusEntry))
		err := http.ListenAndServe(":"+prometheusPort, promrouter)
		if err != nil {
			log.Default().Fatal("Stopped Serving with Error", zap.Error(err))
		}
	}()
}

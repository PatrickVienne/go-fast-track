package util

import (
	"fmt"
	"log"
	prometheus_setup "postgresexample/prometheus"
	"sync"
	"time"

	"golang.org/x/time/rate"
)

var (
	/* global variables to keep rate limiter */
	rateLimiter *rate.Limiter
	mu          sync.Mutex
	requests    = make(map[string]*request)
)

const MAX_BURST_PER_TIME_PERIOD = 1
const MIN_TIME_BETWEEN_REQ_MS = 1000

type request struct {
	limiter  *rate.Limiter
	lastSeen time.Time
}

func RecordLatency(start time.Time, endpoint string, method string) {
	latency := float64(time.Since(start).Microseconds())
	fmt.Printf("LOG-Latency: %s [%s]: %s ms", endpoint, method, latency)
	prometheus_setup.Latency.WithLabelValues(endpoint, method).Set(latency)
}

func GetRequestLimiter(ip string) *rate.Limiter {
	mu.Lock()
	defer mu.Unlock()

	v, exists := requests[ip]
	if !exists {
		rateLimiter = rate.NewLimiter(rate.Every(time.Duration(MIN_TIME_BETWEEN_REQ_MS)*time.Millisecond), MAX_BURST_PER_TIME_PERIOD)
		requests[ip] = &request{rateLimiter, time.Now()}
		return rateLimiter
	} else {
		log.Default().Printf("Request Limits on %s. Last Seen: %s, limiter: %v", ip, v.lastSeen, v.limiter)
	}
	// Update the last seen time for the visitor.
	v.lastSeen = time.Now()
	return v.limiter
}

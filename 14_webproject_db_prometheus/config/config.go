package config

import (
	"fmt"
	"os"
)

const (
	DBUser     = "user"
	DBPassword = "password"
	DBName     = "dbname"
	DBHost     = "localhost"
	DBPort     = "5432"
	DBType     = "postgres"
)

func GetDBType() string {
	return DBType
}

func GetPostgresConnectionString() string {
	host, present := os.LookupEnv("DBHOST")
	if !present {
		host = DBHost
	}

	dataBase := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable",
		host,
		DBPort,
		DBUser,
		DBName,
		DBPassword)
	return dataBase
}

func GetSqlPostgresConnectionString() string {
	dataBase := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		DBUser,
		DBPassword,
		DBHost,
		DBPort,
		DBName)
	return dataBase
}

package controller

import (
	"log"
	"net/http"
	prometheus_setup "postgresexample/prometheus"
	"postgresexample/service"
	"text/template"
)

type BookDTO struct {
	Name   string
	Author string
}

type TemplateData struct {
	Books []BookDTO
}

func Index(w http.ResponseWriter, r *http.Request) {
	prometheus_setup.Requests.Inc()

	tpl, err := template.ParseGlob("templates/*.html")

	if err != nil {
		log.Fatalf("FATAL: could not find templates. %v\n", err)
	}

	// First implementation with fixed data
	// var data = TemplateData{Books: []BookDTO{BookDTO{Name: "Harry Potter", Author: "J. K. Rowling"}}}
	// err = tpl.ExecuteTemplate(w, "mainpage", data)

	authors, _ := service.GetGormAuthors()
	var books = []BookDTO{}
	for _, author := range authors {
		books = append(books, BookDTO{Name: author.Name, Author: author.Author})
	}
	var data = TemplateData{Books: books}

	if err != nil {
		log.Fatalf("FATAL: could not find data for webpage. %v\n", err)
	}

	err = tpl.ExecuteTemplate(w, "mainpage", data)

	if err != nil {
		log.Fatalf("FATAL: could build template. %v\n", err)
	}

}

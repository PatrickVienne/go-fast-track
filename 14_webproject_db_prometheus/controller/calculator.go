package controller

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func adder(numbers ...int) int {
	total := 0
	for _, num := range numbers {
		total += num
	}
	return total
}

func Calculator(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	operation := params["op"]
	if operation == "add" {
		values := []byte(r.FormValue("values"))
		var data []int
		json.Unmarshal(values, &data)
		result := adder(data...)
		w.Write([]byte(fmt.Sprintf("Adder(%v) = %d", data, result)))
	} else {
		w.Write([]byte(`Unknown Operation`))
	}
}

package controller

import (
	"encoding/json"
	"log"
	"net/http"
	"postgresexample/service"
	"postgresexample/util"
	"time"
)

func GetBooks(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	defer util.RecordLatency(start, "getBooks", r.Method)

	authors, err := service.GetGormAuthors()

	if err != nil {
		log.Fatalf("FATAL: could not retrieve books")
	}

	json.NewEncoder(w).Encode(authors)
}

func DeleteBooks(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	defer util.RecordLatency(start, "deleteBooks", r.Method)

	err := service.DeleteGormAuthor("*", "*")

	if err != nil {
		log.Fatalf("FATAL: could not retrieve books")
	}

	json.NewEncoder(w).Encode("")
}

func CreateBooks(w http.ResponseWriter, r *http.Request) {
	start := time.Now()
	defer util.RecordLatency(start, "postBooks", r.Method)

	name := r.FormValue("name")
	author := r.FormValue("author")

	err := service.CreateGormAuthor(author, name)

	if err != nil {
		log.Fatalf("FATAL: could not create books")
	}

	authors, err := service.GetGormAuthors()

	if err != nil {
		log.Fatalf("FATAL: could not retrieve books")
	}
	json.NewEncoder(w).Encode(authors)
}

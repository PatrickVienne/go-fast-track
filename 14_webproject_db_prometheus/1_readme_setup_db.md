# go migrate example

Source:
https://github.com/MarioCarrion/videos/tree/main/2021/01/09-go-tools-database-migrations

## Commands

Install go migrate:
```sh
go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest
```

### Start postgresdb
start the docker-compose file
```
docker-compose up
```
and leave the terminal open to follow the log output of the postgres container.


### Setup migrations file
We initialize our migrations with 4 files:
1) **20210109000712_create_authors.down.sql**
```sql
DROP TABLE authors;
DROP EXTENSION "uuid-ossp";
```
2) **20210109000712_create_authors.up.sql**
```sql
CREATE EXTENSION "uuid-ossp";

CREATE TABLE authors (
  uuid       uuid      PRIMARY KEY DEFAULT uuid_generate_v4(),
  author     VARCHAR   NOT NULL,
  name       VARCHAR   NOT NULL,
  created_at TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW()
);
```
3) **20210109000713_books.down.sql**
```sql
DROP TABLE books;
```
4) **20210109000713_books.up.sql**
```sql
CREATE TABLE books (
  isbn        VARCHAR   PRIMARY KEY,
  author_uuid uuid      NOT NULL,
  created_at  TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),

  FOREIGN KEY (author_uuid) REFERENCES authors (uuid)
);
```

### Run migration

```sh
migrate -path migrations/ -database "postgres://user:password@localhost:5432/dbname?sslmode=disable" up
```

### Check DB state in the docker

We enter into the container:
```sh
docker exec -it $(docker ps | grep gomigrate | awk -F ' ' '{print $1}') bash
```

Enter the postgres shell, and list all schema
```sh
bash-5.1#  psql dbname  -U user
```

Check authors and books table:
```sh
dbname=# \d authors

                                Table "public.authors"
   Column   |            Type             | Collation | Nullable |      Default
------------+-----------------------------+-----------+----------+--------------------
 uuid       | uuid                        |           | not null | uuid_generate_v4()
 author     | character varying           |           | not null |
 name       | character varying           |           | not null |
 created_at | timestamp without time zone |           | not null | now()
Indexes:
    "authors_pkey" PRIMARY KEY, btree (uuid)
Referenced by:
    TABLE "books" CONSTRAINT "books_author_uuid_fkey" FOREIGN KEY (author_uuid) REFERENCES authors(uuid)
```

```sh
dbname=# \d books

                            Table "public.books"
   Column    |            Type             | Collation | Nullable | Default
-------------+-----------------------------+-----------+----------+---------
 isbn        | character varying           |           | not null |
 author_uuid | uuid                        |           | not null |
 created_at  | timestamp without time zone |           | not null | now()
Indexes:
    "books_pkey" PRIMARY KEY, btree (isbn)
Foreign-key constraints:
    "books_author_uuid_fkey" FOREIGN KEY (author_uuid) REFERENCES authors(uuid)
```

### Revert migrate
After we down migrate, all these tables will be gone

You can revert it with
```sh
migrate -path migrations/ -database "postgres://user:password@localhost:5432/dbname?sslmode=disable" down
```

```sh
dbname=# \d books
Did not find any relation named "books".
dbname=# \d authors
Did not find any relation named "authors".
```

### Migrate and create additional changes
Let's get them back:
```sh
migrate -path migrations/ -database "postgres://user:password@localhost:5432/dbname?sslmode=disable" up
```

We would like to add a new column now, and track the change via migration.

First we create a new migration file:
```sh
migrate create -ext sql -dir migrations/ add_title_to_books
```

And we fill in the **..._up.sql** file:
```sql
ALTER TABLE books
ADD COLUMN title VARCHAR NOT NULL;
```


And we fill in the **..._down.sql** file:
```sql
ALTER TABLE books
DROP COLUMN IF EXISTS title;
```

### Apply latest migrations

Applying the migration now, will only add this one entry:
```sh
$ migrate -path migrations/ -database "postgres://user:password@localhost:5432/dbname?sslmode=disable" -verbose up
20220808202236/u add_title_to_books (37.2786ms)
```
package service

import (
	"context"
	"errors"
	"fmt"
	"log"
	"postgresexample/model"
	"postgresexample/persistence"
)

var dbContext = context.Background()
var ErrDeleteFailed = errors.New("Could not Delete Row")

// GetAuthors
func GetGormAuthors() ([]model.Author, error) {
	fmt.Println("==GetGormAuthors==")
	db := persistence.GetGormDBInstance()
	authors := []model.Author{}

	if err := db.Find(&authors).Error; err != nil {
		return nil, err
	}

	for idx := range authors {
		fmt.Printf("%v\n", authors[idx])
	}

	return authors, nil
}

// GetAuthors
func CreateGormAuthor(author string, name string) error {
	fmt.Println("==CreateGormAuthor==")
	db := persistence.GetGormDBInstance()
	newAuthor := &model.Author{Author: author, Name: name}
	if err := db.Create(newAuthor).Error; err != nil {
		return err
	}
	return nil
}

func DeleteGormAuthor(author string, name string) error {
	fmt.Println("==DeleteGormAuthor==")
	db := persistence.GetGormDBInstance()
	var authorM model.Author
	db.Where("author = $1 AND name = $2", author, name).Delete(authorM)
	return nil
}

// GetAuthors
func GetSqlAuthors() {
	fmt.Println("==GetSqlAuthors==")
	db := persistence.GetSqlDBInstance()

	rows, errSelect := db.Query("select * from authors")
	if errSelect != nil {
		log.Fatalf("SELECT ERROR: %v\n", errSelect)
	}

	defer rows.Close()

	for rows.Next() {
		var author model.Author

		err := rows.Scan(&author.Uuid, &author.Author, &author.Name, &author.CreatedAt)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("%+v\n", author)
	}
}

// func GetAuthorsWithGORM(db gorm.DB) {
// 	// query all the posts
// 	result, err := db.Query("SELECT * FROM authors;")
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	defer result.Close()

// 	// iterate over all the posts and print them
// 	for result.Next() {
// 		var author Author

// 		err := result.Scan(&author.Uuid, &author.Author, &author.Name, &author.CreatedAt)
// 		if err != nil {
// 			log.Fatal(err)
// 		}

// 		fmt.Printf("%+v\n", author)
// 	}
// }

// GetAuthors
func CreateSqlAuthor(author string, name string) (string, error) {
	fmt.Println("==CreateSqlAuthor==")

	db := persistence.GetSqlDBInstance()

	err := db.PingContext(dbContext)
	if err != nil {
		log.Fatal(err)
		return "", err
	}

	sqlStatement := `
	INSERT INTO authors(author, name)
	VALUES ($1, $2)
	RETURNING uuid;`

	// query, err := db.Prepare(sqlStatement)
	// if err != nil {
	// 	return -1, err
	// }

	var newID string

	err = db.QueryRow(sqlStatement, author, name).Scan(&newID)

	// defer query.Close()
	// newRecord := query.QueryRowContext(dbContext,
	// 	sql.Named("Author", author),
	// 	sql.Named("Name", name),
	// )

	// var newID int64
	// err = newRecord.Scan(&newID)
	if err != nil {
		log.Fatal(err)
		return "", err
	}

	return newID, nil
}

func DeleteSqlAuthor(author string, name string) error {
	fmt.Println("==DeleteSqlAuthor==")

	db := persistence.GetSqlDBInstance()
	res, err := db.ExecContext(dbContext, "DELETE FROM authors WHERE author = $1 AND name = $2", author, name)
	if err != nil {
		return err
	}
	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if rowsAffected == 0 {
		return ErrDeleteFailed
	}

	return err
}

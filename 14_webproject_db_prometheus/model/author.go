package model

import (
	"time"
)

type Author struct {
	BasePersistenceModel // gorm.Model can also be used,if ID may be an integer
	// gorm.Model
	Uuid      string    `gorm:"primary_key";json:"uuid"`
	Author    string    `gorm:"not null";json:"author"`
	Name      string    `gorm:"not null";json:"name"`
	CreatedAt time.Time `json:"createdAt"`
}

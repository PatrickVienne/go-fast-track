package main

import (
	"postgresexample/persistence"
	"postgresexample/server"
	"postgresexample/service"

	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
)

func testDBAccess() {
	// Echo instance
	persistence.NewGormDB()
	persistence.NewSqlDB()

	// SQL Examples:
	service.GetSqlAuthors()
	service.CreateSqlAuthor("J.K.Rowling", "Joanne")
	service.GetSqlAuthors()
	service.DeleteSqlAuthor("J.K.Rowling", "Joanne")
	service.GetSqlAuthors()

	// GORM Examples:

	service.GetGormAuthors()
	service.CreateGormAuthor("J.K.Rowling", "Joanne")
	service.GetGormAuthors()
	service.DeleteGormAuthor("J.K.Rowling", "Joanne")
	service.GetGormAuthors()
}

func StartServer() {
	server.StartServer()
}

func main() {
	// testDBAccess()
	persistence.NewGormDB()
	persistence.NewSqlDB()

	StartServer()
}

package persistence

import (
	"database/sql"
	"log"
	"postgresexample/config"

	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var DBSql *sql.DB

func NewSqlDB(params ...string) *sql.DB {
	var err error
	conString := config.GetSqlPostgresConnectionString()

	log.Print(conString)

	DBSql, err = sql.Open("postgres", conString)

	if err != nil {
		log.Panic(err)
	}

	return DBSql
}

func GetSqlDBInstance() *sql.DB {
	return DBSql
}

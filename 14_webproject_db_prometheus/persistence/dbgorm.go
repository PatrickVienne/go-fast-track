package persistence

import (
	"log"
	"postgresexample/config"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var DBGorm *gorm.DB

func NewGormDB(params ...string) *gorm.DB {
	var err error
	conString := config.GetPostgresConnectionString()

	log.Print(conString)

	DBGorm, err = gorm.Open(config.GetDBType(), conString)

	if err != nil {
		log.Panic(err)
	}

	if err := DBGorm.DB().Ping(); err != nil {
		log.Fatal("Failed to ping db", err)
	}

	return DBGorm
}

func GetGormDBInstance() *gorm.DB {
	return DBGorm
}

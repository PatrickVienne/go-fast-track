# Reduce the docker size by multistage builds, and basing the final image on the smallest base possible
# https://koenverburg.dev/blog/set-up-a-multi-stage-docker-build-for-go
# REPOSITORY               TAG       IMAGE ID       CREATED              SIZE
# hello-world-stratch      latest    eb41c9777973   58 seconds ago       1.94MB
# hello-world-debian       latest    7ad8a5965a06   About a minute ago   21.1MB
# hello-world-golang1.16   latest    68b776701cbe   About a minute ago   304MB

#
# Builder
#
FROM golang:1.19.0-alpine3.16 AS builder

# Create a workspace for the app
WORKDIR /app

# Download necessary Go modules
COPY go.mod .
RUN go mod download

# Copy over the source files
COPY . /app/

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
    go build -gcflags "all=-N -l" -o /main

#
# Runner
#

# FROM gcr.io/distroless/base-debian10 AS runner
FROM scratch AS runner

WORKDIR /

# Copy from builder the final binary
COPY --from=builder /main /main
COPY --from=builder /app/templates /templates
COPY --from=builder /app/static /static

# USER nonroot:nonroot

EXPOSE 8080
EXPOSE 9001

ENTRYPOINT ["/main"]

# docker build . -t webservice-golang1.18
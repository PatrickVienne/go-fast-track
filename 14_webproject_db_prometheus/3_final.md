# Starting it all

## Build the docker image

```
docker build . -t webservice-golang1.18
```
## 
Up the entire application
```sh
docker-compose up
```

Execute the migration
```sh
migrate -path migrations/ -database "postgres://user:password@localhost:5432/dbname?sslmode=disable" up
```

Check out the endpoints
```
http://localhost:9001/metrics
http://localhost:8080/
http://localhost:8080/api/v1/books
http://localhost:8080/api/v1/books/new?author=tobi&name=biography
http://localhost:8080/api/v1/books/new?author=J.K.Rowling&name=Harry%20Potter
```

Prometheus
```
http://localhost:9090/graph?g0.expr=num_total_requests&g0.tab=0&g0.stacked=0&g0.show_exemplars=0&g0.range_input=1h
```

Grafana
```
http://localhost:3000/d/_owf_Mi4k/new-dashboard-copy?orgId=1&from=now-15m&to=now
```
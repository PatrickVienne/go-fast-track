# Go application

Application Setup
https://onexlab-io.medium.com/golang-postgres-gorm-echo-e2133eed295

Template
https://golangdocs.com/templates-in-golang


## Setup main.go and a module
```sh
go mod init postgresexample
```

**main.go**

```go
package main

import (
	"database/sql"
    "log"
    _ "github.com/lib/pq"
)

func main() {

	// credentials used from the compose setup previously
	db, err := sql.Open("postgres", "postgres://user:password@localhost:5432/postgres?sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()
    
    if err := db.Ping(); err != nil {
		log.Fatal("Failed to ping db", err)
	}
}
```

### get dependencies:

```sh
$ go mod tidy
go: finding module for package github.com/lib/pq
go: downloading github.com/lib/pq v1.10.6
go: found github.com/lib/pq in github.com/lib/pq v1.10.6
```

### first run
```sh
$ go run main.go 
```

## Adding the first entry

Remember that the table of author looks like this:

```js
 uuid       | uuid                        |           | not null | uuid_generate_v4() 
 author     | character varying           |           | not null |
 name       | character varying           |           | not null |
 created_at | timestamp without time zone |           | not null | now()
```

Create a Author golang struct

<!-- https://github.com/oxlb/docker-postgres-multiple-databases/blob/master/docker_postgres_init.sql -->
<!-- https://onexlab-io.medium.com/golang-postgres-gorm-echo-e2133eed295 -->
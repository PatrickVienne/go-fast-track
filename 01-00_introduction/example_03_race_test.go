package introduction

import "testing"

// run with race condition checker
// go test -race -run ^TestConcurrentMapAccess$ example_03_race_test.go

// DETECTION OUTPUT
/*
$ go test -race -run ^TestConcurrentMapAccess$ example_03_race_test.go
# command-line-arguments [command-line-arguments.test]
==================
WARNING: DATA RACE
Write at 0x00c0000b88a0 by goroutine 9:
  runtime.mapassign_faststr()
      C:/Program Files/Go/src/runtime/map_faststr.go:203 +0x0
...
*/
func TestConcurrentMapAccess(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "simple_race_test"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ConcurrentMapAccess()
		})
	}
}

package introduction

import (
	"testing"
)

// get familiar with running simple tests

// run all tests by typing in the terminal, will run verbose
// 'go test -v -race'

func Test_E_00100_HelloWorld(t *testing.T) {
	E_00100_HelloWorld()
}

func Benchmark_E_00100_HelloWorld(b *testing.B) {
	for n := 0; n < b.N; n++ {
		E_00100_HelloWorld()
	}
}

func Test_E_00200_VariablesAndTypes(t *testing.T) {
	E_00200_VariablesAndTypes()
}

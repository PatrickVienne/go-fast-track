package introduction

// imports are defined after the package
import "fmt"

func E_00100_HelloWorld() {
	println("Hello World")
}

// to generate tests, you can also right click and select Generate...
func E_00200_VariablesAndTypes() {

	// variables need to be used when defined
	var (
		name, location string
		age            int
		points         int = 32
	)

	// constants do not need to be used to be defined
	const Pi = 3.14
	const (
		StatusOK      = 200
		StatusCreated = 201
	)

	// assign values with "="
	name = "Spongebob"
	location = "under the sea"
	age = 10

	// declare and assign
	greeting := "Hi"

	// add const example
	const FixedGreeting string = "Salve"

	// now see all assigned values
	fmt.Println(greeting, name, location, age, points, Pi, FixedGreeting)
	fmt.Println(&greeting, &name, &location, &age, &points)

	// cant take pointers of constants. the following does not work
	// &Pi, &FixedGreeting

	type VehicleType int64

	const (
		ICE VehicleType = iota
		EV
	)

	fmt.Println("VehicleTypes: ICE", ICE)
	fmt.Println("VehicleTypes: EV", EV)

}

func E_00300_Functions() {
	add := func(x int, y int) int {
		return x + y
	}

	add_sub := func(x int, y int) (int, int) {
		return x + y, x - y
	}
	result := add(42, 13)
	fmt.Println(result)

	result1, result2 := add_sub(42, 13)
	fmt.Println(result1, result2)
}

func E_00400_ArraysAndSlices() {
	add := func(x int, y int) int {
		return x + y
	}

	add_sub := func(x int, y int) (int, int) {
		return x + y, x - y
	}
	result := add(42, 13)
	fmt.Println(result)

	result1, result2 := add_sub(42, 13)
	fmt.Println(result1, result2)
}

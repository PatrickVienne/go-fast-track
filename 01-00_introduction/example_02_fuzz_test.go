package introduction

import "testing"

// get familiar with running simple tests

// FUZZ  testing
//  This testing framework can help uncover undefined behavior
// that could lead to crashes or other security issues.
//
// run tests without fuzzing:
// go test example_01_test.go -v
//
// run fuzztest with fuzzing:
// go test -fuzz=Fuzz
//
//
// this will fail, as fuzzing finds the 'division by 0' error
// this should create a folder testdata with errors
//
// next time you run the tests, the testdata will be included for regression testing
//
// run tests again without additional fuzzing
// go test -run ^FuzzE_00100_HelloWorld$
//
// following output
/*
 $ go test -run ^FuzzE_00100_HelloWorld$
--- FAIL: FuzzE_00100_HelloWorld (0.00s)
--- FAIL: FuzzE_00100_HelloWorld/5856445ab23f5490 (0.00s)
*/

func FuzzE_00100_HelloWorld(f *testing.F) {
	f.Add(5, 5)
	f.Fuzz(func(t *testing.T, a int, b int) {
		res := div(a, b)
		if res != a/b {
			t.Errorf("wrong results for %d, %d. and was %d", a, b, res)
		}
	})
}

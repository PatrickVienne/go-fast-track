package introduction

import "fmt"

// detect race condition by using this in a script and running with one of:
// go run -race mysrc.go  // compile and run the program
// go build -race mycmd   // build the command
// go install -race mypkg // install the package

func ConcurrentMapAccess() {
	done := make(chan bool)
	m := make(map[string]string)
	m["name"] = "world"
	go func() {
		m["name"] = "data race"
		done <- true
	}()
	fmt.Println("Hello,", m["name"])
	<-done
}

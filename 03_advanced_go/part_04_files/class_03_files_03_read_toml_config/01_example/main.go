package main

import (
	"fmt"
	"log"
	"os"

	toml "github.com/pelletier/go-toml"
)

// https://pkg.go.dev/github.com/pelletier/go-toml

// Config is configuration
type Config struct {
	Login struct {
		User     string
		Password string
	}
}

func ReadWithDecode() {
	fmt.Println("==ReadWithDecode==")
	file, err := os.Open("config.toml")
	if err != nil {
		log.Fatalf("error: can't open config file - %s", err)
	}
	defer file.Close()

	cfg := &Config{}
	dec := toml.NewDecoder(file)
	if err := dec.Decode(cfg); err != nil {
		log.Fatalf("error: can't decode configuration file - %s", err)
	}

	fmt.Printf("%+v\n", cfg)
}

func ReadWithUnMarshaller() {
	fmt.Println("==ReadWithUnMarshaller==")
	data, err := os.ReadFile("config.toml")
	if err != nil {
		log.Fatalf("error: can't read config file - %s", err)
	}

	cfg := &Config{}
	toml.Unmarshal(data, cfg)
	fmt.Printf("%+v\n", cfg)
}

func main() {
	ReadWithDecode()
	ReadWithUnMarshaller()
}

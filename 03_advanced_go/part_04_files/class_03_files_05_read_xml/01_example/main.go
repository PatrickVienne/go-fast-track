package main

import (
	"encoding/xml"
	"fmt"
	"os"
)

type Book struct {
	Name   string `xml:"Name"`
	Author string `xml:"Author"`
}

func ReadWithUnmarshal(filePath string) Book {
	fmt.Println("==ReadWithUnmarshal==")
	// read our opened xmlFile as a byte array.
	bytes, err := os.ReadFile(filePath)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	// we initialize our Users array
	var book Book

	// we unmarshal our byteArray which contains our
	// xmlFile's content into 'book' which we defined above
	xml.Unmarshal(bytes, &book)
	return book
}

func ReadWithDecode(filePath string) Book {
	fmt.Println("==ReadWithDecode==")
	// read our opened xmlFile as a byte array.
	xmlFile, err := os.Open(filePath)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	defer xmlFile.Close()

	dec := xml.NewDecoder(xmlFile)

	var book Book
	dec.Decode(&book)
	return book
}

func main() {

	res1 := ReadWithUnmarshal("book.xml")
	fmt.Printf("%#v\n", res1)
	res2 := ReadWithDecode("book.xml")
	fmt.Printf("%#v\n", res2)
}

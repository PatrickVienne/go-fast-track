# Go visualization

https://youtu.be/46inEdCu0os

We will visualize a dataset published by kaggle on:
https://www.kaggle.com/ursulakaczmarek/brooklyn-food-waste


## Intro
first we just download the data and make it available in a file called brooklyn.csv in our project folder.

Then we set up the first part of the code:
```go
package main

import (
	"log"
	"os"
)

func main() {
	f, err := os.Open("brooklyn.csv")

	if err != nil {
		log.Fatal(err) // will terminate the program if this encounters a fatal error
	}
}
```

then we initialize the csv reader

```go
	reader := csv.NewReader(f)
	reader.LazyQuotes = true
```


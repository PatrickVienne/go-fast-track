package main

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
)

type Languages []Language

type Language struct {
	Id   int
	Name string
}

var (
	languages = Languages{
		{1, "english"},
		{2, "deutsch"},
		{3, "中文"},
		{4, "日本語"},
		{5, "magyar"},
	}
)

func (l *Languages) exportCSV(filename string) {
	f, err := os.Create(filename)
	if err != nil {
		log.Panicf("Could not create %s\n", filename)
	}
	lines := [][]string{}
	for _, line := range *l {
		lines = append(lines, []string{strconv.Itoa(line.Id), line.Name})
	}

	enc := csv.NewWriter(f)
	defer enc.Flush()
	enc.WriteAll(lines)
}

func (l *Language) exportCSVLine(filename string) {
	// do not use "O_TRUNC" as os.Create would do.
	f, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Panicf("Could not create %s\n", filename)
	}

	enc := csv.NewWriter(f)
	defer enc.Flush()
	enc.Write([]string{strconv.Itoa(l.Id), l.Name})
}

func main() {
	languages.exportCSV("languages.csv")

	os.Remove("languages2.csv") // error is ignored
	languages[0].exportCSVLine("languages2.csv")
	languages[1].exportCSVLine("languages2.csv")
}

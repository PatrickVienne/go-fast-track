# JSONS

Parsing JSON files:
https://tutorialedge.net/golang/parsing-json-with-golang/


## Steps

- Step 0: Given are the structs used in this example:
    ```go
    type Users struct {
        Users []User `json:"users"`
    }

    type User struct {
        Name   string `json:"name"`
        Type   string `json:"type"`
        Age    int    `json:"Age"`
        Social Social `json:"social"`
    }

    type Social struct {
        Facebook string `json:"facebook"`
        Twitter  string `json:"twitter"`
    }
    ```

- Step 1:
    ```go
    func toFile(filename string, data Users) {
        file, _ := json.MarshalIndent(data, "", " ")
        _ = ioutil.WriteFile(filename, file, 0644)
    }
    ```

- Step 2: define a function to read a file and return the users
    ```go
    func fromFile(jsonFilePath string) Users {

    }   
    ```
    After that in the function, first the file is open, and we add a check whether opening the file has been successful:
    ```go
        jsonFile, err := os.Open(jsonFilePath)
        if err != nil {
            fmt.Println(err)
        }
    ```
- Step 3: We make sure, that the file is closed, once the function is exited, by making a defer statement:
    ```go
        defer jsonFile.Close()
    ```
- Step 4: Read the content of the file and return it as byte values. This time, we ignore any possible error which might happen.
    ```go
        byteValue, _ := ioutil.ReadAll(jsonFile)
    ```

- Step 5: Finally, parse the byte slice is parsed into golang objects:
    ```go
        var users Users

        json.Unmarshal(byteValue, &users)
        return users
    ```

- Step 6: Now use both functions, to first read the **users.json** and then output the content into another file **new_users.json**
    ```go
    users := fromFile("users.json")
	fmt.Println(users)
	toFile("new_users.json", users)
    ```

## Info
### Important functions

- `os.Create()` : The os.Create() method is used to creates a file with the desired name. If a file with the same name already exists, then the create function truncates the file.

- `ioutil.ReadFile()` : The ioutil.ReadFile() method takes the path to the file to be read as it’s the only parameter. This method returns either the data of the file or an error.

- `ioutil.WriteFile()` : The ioutil.WriteFile() is used to write data to a file. The WriteFile() method takes in 3 different parameters, the first is the location of the file we wish to write to, the second is the data object, and the third is the FileMode, which represents the file’s mode and permission bits.

- `log.Fatalf` : Fatalf will cause the program to terminate after printing the log message. It is equivalent to Printf() followed by a call to os.Exit(1).

- `log.Panicf` : Panic is just like an exception that may arise at runtime. Panicln is equivalent to Println() followed by a call to panic(). The argument passed to panic() will be printed when the program terminates.

- `bufio.NewReader(os.Stdin)` : This method returns a new Reader whose buffer has the default size(4096 bytes).

- `inputReader.ReadString('\n')` : This method is used to read user input from stdin and reads until the first occurrence of delimiter in the input, returning a string containing the data up to and including the delimiter. If an error is encountered before finding a delimiter, it returns the data read before the error and the error itself.

### using `ioutil`
#### Example to simply write file:
```go
package main

import (
    "io/ioutil"
    "log"
)

func main() {
    // create a byte array of a string
    data := []byte("Hello Gopher!")

    // write data to a hello file, with 0777 file permission
    err := ioutil.WriteFile("hello.txt", data, 0777)

    if err != nil {
        log.Fatalf("%v", err)
    }
}
```

#### Example to read file
```go
package main

import (
    "fmt"
    "io/ioutil"
    "log"
)

func main() {

    // read the hello.txt
    content, err := ioutil.ReadFile("hello.txt")

    if err != nil {
        log.Fatalf("error while reading %v", err)
    }

    // convert the byte into string
    fmt.Println(string(content))
}
```

### using `os`

#### Example to simply write file:
```go
package main

import (
    "log"
    "os"
)

func main() {
    // If the file doesn't exist, create it, or append to the file
    file, err := os.OpenFile("hello.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)

    if err != nil {
        log.Fatalf("error while opening the file. %v", err)
    }

    // close the file once program execution complete
    defer file.Close()

    if _, err := file.Write([]byte("\nappended data")); err != nil {
        log.Fatalf("error while writing the file. %v", err)
    }
}
```

#### Example to read file
```go
package main

import (
    "fmt"
    "io/ioutil"
    "log"
)

func main() {

    // read the hello.txt
    content, err := ioutil.ReadFile("hello.txt")

    if err != nil {
        log.Fatalf("error while reading %v", err)
    }

    // convert the byte into string
    fmt.Println(string(content))
}
```
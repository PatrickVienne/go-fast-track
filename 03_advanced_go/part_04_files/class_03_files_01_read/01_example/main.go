package main

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func ReadWith_os_1() {
	fmt.Println("==ReadWith_os_1==")
	// uses os.Open to open the file, read the content, and close the file again.
	dat, err := os.ReadFile("example.txt")
	check(err)
	fmt.Print(string(dat))
}

func ReadWith_os_2() {
	fmt.Println("==ReadWith_os_2==")
	f, err := os.Open("example.txt")
	check(err)
	defer f.Close()

	b1 := make([]byte, 5)
	// Read until the byte array is full, here 5 bytes
	n1, err := f.Read(b1)
	check(err)
	fmt.Printf("%d bytes: %s\n", n1, string(b1[:n1]))

	o2, err := f.Seek(6, 0)
	check(err)
	b2 := make([]byte, 2)
	n2, err := f.Read(b2)
	check(err)
	fmt.Printf("%d bytes @ %d: ", n2, o2)
	fmt.Printf("%v\n", string(b2[:n2]))

	o3, err := f.Seek(6, 0)
	check(err)
	b3 := make([]byte, 2)
	n3, err := io.ReadAtLeast(f, b3, 2)
	check(err)
	fmt.Printf("%d bytes @ %d: %s\n", n3, o3, string(b3))
}

func ReadWith_ioutil() {
	fmt.Println("==ReadWith_ioutil==")
	// ioutil.ReadFile returns a []byte, err
	// As of go 1.20, this function simply calls os.ReadFile.
	dat, err := ioutil.ReadFile("example.txt")
	check(err)
	fmt.Print(string(dat))
}

func ReadWith_bufio_NewReader() {
	fmt.Println("==ReadWith_bufio==")
	// os.Open returns a *os.File, err
	f, err := os.Open("example.txt")
	check(err)
	defer f.Close()

	// uses default buffer size of:
	// 4096
	fmt.Println("==ReadWith_bufio_NewReader==")
	r4 := bufio.NewReader(f)
	b4, err := r4.Peek(10)
	check(err)
	for _, character := range b4 {
		fmt.Printf("% x", character)
	}
	fmt.Printf("\n")
	fmt.Printf("%d bytes. Full text: %s\n", len(b4), string(b4))
}

func ReadWith_bufio_NewScanner() {
	fmt.Println("==ReadWith_bufio==")
	// os.Open returns a *os.File, err
	f, err := os.Open("example.txt")
	check(err)
	defer f.Close()

	fmt.Println("==ReadWith_bufio_NewScanner==")
	// The split function defaults to ScanLines
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		fmt.Println(err)
	}
}

func main() {
	ReadWith_os_1()
	// ReadWith_os_2()
	// ReadWith_ioutil()
	// ReadWith_bufio_NewReader()
	// ReadWith_bufio_NewScanner()
}

package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
)

func ReadCsvFileAtOnce(filename string) [][]string {
	fmt.Println("==ReadCsvFileAtOnce==")
	// Open CSV file
	fileContent, err := os.Open(filename)
	check(err)

	defer fileContent.Close()

	// Read File into a Variable
	reader := csv.NewReader(fileContent)
	reader.LazyQuotes = true
	lines, err := reader.ReadAll()
	check(err)

	return lines
}

func ReadCsvFileLineByLine(filename string) [][]string {
	fmt.Println("==ReadCsvFileLineByLine==")
	// Open CSV file
	f, err := os.Open(filename)
	check(err)

	reader := csv.NewReader(f)
	reader.LazyQuotes = true

	data := [][]string{}

	for {
		col, err := reader.Read()

		if err == io.EOF { // finish if we arrived at the end of file
			break
		}

		if err != nil {
			log.Fatal(err)
		}

		data = append(data, col)
	}
	return data
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	lines1 := ReadCsvFileAtOnce("brooklyn.csv")

	for _, line := range lines1[:10] {
		fmt.Println(line)
	}

	lines2 := ReadCsvFileLineByLine("brooklyn.csv")

	for _, line := range lines2[:10] {
		fmt.Println(line)
	}

}

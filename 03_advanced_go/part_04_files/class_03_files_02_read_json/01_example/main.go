package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type Users struct {
	Users []User `json:"users"`
}

type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"Age"`
	Social Social `json:"social"`
}

type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

func ReadWithUnmarshal(jsonFilePath string) Users {
	fmt.Println("==ReadWithUnmarshal==")
	// read our opened jsonFile as a byte array.
	bytes, err := os.ReadFile(jsonFilePath)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}

	// we initialize our Users array
	var users Users

	// we unmarshal our byteArray which contains our
	// jsonFile's content into 'users' which we defined above
	json.Unmarshal(bytes, &users)
	return users
}

func ReadWithDecode(jsonFilePath string) Users {
	fmt.Println("==ReadWithDecode==")
	// read our opened jsonFile as a byte array.
	jsonFile, err := os.Open(jsonFilePath)
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	defer jsonFile.Close()

	dec := json.NewDecoder(jsonFile)

	var users Users
	dec.Decode(&users)
	return users
}

func ReadBufferedString() {
	fmt.Println("==ReadBufferedString==")

	var data = `
	{
	  "user": "Scrooge McDuck",
	  "type": "deposit",
	  "amount": 1000000.3
	}
	`

	// Request is a bank transactions
	type Request struct {
		Login  string  `json:"user"`
		Type   string  `json:"type"`
		Amount float64 `json:"amount"`
	}

	rdr := bytes.NewBufferString(data) // Simulate a file/socket with a buffer

	// Decode request
	dec := json.NewDecoder(rdr)

	req := &Request{}
	if err := dec.Decode(req); err != nil {
		log.Fatalf("error: can't decode - %s", err)
	}
	fmt.Printf("%#v\n", req)
}

func main() {
	// So a better rule of thumb is this:
	// Use json.Decoder if your data is coming from an io.Reader stream, or you need to decode multiple values from a stream of data.
	// Use json.Unmarshal if you already have the JSON data in memory.
	res1 := ReadWithUnmarshal("users.json")
	fmt.Printf("%#v\n", res1)
	res2 := ReadWithDecode("users.json")
	fmt.Printf("%#v\n", res2)

	ReadBufferedString()
}

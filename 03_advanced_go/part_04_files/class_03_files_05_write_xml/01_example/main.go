package main

import (
	"encoding/xml"
	"fmt"
	"os"
)

type Book struct {
	Name   string `xml:"Name"`
	Author string `xml:"Author"`
}

var book = Book{Name: "War And Peace", Author: "Leo Tolstoy"}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func WriteWithMarshal(xmlFilePath string) {
	fmt.Println("==WriteWithMarshal==")

	bs, _ := xml.Marshal(book)

	err := os.WriteFile(xmlFilePath, bs, 0644)
	check(err)
}

func WriteWithEncode(xmlFilePath string) {
	fmt.Println("==WriteWithEncode==")
	// read our opened xmlFile as a byte array.
	xmlFile, err := os.Create(xmlFilePath)
	// xmlFile, err := os.Open(xmlFilePath)
	check(err)
	defer xmlFile.Close()

	enc := xml.NewEncoder(xmlFile)
	enc.Encode(book)
}

func main() {
	WriteWithEncode("book1.xml")
	WriteWithMarshal("book2.xml")
}

package main

import (
	"fmt"
	"sort"
)

type Pair struct {
	Key   string
	Value int
}

type PairList []Pair

func (p PairList) Len() int           { return len(p) }
func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p PairList) Less(i, j int) bool { return p[i].Value < p[j].Value }

func SortData(freq map[string]int) PairList {
	p := make(PairList, len(freq))

	i := 0
	for k, v := range freq {
		p[i] = Pair{k, v}
		i++
	}

	sort.Sort(sort.Reverse(p))
	return p
}

func countWords(filename string) (counts map[string]int) {
	/* TODO: define reading this file, and count the words*/
	return
}

func main() {
	counts := countWords("constitution.txt")
	sortedCounts := SortData(counts)
	fmt.Println(sortedCounts[:10])
}

package main

import (
	"encoding/json"
	"log"
	"os"
)

type Languages []Language

type Language struct {
	Id   int
	Name string
}

func (l *Languages) export(filename string) {
	f, err := os.Create(filename)
	if err != nil {
		log.Panicf("Could not create %s\n", filename)
	}

	enc := json.NewEncoder(f)
	enc.Encode(l)
}

func main() {
	languages := Languages{
		{1, "english"},
		{2, "deutsch"},
		{3, "中文"},
		{4, "日本語"},
		{5, "magyar"},
	}

	languages.export("languages.json")
}

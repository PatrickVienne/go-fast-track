# Write JSON Task

Write an `export` method for a class called `Languages` which has `Language` instances.

Test it, by making an instance with this code:

```go
	languages := Languages{
		{1, "english"},
		{2, "deutsch"},
		{3, "中文"},
		{4, "日本語"},
		{5, "magyar"},
	}
```

The export method should work with this:

```go
    languages.export("languages.json")
```

The result should be following content written into a File:
```json
[{"Id":1,"Name":"english"},{"Id":2,"Name":"deutsch"},{"Id":3,"Name":"中文"},{"Id":4,"Name":"日本語"},{"Id":5,"Name":"magyar"}]
```
# JSONS

There are 2 possibilities, given that `users` is any json-serializable type.

1. Marshaling and WriteFile:

```go
	bs, _ := json.Marshal(users)

	err := os.WriteFile(jsonFilePath, bs, 0644)
```


2. Writing a json with an encoder:

```go
	jsonFile, err := os.Create(jsonFilePath)
	// jsonFile, err := os.Open(jsonFilePath)
	check(err)
	defer jsonFile.Close()

	enc := json.NewEncoder(jsonFile)
	enc.Encode(users)
```
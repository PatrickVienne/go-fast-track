package main

import (
	"encoding/json"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

type person struct {
	Name  string  `json:"name"`
	Hobby string  `json:"hobby,omitempty"` //omitempty will remove it, if we do not have a value for hobby
	Email string  `json:"-"`               // skip
	Money float64 `json:"money,string"`    // will be saved as string and not float
	Phone string  // default mapping with same name "Phone"
}

func convertStructToJsonStringExamples() {
	p1 := person{Name: "John", Hobby: "Music"}
	bs1, _ := json.Marshal(p1)
	fmt.Println(string(bs1)) // {"name":"John","hobby":"Music","money":"0"}

	p2 := person{Name: "Jane", Email: "e@d.com"}
	bs2, _ := json.Marshal(p2)
	fmt.Println(string(bs2)) // {"name":"Jane","money":"0"}

	p3 := person{Name: "Steve", Money: 200}
	bs3, _ := json.Marshal(p3)
	fmt.Println(string(bs3)) // {"name":"Steve","money":"200"}
}

type Users struct {
	Users []User `json:"users"`
}

type User struct {
	Name   string `json:"name"`
	Type   string `json:"type"`
	Age    int    `json:"Age"`
	Social Social `json:"social"`
}

type Social struct {
	Facebook string `json:"facebook"`
	Twitter  string `json:"twitter"`
}

var users = Users{
	[]User{
		{"Elliot", "Reader", 23, Social{"https://facebook.com", "https://twitter.com"}},
		{"Elliot", "Reader", 23, Social{"https://facebook.com", "https://twitter.com"}},
	},
}

func WriteWithMarshal(jsonFilePath string) {
	fmt.Println("==WriteWithMarshal==")

	bs, _ := json.Marshal(users)

	err := os.WriteFile(jsonFilePath, bs, 0644)
	check(err)
}

func WriteWithEncode(jsonFilePath string) {
	fmt.Println("==WriteWithEncode==")
	// read our opened jsonFile as a byte array.
	jsonFile, err := os.Create(jsonFilePath)
	// jsonFile, err := os.Open(jsonFilePath)
	check(err)
	defer jsonFile.Close()

	enc := json.NewEncoder(jsonFile)
	enc.Encode(users)
}

func main() {
	convertStructToJsonStringExamples()
	WriteWithEncode("users1.json")
	WriteWithMarshal("users2.json")
}

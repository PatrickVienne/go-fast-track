package main

import (
	"testing"
)

func TestParseTime(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "ParseTest"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ParseTime()
		})
	}
}

func TestPrintTime(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "PrintTest"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			PrintTime()
		})
	}
}

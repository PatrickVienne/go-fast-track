# Error logging with 3rd party package

```
go get github.com/pkg/errors
```

This package allows to add a message to an error:
```go
errors.Wrap(err, "can't open configuration file")
```
When the error is logged, this will also add a stacktrace.


# Instructions For Task

Write a killProcess Function, which takes in a process ID file path (string) and reads the process id written in that file.

For practice:
it should then print out "terminating <processId>" to the terminal, instead of calling `os.Kill.`
With the simulated "kill", the file should also be removed.

The most important point is to have proper error handling.

do not forget about cases, such as the file does not exist, the content is not a string and potentially other sources of errors.

Print a readble error message.

Use the `github.com/pkg/errors` package, and get it with `go get`

## Criteria:

Following errors should be handled:
- **process.id** file not found
- content can not be parsed to an integer
- process id file cannot be removed.

## Hint

To read the content of the file, you can use the `ioutil` package

```go
	data, err := ioutil.ReadFile(pidFile)
```
This will read the entire data.
There should just be an integer in this file, the process ID.

To get the process id as integer, you can use these additional lines:
```go
	strPID := strings.TrimSpace(string(data))
	pid, err := strconv.Atoi(strPID)
```

For proper logging, you can reuse the function

```go
func setupLogging() {
	out, err := os.OpenFile("app.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("Cannot open File")
		return
	}
	log.SetOutput(out)
}
```
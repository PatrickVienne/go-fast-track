package main

import (
	"fmt"
	"log"
	"os"
)

func killServer(pidFile string) error {
	/* TODO: fill in code */

	return nil
}

func setupLogging() {
	out, err := os.OpenFile("app.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		fmt.Println("Cannot open File")
		return
	}
	log.SetOutput(out)
}

func main() {
	setupLogging()

	/* TODO: fill in code */
}

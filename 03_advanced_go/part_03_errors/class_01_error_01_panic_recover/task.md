# Errors, panic and recover

## Task

Complete the function given in the task, to accept user inputs:


If the user inputs `1`, print `"I am a  Student"`.

If the user inputs `2`, print `"I am a  Professional"`.

For all other inputs the function should panic.

Finally, if a panic is raised, recover from the panic, and print `"Recovered from Invalid Input"` adding the inputted action.

Task:
- Step 1: raise a panic, if user gives invalid input
- Step 2: recover from the panic, and give a human readable output

## Hint

To read an input value via standard in, use:

```go
	var action int
    fmt.Scanln(&action)
```


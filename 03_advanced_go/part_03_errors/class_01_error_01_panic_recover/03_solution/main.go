package main

import "fmt"

func ReadInput() {
	fmt.Println("Enter 1 for Student and 2 for Professional")

	/* TODO: ask user for input value */
	var action int
	fmt.Scanln(&action)

	/* TODO: catch the panic */
	defer func() {
		if action := recover(); action != nil {
			fmt.Println("Recovered Invalid Input", action)
		}
	}()

	/* TODO: use a switch case to print the correct output, else panic */
	switch action {
	case 1:
		fmt.Println("I am a  Student")
	case 2:
		fmt.Println("I am a  Professional")
	default:
		// the message in the panic can be recovered by recover()
		panic(fmt.Sprintf("I am a  %d\n", action))
	}
}

func main() {
	ReadInput()
	fmt.Println("Proper Exit")
}

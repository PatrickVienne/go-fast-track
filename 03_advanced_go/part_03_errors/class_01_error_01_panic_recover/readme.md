# Panic and Recover

The built-in type system of GO Language catches many mistakes at compile time, but unable to check mistakes like an out-of-bounds array, access or nil pointer deference which require checks at run time.

GO does not have an exception mechanism you can't throw exceptions.

When go detects a mistake:
- Go panics and stops all normal execution
- all deferred function calls in that goroutine are executed
- program crashes with a log message

This log message usually has enough information to analyze the root cause of the problem without running the program repeatedly, so it should always be included in a bug report about a panicking program.

Panic is a built-in function that stops the ordinary flow of control and begins panicking.
When the function X calls panic:
- execution of X stops
- any deferred functions in X are executed normally
- and then X returns to its caller. 
To the caller, X then behaves like a call to panic. 

The process continues up the stack until all functions in the current goroutine have returned, at which point the program crashes. 

Panics can be initiated by:
- invoking panic directly
- by run-time errors, such as out-of-bounds array accesses.

Not all panics come from the run-time. The built-in panic function may be called directly; it accepts any value as an argument. 

A panic is usually the best thing to do when some "impossible" situation happens, for instance, execution reaches a case that logically can't happen

Sometimes a `panic` can occur at runtime, just by referencing an index which does not exist:

```go
func badIndex() {
	vals := []int{1, 2, 3}
	// This will cause a panic
	v := vals[10]
	fmt.Println(v)
}
```

`panic` can be raised by choice, e.g. to propagate a failed operation.

```go
func openWrongPath() {
	file, err := os.Open("no-such-file")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	fmt.Println("file opened")
}
```

However, with the `defer` feature of golang, it is possible to recover a panic

```go
func safeValue(vals []int, index int) int {
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("ERROR: %s\n", err)
		}
	}()

	return vals[index]
}
```


Explanation of error codes:

https://pkg.go.dev/golang.org/x/tools/internal/typesinternal?utm_source=gopls#ErrorCode

package main

import (
	"fmt"
	"os"
	"time"
	//"os"
)

func main() {
	// panic with go routines
	// routines()

	// example 1: failing
	// badIndex()

	// example 2: causing panic by choice
	// openWrongPath()

	// example 3: safe function which can recover from a panic
	// v, ok := safeValue([]int{1, 2, 3}, 10)
	// fmt.Println(v, ok)

	// example 4: execute go routine and try to recover
	catchRoutine()
	fmt.Println("Done")
}

func catchRoutine() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("ERROR", err)
		}
	}()
	go badIndex()
	time.Sleep(1 * time.Second)
	fmt.Println("All good")
}

func routines() {
	go badIndex()
	time.Sleep(1 * time.Second)
	fmt.Println("All good")
}

func badIndex() {
	vals := []int{1, 2, 3}
	// This will cause a panic
	v := vals[10]
	fmt.Println(v)
}

type FileOpenError struct{}

func (e FileOpenError) Error() string { return "Problem" }

func openWrongPath() error {
	file, err := os.Open("no-such-file")
	if err != nil {
		panic(err)
		return FileOpenError{}
	}
	defer file.Close() // Python: finally:

	fmt.Println("file opened")
	return nil
}

func safeValue(vals []int, index int) (int, bool) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Printf("ERROR: %s\n", err)
		}
	}()

	return vals[index], true
}

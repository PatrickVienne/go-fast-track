// pkg/errors example
package main

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
)

func CustomFail() {
	/*
		If the format specifier includes a %w verb with an error operand,
		the returned error will implement an Unwrap method returning the operand.
		 It is invalid to include more than one %w verb or to supply it with
		 an operand that does not implement the error interface.
	*/
	_, err := os.Open("no-file")
	// func Errorf(format string, a ...interface{}) error
	errMsg := fmt.Errorf("WrappedError: %w", err)
	unwrappedErr := errors.Unwrap(errMsg)

	var perr *fs.PathError
	if errors.As(err, &perr) {
		fmt.Println("Patherror", perr.Path)
	} else {
		log.Panicln(unwrappedErr)
	}

	// use As instead of this
	if perr, ok := err.(*fs.PathError); ok {
		fmt.Println(perr.Path)
	}
}
func main() {
	CustomFail()
}

// pkg/errors example
package main

import (
	"errors"
	"fmt"
)

// Do not do this
func FailWrong() error {
	// the problem, to find identify this error, only string comparison will work in the future.
	myError := errors.New("ZeroError")
	return myError
}

// but do this, define an exportable variable for the specific error.
var ZeroError = errors.New("ZeroError")

func FailRight() error {
	// the problem, to find identify this error, only string comparison will work in the future.
	myError := ZeroError
	return myError
}

func main() {
	err1 := FailWrong()
	fmt.Printf("ErrorType: %T, Msg: %s\n", err1, err1.Error())
	err2 := FailRight()
	fmt.Printf("ErrorType: %T, Msg: %s\n", err2, err2.Error())

	if errors.Is(err1, ZeroError) {
		fmt.Printf("err1 is ZeroError\n")
	}
	// then this comparison will be possible:
	if errors.Is(err2, ZeroError) {
		fmt.Printf("err2 is ZeroError\n")
	}
}

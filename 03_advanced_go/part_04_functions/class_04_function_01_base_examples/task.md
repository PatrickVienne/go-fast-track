# Function Tasks

## Task 1: Fibonacci
Implement a fibonacci function that returns a function (a closure) that returns successive fibonacci numbers (0, 1, 1, 2, 3, 5, ...).

### Hint
There are many ways to implement this.
Try to implement it with the suggested way, using successive function calls to get the next fibonacci number.
Thereby, creating something as a fibonacci number generator.

## Task 2: Prime Numbers Generator

Write a prime number calculator, which outputs all prime numbers up to a number "n" (x<1000)

You can check prime numbers here:
https://www.calculatorsoup.com/calculators/math/prime-numbers.php

### Hint
No Conditions here, you can go for the simplest bruteforce method, however try to combine leanred knowledge about datatypes and loops

## Task 3: Caesar Cipher

Implement a caesarCipher function, which modifies a string by applying the cipher to all alphabetical characters, which substitutes a character by a shift of "n".

e.g. n=3
a->d
b->e
A->D

Only consider a-z and A-Z characters for the shift.

https://www.dcode.fr/caesar-cipher

Ascii Table
https://www.asciitable.com/


### Hint:

1. Modulus

If you intend to use modulus operator, wtch out that the `%` of a negative number may still be a negative number in golang.


To mitigate this (if needed) you may need to correct this to get a positive number:
```go
	shift %= NUM
	if shift < 0 {
		shift += NUM
	}
```


2. Ascii

Please double check the byte and integer values of the ascii character you are trying to shift.

Find good conditions to restrict the shift only to alphabetic characters which the algorithm needs to shift.

### Bonus Task:

You find the following code on a secret server, can you decipher it?

```go
    code := "Lbh penpxrq gur pbqr!"
```
package main

import "fmt"

// This is given as a help, do not change
const (
	LOWER = "abcdefghijklmnopqrstuvwxyz"
	UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	NUM   = len(LOWER)
)

// Do not change the function signature
func caesarCipher(cipher string, shift int) (text []byte) {
	// since only single byte ascii characters are used, it is safe to use len
	text = make([]byte, len(cipher))

	/*
		TODO: implement cipher
		https://www.asciitable.com/
	*/

	return
}

func main() {
	fmt.Println("==DECODING==")
	textToDecode := "Wihalunofuncihm, sio uly nby vymn!"
	ceasarShift := -20
	decoded := string(caesarCipher(textToDecode, ceasarShift))
	fmt.Println(decoded)

	fmt.Println("==ENCODING==")
	encoded := string(caesarCipher(decoded, -ceasarShift))
	fmt.Println(encoded)

	fmt.Println("==Validate==")
	fmt.Println("Success: ", textToDecode == encoded)
}

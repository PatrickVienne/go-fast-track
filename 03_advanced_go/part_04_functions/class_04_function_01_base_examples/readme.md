# Functions

Naming conventions:

- A name must begin with a letter, and can have any number of additional letters and numbers.
- A function name cannot start with a number.
- A function name cannot contain spaces.
- If the functions with names that start with an uppercase letter will be exported to other packages. If the function name starts with a lowercase letter, it won't be exported to other packages, but you can call this function within the same package.
- If a name consists of multiple words, each word after the first should be capitalized like this: empName, EmpAddress, etc.
- function names are case-sensitive (car, Car and CAR are three different variables).

in this section you will find example for:

- Simple call
- Passing arguments
- Accepting return value in varaible
- named return values1
- updating variables
- call anonymous function area
- Closures, Anonymous function accessing the variable defined outside body.
- Higher Order Functions in Golang
- Function Closure, shared variables
- Defer Examples
- Function Closure with defer
- variadic functions
- combine variadic argument with normal argument
- optionalArguments (There is no real GoLang way of doing this)

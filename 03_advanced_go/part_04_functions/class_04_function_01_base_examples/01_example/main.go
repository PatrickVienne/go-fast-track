package main

import (
	"fmt"
	"os"
)

// SimpleFunction prints a message
func SimpleFunction() {
	fmt.Println("Hello World")
}

// Passing arguments
func add(x int, y int) {
	total := 0
	total = x + y
	fmt.Println(total)
}

// Function with int as return type
func add2(x int, y int) int {
	total := 0
	total = x + y
	return total
}

// named return values, and multiple values
func rectangle(l int, b int) (area int, parameter int) {
	parameter = 2 * (l + b)
	area = l * b
	return // Return statement without specify variable name
}

// passing addresses
func update(a *int, t *string) {
	*a = *a + 5      // dereferencing pointer address
	*t = *t + " Doe" // dereferencing pointer address
	// Return statement not needed
}

// anonymous function defined for this package

var (
	area = func(l int, b int) int {
		return l * b
	}
)

// higher order functions

func sum(x, y int) int {
	return x + y
}
func partialSum(x int) func(int) int {
	return func(y int) int {
		return sum(x, y)
	}
}

// defer examples

func NoDefer() bool {
	file, _ := os.Open("filepath.txt")

	failureX := false
	failureY := false

	if failureX {
		file.Close() //And here...
		return false
	}
	if failureY {
		file.Close() //And here...
		return false
	}
	file.Close() //And here...
	return true
}

func WithDefer() bool {
	file, _ := os.Open("filepath.txt")
	defer file.Close()

	failureX := false // example condition
	failureY := false

	if failureX {
		return false
	}
	if failureY {
		return false
	}
	return true
}

func first() {
	fmt.Println("First")
}
func second() {
	fmt.Println("Second")
}
func third() {
	fmt.Println("Third")
}

func combineDefers() {
	defer second()
	defer third()
	first()
}

// Closure Examples

func myClosureCounter(startval int) func() int {
	startval--
	return func() int {
		startval++
		return startval
	}
}

func myClosureCounterWithDefer(startval int) func() int {
	return func() int {
		defer func() {
			startval++
		}()
		return startval
	}
}

func variadicExample1(s ...string) {
	fmt.Println(s[0])
	fmt.Println(s[3])
	fmt.Println("All: ", s)
}

func variadicExample2(text string, s ...string) {
	// this does not work anymore: "variadicExample2(moreStrings[:]...)", need to pass first text
	fmt.Println(text)
	fmt.Println(s[0])
	fmt.Println(s[3])
	fmt.Println("All: ", text, s)
}

// types as arguments
type FullName [2]string

func showName(fullName FullName) {
	fmt.Println("First Name: ", fullName[0], "Last Name: ", fullName[1])
}

// Optional Argument Example Code

type Config struct {
	color       string
	width       int
	length      int
	addDefaults bool
}

func NewDefaultConfig() Config {
	return Config{"yellow", 12, 12, true}
}

func (c *Config) check() {
	// proper way would be to have a builder pattern for this.
	if c.addDefaults {
		if c.color == "" {
			c.color = "yellow"
		}
		if c.width == 0 {
			c.width = 12
		}
		if c.length == 0 {
			c.length = 12
		}
		c.addDefaults = false
	}
}

func optionalArguments(text string, opt Config) {
	// this does not work anymore: "variadicExample2(moreStrings[:]...)", need to pass first text
	fmt.Println(text)
	opt.check()
	fmt.Println("All: ", text, opt)
}

// Task1: Create Fibonacci number generator
// Task2: (if Task1 Done): Create Prime Number generator

func main() {
	// // Simple call
	// SimpleFunction()

	// // Passing arguments
	// add(20, 30)

	// // Accepting return value in varaible
	// sum := add2(20, 30)
	// fmt.Println(sum)

	// // named return values1
	// a, p := rectangle(20, 30)
	// fmt.Println("Area:", a)
	// fmt.Println("Parameter:", p)

	// // updating variables
	// var age = 20
	// var text = "John"
	// fmt.Println("Before:", text, age)
	// update(&age, &text)
	// fmt.Println("After :", text, age)

	// // call anonymous function area
	// // Anonymous functions can be used for containing functionality that need not be named and possibly for short-term use.
	// fmt.Println(area(20, 30))
	// func() { fmt.Println("Print from inside anonymous function") }()

	// // Higher Order Functions in Golang
	// // A Higher-Order function is a function that receives a function as an argument or returns the function as output.
	// // Higher order functions are functions that operate on other functions, either by taking them as arguments or by returning them.
	// partial := partialSum(3)
	// fmt.Println(partial(7))

	// // Defer Examples
	// WithDefer()
	// combineDefers()

	// // Closures, Anonymous function accessing the variable defined outside body.
	// l, b := 20, 30
	// func() { area := l * b; fmt.Println(area) }()

	// // Function Closure, shared variables between calls
	// f := myClosureCounter(10)
	// fmt.Println(f())
	// fmt.Println(f())
	// fmt.Println(f())

	// // Function Closure with defer
	// g := myClosureCounterWithDefer(20)
	// fmt.Println(g())
	// fmt.Println(g())
	// fmt.Println(g())

	// // variadic functions
	// moreStrings := [7]string{"orange", "purple", "teal", "red", "blue", "green", "yellow"}
	// variadicExample1("red", "blue", "green", "yellow")
	// // need to unpack the slice, since this would not work with arrays
	// variadicExample1(moreStrings[:]...)

	// // combine variadic argument with normal argument
	// variadicExample2("red", "blue", "green", "yellow")
	// variadicExample2(moreStrings[:]...)

	// // Use custom types as Argument
	// showName(FullName{"Peter", "Pan"})

	// // optionalArguments (There is no real GoLang way of doing this)
	// optionalArguments("Rectangle", Config{})
	// optionalArguments("Rectangle", Config{"green", 0, 0, true})

}

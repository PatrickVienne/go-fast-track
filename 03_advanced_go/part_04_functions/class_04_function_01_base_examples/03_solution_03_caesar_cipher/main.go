package main

import "fmt"

const (
	LOWER = "abcdefghijklmnopqrstuvwxyz"
	UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	NUM   = len(LOWER)
)

func caesarCipher(cipher string, shift int) (text []byte) {
	// since only single byte ascii characters are used, it is safe to use len
	text = make([]byte, len(cipher))

	// watch out, modulo in GoLang follows the protocol of C/C++ instead of Python
	// "%" here is more the "remainder" than a "modulus" as in Python
	// so we will need an additional shift, if the shift is negative
	shift %= NUM
	if shift < 0 {
		shift += NUM
	}
	fmt.Printf("Shift by: %d, To Shift: %s\n", shift, cipher)

	for i := 0; i < len(cipher); i++ {
		c := cipher[i]
		b := byte(c)
		if (c >= LOWER[0] && c <= LOWER[(NUM-shift-1)%NUM]) || (c >= UPPER[0] && c <= UPPER[(NUM-shift-1)%NUM]) {
			b += byte(shift)
		} else if (c >= LOWER[(NUM-shift)%NUM] && c <= LOWER[NUM-1]) || (c >= UPPER[(NUM-shift)%NUM] && c <= UPPER[NUM-1]) {
			b += byte(shift - NUM)
		}
		// for any other, just leave b what it is

		text[i] = b
	}
	return
}

func main() {
	fmt.Println("==DECODING==")
	textToDecode := "Wihalunofuncihm, sio uly nby vymn!"
	ceasarShift := -20
	decoded := string(caesarCipher(textToDecode, ceasarShift))
	fmt.Println(decoded)

	fmt.Println("==ENCODING==")
	encoded := string(caesarCipher(decoded, -ceasarShift))
	fmt.Println(encoded)

	fmt.Println("==Validate==")
	fmt.Println("Success: ", textToDecode == encoded)
}

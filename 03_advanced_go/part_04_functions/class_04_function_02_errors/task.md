# Errors Task

Sqrt should return a non-nil error value when given a negative number, as it doesn't support complex numbers.

Define an `ErrNegativeSqrt` type which implements the `error` interface by implementing the `Error() string` function
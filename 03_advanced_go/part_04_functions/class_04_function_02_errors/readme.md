# Errors


## output error of http call

We write a function which return the content type of a passed url

If the page has no content type, the function should return an error

Example call:
```
go run . https://www.google.com
```

Example output:
```
text/html; charset=ISO-8859-1
```

Example call:
```
go run main.go hhtp://google.com
```

Example output:
```
ERROR: Get "hhtp://google.com": unsupported protocol scheme "hhtp"
```

## create function and define error for division by zero


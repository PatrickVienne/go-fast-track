// Writing a function that return Content-Type header
package main

import (
	"fmt"
	"net/http"
	"os"
)

// contentType will return the value of Content-Type header returned by making an
// HTTP GET request to url
func getContentType(url string) (contentType string, err error) {
	resp, err := http.Get(url)

	if err != nil {
		return "", err
	}
	// Close body after the function is finished
	defer resp.Body.Close()

	contentType = resp.Header.Get("Content-Type")
	if contentType == "" { // Return error if Content-Type header not found
		return "", fmt.Errorf("can't find Content-Type header")
	}
	return
}

func main() {
	url := os.Args[1]
	contentType, err := getContentType(url)
	if err != nil {
		fmt.Printf("ERROR: %s\n", err)
	} else {
		fmt.Println(contentType)
	}
}

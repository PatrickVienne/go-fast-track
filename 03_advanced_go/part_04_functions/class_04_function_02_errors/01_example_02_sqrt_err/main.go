package main

import (
	"errors"
	"fmt"
	"math"
)

var NegativeSquareRootErr = errors.New("NegativeSquareRootErr")

type InvalidNumberError struct {
	Code    string
	Message string
	Err     error
}

func (e *InvalidNumberError) Error() string {
	return "Code: " + e.Code + ": " + e.Message + " " + e.Err.Error()
}

const (
	NegativeSquareRootErrCode = "ERR0024"
)

func sqrt(n float64) (float64, error) {
	if n < 0 {

		// ok
		// return 0.0, fmt.Errorf("sqrt of negative value (%f)", n)

		// better
		// return 0.0, NegativeSquareRootErr

		// best
		return 0.0, &InvalidNumberError{Code: NegativeSquareRootErrCode, Message: "Tried to devide by zero", Err: NegativeSquareRootErr}
	}

	return math.Sqrt(n), nil
}

func main() {
	s1, err := sqrt(2.0)
	if err != nil {
		fmt.Printf("ERROR: %s\n", err)
	} else {
		fmt.Println(s1)
	}

	s2, err := sqrt(-2.0)
	if err != nil {

		// option 1
		// fmt.Printf("ERROR: %s\n", err)

		// option 2:
		// using special errors, allows to easily compare, without having to compare strings
		// switch err {
		// case NegativeSquareRootErr:
		// 	fmt.Printf("ERROR-NegativeSquareRootErr: %s\n", err)
		// 	// add this, if you still want to also use the default case
		// 	// fallthrough
		// default:
		// 	fmt.Printf("ERROR: %s\n", err)
		// }

		//option 3:
		if err.Error()[:7] == NegativeSquareRootErr {
			fmt.Printf("ERROR-NegativeSquareRootErr: %s\n", err)
		} else {
			fmt.Printf("ERROR: %s\n", err)
		}

	} else {
		fmt.Println(s2)
	}
}

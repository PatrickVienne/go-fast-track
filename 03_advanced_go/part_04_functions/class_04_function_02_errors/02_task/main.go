package main

import (
	"fmt"
	"math"
)

func Sqrt(x float64) /* define the signature */ {

	// TODO: Implement here a check for negative numbers, and create an error which gives a good error message

	// first approximation
	z := float64(1)
	tolerance := 1e-6

	for {
		// linear approximation step
		oldz := z
		// (z*z - x), how much off are we
		// (2 * z), derivative of z*z -> slope to get best step size for next approximation step
		z -= (z*z - x) / (2 * z)

		// abort the loop, if our approximation is close enough,
		// and the step we made is lower than the tolerance
		if math.Abs(z-oldz) < tolerance {
			break
		}
	}

	// TODO: Return the solution, without error

}
func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(-2))
}

# IO package

https://golangdocs.com/io-package-in-golang

The io package provides two very fundamental types the Reader and Writer. The reader provides a function that simply reads bytes from streams. The writer is just the opposite.

https://medium.com/learning-the-go-programming-language/streaming-io-in-go-d93507931185
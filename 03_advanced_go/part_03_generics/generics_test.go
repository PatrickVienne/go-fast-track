package generics

import (
	"testing"
)

func TestUseGenerics(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "Generics"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			UseGenerics()
		})
	}
}

// TODO
// https://www.youtube.com/watch?v=M-ofC23gSZ4&ab_channel=SamxSmith
// debugging
// https://www.youtube.com/watch?v=xmWqd1LBa4E
package main

import (
	"fmt"
	"time"
)

func rateLimiter() {
	maxCalls := 3
	maxTimeframePerCall := 200 * time.Millisecond

	burstyLimiter := make(chan time.Time, maxCalls)

	go func() {
		for t := range time.Tick(maxTimeframePerCall) {
			burstyLimiter <- t
		}
	}()

	i := 0
	for val := range burstyLimiter {
		fmt.Println(i, val)
		if i%3 == 0 {
			time.Sleep(time.Duration(1) * time.Second)
		}
		i++
	}
}

func main() {
	rateLimiter()
}

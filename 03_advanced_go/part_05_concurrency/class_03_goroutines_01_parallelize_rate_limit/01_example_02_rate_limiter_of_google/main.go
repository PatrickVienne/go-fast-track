// TODO
// https://www.youtube.com/watch?v=M-ofC23gSZ4&ab_channel=SamxSmith
// debugging
// https://www.youtube.com/watch?v=xmWqd1LBa4E
package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"golang.org/x/time/rate"
)

func rateLimiter() {
	l := rate.NewLimiter(rate.Every(1*time.Second), 20)
	fmt.Println(l.Limit(), l.Burst())
	for {
		// allow checks if an event is allowed according to the limiter.
		// if yes, it will return true and also add another event as happened to the limiter
		if l.Allow() {
			log.Println("log:event happen", time.Now().Format(time.RFC1123))
		} else {
			time.Sleep(400 * time.Millisecond)
			log.Println("log:event not allow", time.Now().Format(time.RFC1123))
		}

	}
}

func rateLimiterWithWait() {
	// every second, but 5 can be made in a burst
	// this will then use up the rate for the next 5 seconds.
	l := rate.NewLimiter(rate.Every(1*time.Second), 5)

	fmt.Printf("Limits set: %f, burst: %d\n", l.Limit(), l.Burst())
	for {
		// wait adds to the events, but also sleeps until the needed events are available
		l.WaitN(context.TODO(), 3)
		log.Println("log:event happen", time.Now().Format(time.RFC1123))
	}
}

func rateLimiterWithDelayAndReserve() {
	// every second, but 5 can be made in a burst
	// this will then use up the rate for the next 5 seconds.
	l := rate.NewLimiter(rate.Every(1*time.Second), 5)

	fmt.Printf("Limits set: %f, burst: %d\n", l.Limit(), l.Burst())
	for {
		// reserve spot for 3 actions
		// the reservation also in effect adds 3 events to the event count,
		// however the developer can decide how to handle the delay given by the reservation.
		r := l.ReserveN(time.Now(), 3)
		if !r.OK() {
			return
		}

		sleepTime := r.Delay()
		log.Println("log:sleeping to wait until request is allowed: ", sleepTime)
		time.Sleep(sleepTime)

		log.Println("log:event happen", time.Now().Format(time.RFC1123))

	}
}

func main() {
	rateLimiter()
	//rateLimiterWithDelayAndReserve()
	//rateLimiterWithWait()
}

# Mutex

https://golangdocs.com/mutex-in-golang

Before we write that program, let’s see what is mutex actually? A mutex is simply a mutual exclusion in short. It means that when multiple threads access and modify shared data before they do that they need to acquire a lock. Then when the work is done they release the lock and let some other goroutine to acquire the lock.

This allows the goroutines to have synchronized access to the data and prevents data race.

```go
// working of a mutex:
 
// acquire lock
// modify data
// release lock
```

It may be confusing that mutexes are very similar to atomic operations but they are much more complicated than that. Atomics utilize CPU instructions whereas mutexes utilize the locking mechanism. So when updating shared variables like integers, atomics are faster. But the real power of mutexes comes when the complex structure of data is handled concurrently. Then it is the only option since atomics don’t support that.

## rate limiter

`go get golang.org/x/time/rate`
package main

import (
	"bufio"
	"fmt"
	"log"
	"net/http"
	"strings"

	// "sync"
	"time"

	"golang.org/x/time/rate"
)

var (
// wg sync.WaitGroup
)

func GetWebsiteData(retrievedData chan<- string) {
	res, err := http.Get("https://httpbin.org/delay/1")
	if err != nil {
		log.Fatal(err)
	}
	// defer wg.Done()
	defer res.Body.Close()

	// option 1, using scanner
	b := strings.Builder{}
	scanner := bufio.NewScanner(res.Body)
	for scanner.Scan() {
		// without newlines
		b.Write(scanner.Bytes())

		// with new lines added back
		// b.Write(append(scanner.Bytes(), '\n'))

		// or
		// b.WriteString(scanner.Text())
	}
	data := b.String()

	// option 2, using ioutil
	// body, err := ioutil.ReadAll(res.Body)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// data := string(body)

	retrievedData <- data

}

func RequestWithLimit(retrievedData chan<- string, concurrentCalls int, rateLimit rate.Limit, burst int) {
	l := rate.NewLimiter(rateLimit, burst)
	fmt.Printf("Limits set: %f, burst: %d\n", l.Limit(), l.Burst())
	for {

		r := l.ReserveN(time.Now(), concurrentCalls)
		if !r.OK() {
			return
		}

		sleepTime := r.Delay()
		log.Println("log:sleeping to wait until request is allowed: ", sleepTime)
		time.Sleep(sleepTime)

		for i := 0; i < concurrentCalls; i++ {
			// wg.Add(1)
			go GetWebsiteData(retrievedData)
		}
		// wg.Wait()

		log.Println("log:event happen", time.Now().Format(time.RFC1123))

	}
}

func main() {
	retrievedData := make(chan string)
	concurrentRequests := 3
	burstLimit := 5
	limitFreq := rate.Every(1 * time.Second)
	maxPackage := 30
	fmt.Println("== START WEBREQUESTS ==")
	fmt.Printf("== CONCURRENT: %d ==\n", concurrentRequests)
	go RequestWithLimit(retrievedData, 3, limitFreq, burstLimit)
	i := 0
	log.Println("== DATA ==")
	for data := range retrievedData {
		log.Println("package: ", i, "bytes: ", len(data))
		i++
		if i >= maxPackage {
			close(retrievedData)
		}
	}
	log.Println("==========")
}




package main

// Task: KillProcess with Error Handling
func main() {
	/*
		Be creative, describe a function which has a race condition issue
		use the golang race condition detector to prove that you created a race condition.
	*/

	// Idea: https://jsonplaceholder.typicode.com/guide/
	// make 50 parallel requests to load pictures from jsonplaceholder
	// look at: https://jsonplaceholder.typicode.com/albums/1/photos
}

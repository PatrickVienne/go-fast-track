package cas

import (
	"runtime"
	"sync/atomic"
)

type SpinLock struct {
	state *int32
}

const free = int32(0)

func (l *SpinLock) Lock() {
	for !atomic.CompareAndSwapInt32(l.state, free, 42) { // any other non-zero value, 42 is just an example
		runtime.Gosched() // poke the scheduler
	}
}

func (l *SpinLock) Unlock() {
	atomic.StoreInt32(l.state, free) // once atomic always atomic
}

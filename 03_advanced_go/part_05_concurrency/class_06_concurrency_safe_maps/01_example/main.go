package main

import (
	"fmt"
	"sync"
)

type Store struct {
	sync.RWMutex
	M map[string]string
}

func (s *Store) Get(key string) (string, bool) {
	s.RLock()
	value, ok := s.M[key]
	s.RUnlock()
	return value, ok
}

func (s *Store) Put(key string, value string) error {
	s.Lock()
	s.M[key] = value
	s.Unlock()
	return nil
}

func (s *Store) Delete(key string) error {
	s.Lock()
	delete(s.M, key)
	s.Unlock()
	return nil
}

func main() {
	store := Store{M: make(map[string]string)}
	store.Put("A", "HiGolang")
	value, _ := store.Get("A")
	fmt.Println(value)
}

# Instructions For Task

Write a CLI tool, which asks for a command to be entered.

If a command starting with `hi` is entered, the command will be treated as high priority.

If any other command is entered, then the command is treated as normal priority.

Als long as there are high priority tasks, they should be treated first.

## Hint:

use some utility functions.
- `strings.HasPrefix` to check how a string starts
- `strings.ToLower(cmd)` to make show that you compare without case sensitivity
- `fmt.Scanln(&cmd)` in an eternal for loop, until broken, can be used to repeatedly ask for the users input
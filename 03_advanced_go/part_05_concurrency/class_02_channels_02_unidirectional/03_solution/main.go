// Get content type of sites (using channels)
package main

import (
	"fmt"
	"strings"
	"time"
)

func HandleTask(lowPrioChannel, highPrioChannel chan string) {
	var data string
	for { // forever
		select {
		case data = <-highPrioChannel:
			fmt.Printf("Hi Prio Channel output: %s %d %d\n", data, len(highPrioChannel), len(lowPrioChannel))
		default:
			select {
			case data = <-highPrioChannel:
				fmt.Printf("Hi Prio Channel output: %s %d %d\n", data, len(highPrioChannel), len(lowPrioChannel))
			case data = <-lowPrioChannel:
				fmt.Printf("Low Prio Channel output: %s %d %d\n", data, len(highPrioChannel), len(lowPrioChannel))
			}
		}
		time.Sleep(3 * time.Second)
	}
}

func main() {
	// Create response channel
	lowPrioChannel := make(chan string, 3)
	highPrioChannel := make(chan string, 3)

	// create a variable to hold the entered command
	var cmd string

	// start goroutine which will handle the commands
	go HandleTask(lowPrioChannel, highPrioChannel)

	// start endless loop, asking for the user's commands
	for {
		fmt.Print("\nPlease enter your command ('close' to stop): ")
		fmt.Scanln(&cmd)

		if cmd == "close" {
			close(lowPrioChannel)
			close(highPrioChannel)
			break
		} else if strings.HasPrefix(strings.ToLower(cmd), "hi") {
			// handle the request by adding it to the channel
			go func() { highPrioChannel <- cmd }()
		} else {
			go func() { lowPrioChannel <- cmd }()
		}
	}
}

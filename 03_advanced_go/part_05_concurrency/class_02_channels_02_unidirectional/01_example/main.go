// Golang program to convert the bidirectional channel
// into the unidirectional channel

package main

import "fmt"

// input is a send-only channel
func ConvertToUnidirection(uniCh chan<- string) {
	uniCh <- "Hello World"

	// Inside the ConvertToUnidirection() function
	// channel is unidirectional.

	// Below statement will generate error
	// fmt.Println(<-uniCh)

	// this line would also not work,
	// because uniCh is a send-only, and we cannot receive from this channel
	// aa := <- uniCh
}

func main() {
	// Create a bidirection channel
	msg := make(chan string)

	go ConvertToUnidirection(msg)

	// Outside the ConvertToUnidirection() function
	// channel is bidirectional.
	fmt.Println(<-msg)
}

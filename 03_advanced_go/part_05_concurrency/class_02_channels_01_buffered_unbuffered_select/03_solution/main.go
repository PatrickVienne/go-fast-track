// Get content type of sites (using channels)
package main

import (
	"fmt"
)

func PrintResults(ch chan string) {
	for out := range ch { // Run number of URLs times
		fmt.Printf("Channel output: %s\n", out)
	}
	fmt.Println("Channel closed")
}

func main() {
	// Create response channel
	ch := make(chan string)

	// create a variable to hold the entered command
	var cmd string

	// start goroutine which will handle the commands
	go PrintResults(ch)

	// start endless loop, asking for the user's commands
	for {
		fmt.Print("\nPlease enter your command ('close' to stop): ")
		fmt.Scanln(&cmd)

		if cmd == "close" {
			close(ch)
			break
		} else {
			// handle the request by adding it to the channel
			go func() { ch <- cmd }()
		}
	}
}

# Instructions For Task

Write a CLI tool, which asks for a command to be entered.

After each time a command is entered, the command should be dealt with asynchronously, and the main program should ask the user for the next command.

When the command is ready, an output should give back a message that the command has been processed.

Upen command 'close' the user should be able to stop the program and quit.
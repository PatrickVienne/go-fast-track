# CHANNELS

## Info
Presentation by Arne Claus at Golang UK 2017: https://youtu.be/YEKjSzIwAdA

> Main ideas of Golang, concurrency in details:
> - no group code (and data) by identifying independent tasks
> - no race conditions
> - no deadlocks
> - more workers = faster execution

> Communicating Sequential Processes (CSP)
> - Tony Hoare 1978 (https://cs.stanford.edu/people/eroberts/courses/soco/projects/2008-09/tony-hoare/csp.html)
> - each process is built for sequential execution
> - Data is communicated between processes via channels, no sharing -> no deadlocks / no race conditions
> - Scale by adding more fo the same

> Writing parallel code is very hard, CSP says, write sequential code instead!

## Go Tools
- go routines
- channels
- select
- sync package (why do we need this, does not sound like concurrency)

## Channels

Think of channels as a link between sender and receiver, or a waterpipe which needs and supplier (sender) and a consumer (receiver).

Once the pipe is full, trying to put more water it will block.

Once the pipe is empty, trying to get more water out will block.

In Golang words, we distinguish between 2 types of channels. Buffered and Unbuffered.

### Unbuffered

This is a blocking code, since we will be waiting for data to come
```go
unbuffered := make(chan int)
a := <- unbuffered
```
So we send something to the channel. But this will block again, because nobody takes the value which was entered. 
```go
unbuffered := make(chan int)
unbuffered <- 1
```

We can start a routine, that will take 1 element from the channel.
We can synchronize in this way.
```go
go func() { <- unbuffered } ()
unbuffered <- 1
```

### Buffered
What does this do?
```go
buffered := make(chan int, 1)
a := <- buffered
```

It blocks! because there is no data

What if we send data?
What does this do?
```go
buffered := make(chan int, 1)
buffered <- 1
```
This time it works, because the channel is buffered, so the go routine can continue execution.
But!

```go
buffered := make(chan int, 1)
buffered <- 1
buffered <- 2
```
This blocks again

> Blocking breaks concurrency!
>
> Contradiction to inital ideas?
 

- Promise of
    - no-deadlocks
    - more workers = faster execution
- Blocking can lead to deadlocks
- Blocking can prevent scaling


### Closing channels (buffered or unbuffered)

- close sends a special **closed** message
- the receiver will at some point see **closed** and finishing.
- if you try to send more: **panic!**
    - Why? Because data would be lost, since no receiver will read from a closed channel.
    - What if a closed channel is closed again? Answer: **panic!** Closing generates a message, and sending a message on a closed channel panics.

Question: what does this print?
```go
c := make(chan int)
close(c)
fmt.Println(<- c)  // since closing generated a message, we can receive 1 message
```
Answer:
```0, false```
The first is the close message, which is the zero value of the type. for `int` it is `0`, for a pointer `*[]byte` it would be `nil`

The second parameter is always there, but we usually ignore it. `false` means, no more data, or data is invalid. It shows that the channel is closed.

> Important: Receiver always gets notified if a channel gets closed, but the sender does not. So it is important to always close a channel from the sender side.

### Select

What if we have more than 1 channel?

We need select.
- Like Switch statement on channel operations
- The order of cases does not matter at all
- Has default
- First non-blocking case is chosen


So we can write a function, which tries to receive, but also returns a default value when nothing could be received.

```go
func TryReceive(c chan int) (data int, more, ok bool) {
	select {
	case data, more = <-c:
		return data, more, true
	default: // done when c is blocking
		return 0, true, true
	}
}
```

But this is not much useful, in reality there is a certain throughput and you would like to wait for the new message to come.

So we can give it a timeout

```go
import "time"

func TryReceive(c chan int, duration time.Duration) (data int, more, ok bool) {
	select {
	case data, more = <-c:
		return data, more, true
	case <-time.After(duration): // time.After returns a channel, which gets a message sent after the timeout.
		return 0, true, true
	}
}
```

`time.After(duration)` returns a channel. And this channel will be blocking immediately, and gets a message after some time. So we have a receive with a timeout.

### `Select` to shape data flow

- Channel are streams of data
- `Select` great tool, true power, to organize streams

Fan-Out
```
      ---o
     /
o---o---o
     \
      ---o
```

Funnel

```
o--
   \
o---o---o
   /
o--
```

Turn-Out
```
o--   ---o
   \ /
o---o---o
   / \
o--   ---o
```

> Code Examples:

We have a for-loop, which executes each time a new message is sind across the channel.
Forloop finishes when the channel is closed, and when the channel is closed, the last message will not be processed anymore, so the outgoing channels will not receive the close message in setup shown below:


```go
func Fanout(In <-chan int, OutA, OutB, chan int) {
	// Could also call this load balancer
	for data := range In {  // receive until closed
		select {  // sends to first non-blocking channel
		case OutA<-data:
		case OutB<-data:
		}
	}
}
```

Data is sent to the first channel which has place available

If we continue with the Turnout, the first design might look something like this:

What could be the issue with the following code?
```go
func Turnout(InA, InB <-chan int, OutA, OutB, chan int) {
	// Could also call this load balancer
	for {  // forever
		select {  // receive from 1st non-blocking
		case OutA, more <- InA:
		case OutB, more <- InB:
		}
		if !more {  // stop as soon as 1 channel is closed
			// ... reasonable?
			return
		}
		select {  // send to 1st non-blocking, like fanout
		case OutA <-data:
		case OutB <-data:
		}
	}
}
```
The problem is, that even though1 input channel is finished, the other input channel might still have data, which will not be forwarded anymore.

So we might want to add a quit channel

```go
func Turnout(Quit <-chan int, InA, InB, OutA, OutB, chan int) {
	for {  // forever
		select {  // receive from 1st non-blocking
		case OutA, more <- InA:
		case OutB, more <- InB:

		case <- Quit:
			close(InA)  // this will not call the case on top, since first this select case has to be processed until the end.
			close(InB)  // however, this still is an antipattern

			Fanout(InA, OutA, OutB)
			Fanout(InB, OutA, OutB)
			return
		}
	}
}
```

Using this quit channel is one way to do it.
The only reason for this channel to exist is to be closed. And this would then close all other channels.
This is antipattern, because we said the receiver should never close. 
But in this case, the sender says quit and we can call the channel a delegate, since the sender is still aware of the close due to its calling close.

This can cover about 90% of use cases.

### Where Channels Fail
- Channels can lead to deadlocks
- Channels pass around copies, which can impact performance
- passing pointers to channels can create race conditions
- How about naturally shared structures like caches and registries?

Channels are blocking as discussed previously, and using them without care can quickly lead to deadlocks.

Channels pass around copies, since we want to have separated local states to avoid shared states and race conditions. But this can be a problem if we share large scale structs.

Passing pointers instead, would then again introduce race conditions, since nobody stops multiple routines modifying the content of the same address.

You can build cache with channel, but it does not make much sense, since it contradicts.

### `sync` should help here

Mutexes can help, but have other issues:
- The longer it is held, the longer the queue of waiting routines trying to access the variable.
- Read/Write locks Can only reduce the problem
- multiple mutexes will cause deadlock sooner or later
- all-in-all not the solution we want.

Read/Write locks only reduce the issue, and only are helpful when there are a lot more reads then writes.

With multiple mutexes, there are usually always orders implied, and at some point somebody will mess up the order.

> 3 Types of Code:
> - Blocking (could wait for an undefined amount of time)
> - Lock Free (At least one part of the program is always progressing)
> - Wait Free (All parts always make progress, but very hard to achieve)
>
> This does not mean that wait free is faster than blocking

So what can help?

**Atomic Operations** in the `sync` package.

Key Features:

- sync.atomic package
- Store, Load, Add, Swap and CompareAndSwap (these are CPU instructions)
- Mapped to thread-safe CPU-instructions
- These only work for integer types
- Only about 10-60 times slower than non-atomic types

Drawbacks, there are only these few operations.

So what we can do, are a few patterns for lockless concurreny

## Concurrency Patterns

### Spinning Compare-And-Swap (CAS)

```go
import (
	"runtime"
	"sync/atomic"
)

type SpinLock struct {
	state *int32
}

const free = int32(0)

func (l *SpinLock) Lock() {
	for !atomic.CompareAndSwapInt32(l.state, free, 42) { // any other non-zero value, 42 is just an example
		runtime.Gosched() // poke the scheduler
	}
}

func (l *SpinLock) Unlock() {
	atomic.StoreInt32(l.state, free) // once atomic always atomic
}
```
This is mostly what a lock does.
Because CompareAndSwap is atomic, we know that only one routine can have the lock at a time.
However it is also important to call Gosched() and not to do anything also, since this routine may become blocking on a single core cpu.

Since the scheduler is called, this now does not block (as opposed to what a mutix would have done.)

### Ticket Storage

- We need an indexed data structure, a ticket and a done variable
- A function draws a new ticket by adding 1 to the ticket
- Every Ticket number is unique as we never decrement
- Ticket is an index under which the data is stored
- Increase done, to extend the "ready to read" range


```go
import (
	"runtime"
	"sync/atomic"
)

// datastore to save the numbers of tickets and done tickets
type TicketStore struct {
	ticket *uint64
	done   *uint64
	slots  []string
}

func (ts *TicketStore) Put(s string) {
	t := atomic.AddUint64(ts.ticket, 1) - 1
	ts.slots[t] = s
	for !atomic.CompareAndSwapUint64(ts.done, t, t+1) {
		runtime.Gosched()
	}

}

func (ts *TicketStore) GetDone() []string {
	return ts.slots[:atomic.LoadUint64(ts.done)+1] //read up to done
}

```

The part which keeps us from being wait-free, because it waits for the integer to be swappable:
```go
	for !atomic.CompareAndSwapUint64(ts.done, t, t+1) {
		runtime.Gosched()
	}
```

But this is important to avoid a race condition.
Example:
1. Routine1: Ticket 1 gets assigned as number to variable `ticket`.
2. Routine2: Ticket 2 gets assigned as number to variable `ticket`.
3. Routine2: ticket gets stored in slots with index 2
4. Routine2: When the ticket is done, done is increased.
> Problem: Now we have marked 1 as done, but ticket 1 is not yet processed.

### Guieline to write non-blocking code

- Don't switch between atomic and non-atomic functions (e.g. if atomic function, use only atomic operations and types)
- Target and exploit situations which enforce uniqueness (e.g. you are the only one that has-state/can-change/...)
- Avoid changing two things at a time
    - Sometimes: exploit bit operations (bit operations, two 32bit values in one 64bit integer)
    - Sometimes: intelligent ordering helps
    - Sometimes it's just not possible

### Final thoughts on Concurrency

- Avoid blocking and race conditions (easiest to do this by sticking to CSP)
- Use channels to avoid shared state.
- Use select to manage channels.
- Where channels don't work:
    - Try to use tools form the sync package
    - In simple cases, or when really needed, try lockless code.

## Debugging in Concurrency
https://youtu.be/xmWqd1LBa4E

Debugging sequential programs is already hard sometimes, concurrent programs is even harder.

using delve debugger


// TODO
// https://www.youtube.com/watch?v=M-ofC23gSZ4&ab_channel=SamxSmith
// debugging
// https://www.youtube.com/watch?v=xmWqd1LBa4E
package main

import (
	"fmt"
	"time"
)

func timerLeak() {
	timer := time.NewTimer(5 * time.Second)
	ticker := time.NewTicker(1 * time.Second)

	// if this line misses, then the timer will just continue ticking in the goroutine, even after this function exists.
	// defer ticker.Stop()

	done := make(chan bool)

	go func() {
		for {
			select {
			case <-ticker.C:
				fmt.Println("Tick!")
			case <-done:
				return
			}
		}
	}()

	<-timer.C

	fmt.Println("It's time!")
	close(done)
}
func main() {
	timerLeak()
}

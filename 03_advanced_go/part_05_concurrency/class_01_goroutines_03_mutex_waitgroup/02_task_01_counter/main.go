package main

func asyncIncrement( /* TODO */ ) {
	/* TODO: make a threadsafe increment of a passed integer */
}

func RunIncrement() {
	// integer to be incremented in each async go routine
	var v int = 0

	/* TODO: declare the necessary variables to make this work */

	for i := 0; i < 1000; i++ {
		/* TODO: add code here if necessary */
		go asyncIncrement( /* TODO: pass the arguments you need */ )
	}

	/* TODO: add code here if necessary, and print the final value of `v`*/
}

func main() {
	RunIncrement()
}

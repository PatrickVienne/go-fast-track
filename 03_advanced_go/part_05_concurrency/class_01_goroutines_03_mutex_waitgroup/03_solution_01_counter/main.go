package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

// TRY 1
// before you continue to try2, look at the code of function f() and TryNo1()
// what may be the issue, what output do you expect, and how to fix it?
func f(v *int, wg *sync.WaitGroup) {
	*v++
	wg.Done()
}

func TryNo1() {
	var wg sync.WaitGroup
	var v int = 0

	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go f(&v, &wg)
	}

	wg.Wait()
	fmt.Println("Finished", v)
}

// TRY 2
func f2(v *int, wg *sync.WaitGroup, m *sync.Mutex) {
	// acquire lock
	m.Lock()
	// do operation
	*v++
	// release lock
	m.Unlock()
	wg.Done()
}

func TryNo2() {
	var wg sync.WaitGroup
	// declare mutex
	var m sync.Mutex
	var v int = 0

	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go f2(&v, &wg, &m)
	}

	wg.Wait()
	fmt.Println("Finished", v)
}

// TRY 3
func f3(v *int32, wg *sync.WaitGroup) {
	defer wg.Done()
	atomic.AddInt32(v, 1)
}

func TryNo3() {
	var wg sync.WaitGroup
	var v int32 = 0

	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go f3(&v, &wg)
	}

	wg.Wait()
	fmt.Println("Finished", v)
}

func main() {

	// TryNo1()
	// TryNo2()
	TryNo3()
}

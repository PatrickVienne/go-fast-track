# Golang goroutines - Mutex - Parallelism

## Task 1:

implement an asynchronous counter, which will increment a shared counter to a final number, which equals the asynchronous function calls.

the variable to increment is:
```go
var v int = 0
```

### Hint:

Use `sync.WaitGroup`, `sync.Mutex`, `sync.Add` and `sync.Done` to make sure that the program executes all calls, and waits until all calls finished.

Also use the locks, to guarantee that a thread owns a resource while accessing and modifying it.


## Task 2:

Example to turn synchronous requests into parallel ones to reduce time blocked by waiting for responses.

Make the given code threadsafe.


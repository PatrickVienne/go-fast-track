# Structs / Interface

## Task 1: Implement Lower

Create an implementation to io writer which turns everything into lowercase that is written with it.

Call it `Lower`

Try to find the most effective implementation possible. Try it with the text: "Hello World! Ready? ☺"

Hint:

you can find the ascii table here:

https://www.asciitable.com/

## Task 2: Implement a RotReader

Implement a rotReader that implements io.Reader and reads from an io.Reader, modifying the stream by applying the caeser cipher substitution to all alphabetical characters.

This is a modification to "Tour of Go: Exercise: rot13Reader"

https://go.dev/tour/methods/23

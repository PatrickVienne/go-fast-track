package main

import (
	"fmt"
	"io"
	"os"
)

type Lower struct {
	wtr io.Writer
}

func ( /* TODO */ ) Write( /* TODO */ ) /* TODO */ {
	/* TODO */
}

func main() {
	// to print decimal and binary representation of a number:
	// fmt.Printf("Decimal value: %d\n", num)
	// fmt.Printf("Binary value : %b\n", num)

	l := &Lower{os.Stdout}
	text := "Hello World! Ready? ☺"

	fmt.Println("Original", text)
	fmt.Fprintln(l, "Lower: "+text)

}

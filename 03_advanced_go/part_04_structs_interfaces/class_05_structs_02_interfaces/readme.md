# Implement io.writer

Interface

https://jordanorelli.com/post/32665860244/how-to-use-interfaces-in-go

In this example, we will reimplement `io.Writer` with a writer, that can replace all characters with uppercase characters

ASCII table:
https://www.binaryhexconverter.com/binary-ascii-characters-table


We will use a type
```go
type Upper struct {
	wtr io.Writer
}
```

So that we can call it with the  `fmt.Fprintln` function:

```go
	c := &Upper{os.Stdout}
	text := "Hello World! Ready? ☺"

	fmt.Println("Original", text)
	fmt.Fprintln(c, "Upper: "+text)
```

The `Upper` type needs to implement the `Write` Method:
```go

func (c *Upper) Write(p []byte) (n int, err error) {
	for i, c := range p {
		// only modify lowercase characters
		if 97 <= c && c <= 122 {
			p[i] &= 0b11011111
		}
	}
	return c.wtr.Write(p)
}
```

Since the difference between upper and lowercase characters is 32, which in binary is `00100000`, we can reduce all characters by 32 using an `AND` operation with `0b11011111`.



package main

import (
	"io"
	"os"
	"strings"
)

const (
	LOWER = "abcdefghijklmnopqrstuvwxyz"
	UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	NUM   = len(LOWER)
)

type rotReader struct {
	r     io.Reader
	shift int
}

func (rd *rotReader) Read(b []byte) (n int, e error) {
	n, e = rd.r.Read(b)
	shift := rd.shift
	shift %= NUM
	if shift < 0 {
		shift += NUM
	}

	for i := 0; i < len(b); i++ {
		c := b[i]
		if (c >= LOWER[0] && c <= LOWER[(NUM-shift-1)%NUM]) || (c >= UPPER[0] && c <= UPPER[(NUM-shift-1)%NUM]) {
			b[i] += byte(shift)
		} else if (c >= LOWER[(NUM-shift)%NUM] && c <= LOWER[NUM-1]) || (c >= UPPER[(NUM-shift)%NUM] && c <= UPPER[NUM-1]) {
			b[i] += byte(shift - NUM)
		}
	}

	return
}

func main() {
	s := strings.NewReader("Lbh penpxrq gur pbqr!")
	r := rotReader{s, -13}
	io.Copy(os.Stdout, &r)
}

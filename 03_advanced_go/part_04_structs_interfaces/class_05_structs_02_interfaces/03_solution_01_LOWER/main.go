package main

import (
	"fmt"
	"io"
	"os"
)

type Lower struct {
	wtr io.Writer
}

func (c *Lower) Write(p []byte) (n int, err error) {
	for i, c := range p {
		// only modify uppercase characters
		if 65 <= c && c <= 90 {
			p[i] |= 0b00100000
		}
	}
	return c.wtr.Write(p)
}

func main() {
	// to print decimal and binary representation of a number:
	// fmt.Printf("Decimal value: %d\n", num)
	// fmt.Printf("Binary value : %b\n", num)

	l := &Lower{os.Stdout}
	text := "Hello World! Ready? ☺"

	fmt.Println("Original", text)
	fmt.Fprintln(l, "Lower: "+text)

}

package main

import (
	"fmt"
	"strings"
)

type Robot struct {
	purpose  string
	todolist []string
	battery  int
}

// - work
// - showOpenTasks

func (r *Robot) charge() {
	r.battery++
}

func (r *Robot) addTask(task string) {
	r.todolist = append(r.todolist, task)
}

func (r *Robot) work() (string, error) {
	if len(r.todolist) == 0 {
		return "", fmt.Errorf("No more tasks to do")
	} else if r.battery == 0 {
		return "", fmt.Errorf("Battery is empty")
	} else {
		task := r.todolist[0]
		fmt.Println("Working on task: ", task)

		r.todolist = r.todolist[1:]
		return task, nil
	}
}

func (r *Robot) showOpenTasks() {
	fmt.Printf("Leftover Tasks: %s\n", strings.Join(r.todolist, ","))
}

func newRobot(purpose string) *Robot {
	return &Robot{battery: 100, todolist: []string{}, purpose: purpose}
}

func main() {
	// creating an instance
	robot := newRobot("Help humasn")
	robot.addTask("Wash Dishes")
	robot.addTask("Code Fizzbuzz")
	robot.addTask("Train Developers")
	robot.work()
	robot.work()
	robot.charge()
	robot.showOpenTasks()
}

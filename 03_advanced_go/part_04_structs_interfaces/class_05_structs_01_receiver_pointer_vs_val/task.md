# Structs More Challenge

Define a struct Robot

It should have the fields:
- purpose
- battery
- todolist

Create a helper Constructor NewRobot, which creates a new robot given a purpose, and always assigns the value 100 for battery

add 2 methods:
- charge
- addTask
- work
- showOpenTasks

Explanations:
- `charge` takes no parameters, but adds 1 to the battery as long as the battery does not reach 100
- `addTask` adds 1 string entry to a todolist
- `work` will take 1 element of the todolist, do it (and thereby remove it from the todolist again), and this also decreases the battery by 1.

Final Code:
- Make 1 robot
- add 3 tasks to the todolist
- let the robot make 2 tasks
- finally let the robot show the leftover tasks it has


package main

import (
	"encoding/json"
	"fmt"
	"os"
	"reflect"
)

type T struct {
	name string
}

func newT() interface{} {
	var u *T = nil
	return u
}

type School struct {
	Id   interface{}
	Name string
}

func DeepEqual(v1, v2 interface{}) bool {
	if reflect.DeepEqual(v1, v2) {
		return true
	}
	var x1 interface{}
	bytesA, _ := json.Marshal(v1)
	_ = json.Unmarshal(bytesA, &x1)
	var x2 interface{}
	bytesB, _ := json.Marshal(v2)
	_ = json.Unmarshal(bytesB, &x2)
	if reflect.DeepEqual(x1, x2) {
		return true
	}
	return false
}

func compareWithCustomDeepEqual() {
	x := School{
		Id:   1,
		Name: "Golang Public School",
	}
	bytes, err := json.Marshal(x)
	if err != nil {
		println("error while unmarshalling the json ", err)
		os.Exit(1)
	}
	var y School
	err = json.Unmarshal(bytes, &y)
	if err != nil {
		println("error while unmarshalling the json ", err)
		os.Exit(1)
	}
	if DeepEqual(x, y) {
		println("both x & y are same")
	} else {
		println("x & y are different")
	}
	fmt.Println("value of x is ", x)
	fmt.Println("value of y is ", y)
}

func compareWithDeepEqual() {
	x := School{
		Id:   1,
		Name: "Golang Public School",
	}
	bytes, err := json.Marshal(x)
	if err != nil {
		println("error while unmarshalling the json ", err)
		os.Exit(1)
	}
	var y School
	err = json.Unmarshal(bytes, &y)
	if err != nil {
		println("error while unmarshalling the json ", err)
		os.Exit(1)
	}
	if reflect.DeepEqual(x, y) {
		println("both x & y are same")
	} else {
		println("x & y are different")
	}
	fmt.Println("value of x is ", x)
	fmt.Println("value of y is ", y)
}

func ComparisonExamples() {
	// m := 1
	// n := 1.0
	// fmt.Println(m == n) // this line would even

	s := struct{ name string }{"foo"}
	t := T{"foo"}
	u := newT() // internally, as an interface is returned, we have a type + pointer pair, which is used for the comparison
	var v *T = nil
	var w *struct{} = nil
	fmt.Println(s == t) // true
	fmt.Println(1 == 1.0)
	fmt.Println(u == nil, fmt.Sprintf("%T, %v", u, u)) // false *main.T, <nil>
	fmt.Println(v == nil, fmt.Sprintf("%T, %v", v, v)) // true *main.T, <nil>
	fmt.Println(w == nil, fmt.Sprintf("%T, %v", w, w)) // true *struct {}, <nil>
}

func nilValues() {
	// // each type has its default value

	// var i int  // 0
	// var t bool // false

	// // all reference types are initialized with nil

	// var s []int             // s == nil -> true
	// var m map[string]string // m == nil -> true
	// var p *int              // p == nil -> true
	// var c chan int          // c == nil -> true
	// var f func()            // f == nil -> true

	// type T struct {
	// 	p *int
	// }
	// var t T // t.p == nil -> true

	// var i interface{} // i == nil -> true

	// var a = nil // compile error: use of untyped nil
}

func nilComparisons() {
	var p *int        // (type=*int,value=nil)
	var i interface{} // (type=nil,value=nil)

	if i != nil { // a hardcoded nil is always nil,nil (type,value)
		fmt.Println("i (interface{}) is not a nil")
	} else {
		fmt.Println("i (interface{}) is a nil") // this
	}

	i = p // assign p to i, now i will be (type=*int,value=nil)

	if i != nil {
		fmt.Println("(p *int assigned to i) is not a nil") // this
	} else {
		fmt.Println("(p *int assigned to i) is a nil")
	}

	if p != nil {
		fmt.Println("p *int is not a nil")
	} else {
		fmt.Println("p *int is a nil") // this
	}

	//But! here this translates to compiler doing p == (*int)(nil)
	fmt.Println("p *int is nil?", p == nil) // prints `true`

	// With interface the compiler is not sure about the underlying type because it can change any time
	// (you can assign string and then pointer to the same interface variable)
}

func main() {
	nilComparisons()
	// ComparisonExamples()
	// compareWithCustomDeepEqual()
	// compareWithDeepEqual()
}

// Program in GO language to demonstrates how to use base log package.
package main

import (
	"log"
	"net/smtp"
	"os"
)

func init() {
	filePtr, err := os.OpenFile("mylogs.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}

	//Set log output to the "mylogs.log" file.
	log.SetOutput(filePtr)
	log.SetPrefix("LOG: ")
	// Llongfile: full file name and line number: /a/b/c/d.go:23
	// Lshortfile:  final file name element and line number: d.go:23. overrides Llongfile
	log.SetFlags(log.Ldate | log.Lmicroseconds | log.Llongfile)
	log.Println("init started")
	// log message generated:
	// LOG: 2022/03/03 10:55:23.480879 C:/code/gitlab.com/golangteam2022/gobasics/class_09_t_logs/00_train/main.go:11: init started
}
func main() {
	// Println writes to the standard logger.
	log.Println("main started")

	// Fatalln is Println() followed by a call to os.Exit(1)
	log.Fatalln("fatal message")

	// Panicln is Println() followed by a call to panic()
	log.Panicln("panic message")

	client, err := smtp.Dial("smtp.smail.com:25")
	if err != nil {
		log.Fatalln(err)
	}
	client.Data()
}

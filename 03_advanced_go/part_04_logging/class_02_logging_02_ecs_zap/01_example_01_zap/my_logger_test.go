package my_logger

import "testing"

func TestRunLogger(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "run-logger-1"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			RunLogger()
		})
	}
}

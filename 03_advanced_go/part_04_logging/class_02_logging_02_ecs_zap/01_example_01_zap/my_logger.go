package my_logger

import (
	"io"
	"os"
	"time"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

func RunLogger() {
	// Using zap's preset constructors is the simplest way to get a feel for the
	// package, but they don't allow much customization.

	// option 1:
	// NewExample builds a Logger that's designed for use in zap's testable examples.
	// It writes DebugLevel and above logs to standard out as JSON, but omits the timestamp and calling function to keep example output short and deterministic.
	// example output: {"level":"info","msg":"Failed to fetch URL.","url":"http://example.com","attempt":3,"backoff":"1s"}

	logger := zap.NewExample()

	// option 2:
	// NewProduction builds a sensible production Logger that writes InfoLevel and above logs to standard error as JSON.
	// example output: {"level":"info","ts":1646946297.2961655,"caller":"01_example_01_zap/main.go:26","msg":"Failed to fetch URL.","url":"http://example.com","attempt":3,"backoff":1}

	// logger, err := zap.NewProduction()
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	// option 3:
	// NewDevelopment builds a development Logger that writes DebugLevel and above logs to standard error in a human-friendly format.
	// example output:
	// 2022-03-10T22:06:28.706+0100    INFO    01_example_01_zap/main.go:35    Failed to fetch URL.    {"url": "http://example.com", "attempt": 3, "backoff": "1s"}

	// logger, err := zap.NewDevelopment()
	// if err != nil {
	// 	log.Fatalln(err)
	// }

	// flush the logs
	defer logger.Sync()

	const url = "http://example.com"

	// In most circumstances, use the SugaredLogger. It's 4-10x faster than most
	// other structured logging packages and has a familiar, loosely-typed API.
	sugar := logger.Sugar()
	sugar.Infow("Failed to fetch URL.",
		// Structured context as loosely typed key-value pairs.
		"url", url,
		"attempt", 3,
		"backoff", time.Second,
	)
	sugar.Infof("Failed to fetch URL: %s", url)

	// In the unusual situations where every microsecond matters, use the
	// Logger. It's even faster than the SugaredLogger, but only supports
	// structured logging.
	logger.Info("Failed to fetch URL.",
		// Structured context as strongly typed fields.
		zap.String("url", url),
		zap.Int("attempt", 3),
		zap.Duration("backoff", time.Second),
	)

	// use case: log to file, but also to console.
	syncer := zap.CombineWriteSyncers(os.Stdout, getWriteSyncer("example.log"))

	// NewCore creates a Core that writes logs to a WriteSyncer.
	core := zapcore.NewCore(zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()), syncer, zap.NewAtomicLevelAt(zap.InfoLevel))
	newLogger := zap.New(core)
	newLogger.Info("Failed to fetch URL.",
		// Structured context as strongly typed fields.
		zap.String("url", url),
		zap.Int("attempt", 3),
		zap.Duration("backoff", time.Second),
	)

}

type WriteSyncer struct {
	io.Writer
}

func getWriteSyncer(logName string) zapcore.WriteSyncer {
	var ioWriter = &lumberjack.Logger{
		Filename:   logName,
		MaxSize:    10, // MB
		MaxBackups: 3,  // number of backups
		MaxAge:     28, //days
		LocalTime:  true,
		Compress:   false, // disabled by default
	}

	return zapcore.AddSync(ioWriter)
}

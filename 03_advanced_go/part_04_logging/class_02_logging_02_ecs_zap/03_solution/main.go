package main

import (
	"fmt"
	"io"
	"os"
	"strconv"

	"go.elastic.co/ecszap"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

var (
	logger *zap.Logger
)

type WriteSyncer struct {
	io.Writer
}

func getWriteSyncer(logName string) zapcore.WriteSyncer {
	var ioWriter = &lumberjack.Logger{
		Filename:   logName,
		MaxSize:    10, // MB
		MaxBackups: 3,  // number of backups
		MaxAge:     28, //days
		LocalTime:  true,
		Compress:   false, // disabled by default
	}

	return zapcore.AddSync(ioWriter)
}

func init() {
	// use case: log to file, but also to console.
	syncer := zap.CombineWriteSyncers(os.Stdout, getWriteSyncer("example.log"))

	// NewCore creates a Core that writes logs to a WriteSyncer.
	//NewAtomicLevelAt is a convenience function that creates an AtomicLevel and then calls SetLevel with the given level.
	encoderConfig := ecszap.NewDefaultEncoderConfig()
	core := ecszap.NewCore(encoderConfig, syncer, zap.NewAtomicLevelAt(zap.InfoLevel))
	logger = zap.New(core, zap.AddCaller())
	logger = logger.With(zap.String("domain", "Domain"))
}

func fizzbuzz(num int) {
	for i := 1; i <= num; i++ {
		if i%3 == 0 && i%5 == 0 {
			logger.Info("fizz buzz")
		} else if i%3 == 0 {
			logger.Info("fizz")
		} else if i%5 == 0 {
			logger.Info("buzz")
		} else {
			logger.Info(strconv.Itoa(i))
		}
	}
}

func main() {
	var num int

	// Input a string
	fmt.Println("Input a value for fizzbuzz: ")
	n, err := fmt.Scanln(&num)
	if err != nil {
		logger.Error(err.Error())
	}
	logger.Info("starting",
		zap.Int("user-value", n),
	)
	fizzbuzz(num)
	logger.Info("finished",
		zap.String("state", "finished"),
	)
}

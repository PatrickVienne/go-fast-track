# Log Zap

To run the example, you will need to download the zap package:

```
go get go.uber.org/zap
```

Sources:
https://pkg.go.dev/go.uber.org/zap

https://github.com/uber-go/zap


Previously, logrus was recommended, now zap just gives much better performance and is more flexible.
There also is a good integration with elasticsearch, elk stack.

elastic offers good introductions to logging with a package based on zap: ecszap
https://www.elastic.co/guide/en/ecs-logging/go-zap/current/setup.html


## Getting started

One important point is the difference between SugaredLogger and a simple Logger.

The SugaredLogger allows for loosely typed values.

Ex1:

```go
sugar := zap.NewExample().Sugar()
defer sugar.Sync()
sugar.Infow("failed to fetch URL",
  "url", "http://example.com",
  "attempt", 3,
  "backoff", time.Second,
)
sugar.Infof("failed to fetch URL: %s", "http://example.com")
```

However, Performance can be improved by about 20%, if no sugar is used, i.e. if predefined zap types are used for logging:

```go
logger := zap.NewExample()
defer logger.Sync()
logger.Info("failed to fetch URL",
  zap.String("url", "http://example.com"),
  zap.Int("attempt", 3),
  zap.Duration("backoff", time.Second),
)
```

And it is quite easy to change between both

```go
logger := zap.NewExample()
defer logger.Sync()
sugar := logger.Sugar()
plain := sugar.Desugar()
```

## ZapCore

An additional core package allows more customization:
https://pkg.go.dev/go.uber.org/zap/zapcore

```go
	encoderConfig := ecszap.EncoderConfig{
		EncodeName:     customNameEncoder,
		EncodeLevel:    zapcore.CapitalLevelEncoder,
		EncodeDuration: zapcore.MillisDurationEncoder,
		EncodeCaller:   ecszap.FullCallerEncoder,
	}
```
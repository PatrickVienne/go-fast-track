# Fizzbuzz with logging

## Task

Take the fizzbuzz program, and extend it with logging, which logs to the console, but also logs into a file, using lumberjack and ecszap

try to use following statements at least once:
- `logger.Info`
- `zap.CombineWriteSyncers`           // to get a logger which logs to std out, and also to the file
- `ecszap.NewDefaultEncoderConfig()`  // use standard json configuration
- `ecszap.NewCore`                    // add a new core
- `zap.AddCaller`                     // add info about the caller in the logs
- `zap.New`                           // constructor for the logger
- `logger.With`                       // add structured fields
- `zap.String`                        // field

	// use case: log to file, but also to console.
	syncer := zap.CombineWriteSyncers(os.Stdout, getWriteSyncer("example.log"))

	// NewCore creates a Core that writes logs to a WriteSyncer.
	//NewAtomicLevelAt is a convenience function that creates an AtomicLevel and then calls SetLevel with the given level.
	encoderConfig := ecszap.NewDefaultEncoderConfig()
	core := ecszap.NewCore(encoderConfig, syncer, zap.NewAtomicLevelAt(zap.InfoLevel))
	logger = zap.New(core, zap.AddCaller())
	logger = logger.With(zap.String("domain", "Domain"))


for convenience, the go.mod file is already adjusted with all necessary dependencies:
```
module main

go 1.21

require (
	go.elastic.co/ecszap v1.0.1
	go.uber.org/zap v1.21.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)

require (
	github.com/BurntSushi/toml v1.0.0 // indirect
	github.com/magefile/mage v1.9.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
```

## Hint

Think about using a global variable as logger:

```go
var (
	logger *zap.Logger
)
```

and initializing the logger for the package in an `init()` function

```go
func init() {
	// TODO
}
```
package main

import (
	"errors"
	"os"

	"go.elastic.co/ecszap"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func DefaultLogging() {
	// option 1
	encoderConfig := ecszap.NewDefaultEncoderConfig()
	core := ecszap.NewCore(encoderConfig, os.Stdout, zap.DebugLevel)
	logger := zap.New(core, zap.AddCaller())

	// Add fields and a logger name
	logger = logger.With(zap.String("custom", "foo"))
	logger = logger.Named("mylogger")

	// Use strongly typed Field values
	logger.Info("some logging info",
		zap.Int("count", 17),
		zap.Error(errors.New("boom")))
}

func CustomLogging() {

	encoderConfig := ecszap.EncoderConfig{
		EncodeLevel:    zapcore.CapitalLevelEncoder,
		EncodeDuration: zapcore.MillisDurationEncoder,
		EncodeCaller:   ecszap.FullCallerEncoder,
	}
	core := ecszap.NewCore(encoderConfig, os.Stdout, zap.DebugLevel)
	logger := zap.New(core, zap.AddCaller())

	// Add fields and a logger name
	logger = logger.With(zap.String("custom", "foo"))
	logger = logger.Named("mylogger")

	// Use strongly typed Field values
	logger.Info("some logging info",
		zap.Int("count", 17),
		zap.Error(errors.New("boom")))
}

func main() {
	DefaultLogging()
	CustomLogging()
}

package main

func main() {
	// why is an unused variable a problem, but an unused constant not?
	var i = 100
	const j = 100
}

// Answer:
// This is because constants in Go are calculated at compile time and cannot have any side-effects.
// This makes them easy to eliminate if they are unused and they are not included in the compiled binary.

package main

import (
	"fmt"
)

func DoesNotWork() {
	// values cannot be initialized with nil
	var data string = nil
	if data == nil {
		fmt.Println(data)
	}
}

func Works() {
	// but pointers can. The zero value of pointers is nil.
	var data *string = nil
	if data == nil {
		fmt.Println(data)
	}
}

func main() {
	Works()
}

package main

import (
	"fmt"
)

func main() {
	// Question, what is the output of these 2 calculations?
	// same or different result?
	var m = 1.39
	fmt.Println(m * m)

	const n = 1.39
	fmt.Println(n * n)
}

// Answer: Output is:
// 1.9320999999999997
// 1.9321
// IEEE 754 single-precision binary floating-point format: binary32
// reason goes back to: there is a negative zero: https://hackernoon.com/negative-zero-bbd5fd790af3

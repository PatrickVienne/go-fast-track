package ex2

import (
	"fmt"
	"strconv"
	"strings"
)

type ErrorCode uint64

func (c ErrorCode) String() string {
	return strconv.Itoa(int(c))
}

type Error struct {
	Code    ErrorCode
	Message string
	Detail  interface{}
}

func (e Error) Error() string {
	return fmt.Sprintf("%s: %s",
		strings.ToLower(strings.Replace(e.Code.String(), "_", " ", -1)), e.Message)
}

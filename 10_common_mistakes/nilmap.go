package main

import "fmt"

func main() {
	var rect map[string]int
	rect["height"] = 10
	// this will create a panic, why? can you solve it?
	fmt.Println(rect["height"])
}

// SOL, line 7 tries to assign to a nil map, the map has to be initialized first, before any assignment can be done.
// however, for nil maps, many functions still return at least the zero value of the expected type
func thisWorks() {
	var rect map[string]int
	fmt.Println(rect["height"])
	fmt.Println(len(rect))

	idx, key := rect["height"]
	fmt.Println(idx)
	fmt.Println(key)
}

// maps can be initialized immediately, to have values for a wanted key
func Sol() {
	var rect = map[string]int{"height": 10}
	fmt.Println(rect["height"])
}

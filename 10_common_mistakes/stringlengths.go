package main

import (
	"fmt"
	"unicode/utf8"
)

func main() {
	data := "We♥Go"
	fmt.Println(len(data))
	// this output: 7
	// would you have expected this? Why?

	// Answer: it shows the necessary number of bytes.
	// ascii characters only need 1 byte, but the utf character ♥ has 3 bytes: 0xE2 0x99 0xA5
	// so instead, it makes sense to get the number of runes
	fmt.Println(utf8.RuneCountInString(data))
}

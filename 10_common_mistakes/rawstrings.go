package main

import "fmt"

func main() {
	s := `Go\tJava\nPython`
	fmt.Println(s)
	// this will print
	// Go\tJava\nPython

	// Raw strings are enclosed in back-ticks `.
	// Here, \t and \n has no special meaning, they are considered as backslash with t and backslash with n.
	// If you need to include backslashes, double quotes or newlines in your string, use a raw string literal.
	text := "Go\tJava\nPython"
	fmt.Println(text)
	// Output
	// Go      Java
	// Python
}

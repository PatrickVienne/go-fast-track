# CHANNELS

## Info

## Common Mistakes:

### Not accepting interfaces

> **State&Behaviour** 
> - Types can express state&behaviour. 
> - State = data-structure
> - behaviour = methods

> **Interfaces&Behaviour** 
> - One of Go's most powerful features
> - Permits extensibility
> - Defined by methods
> - Adherence is only satisfied behaviour

Don't do this:
```go
b := new(bytes.Buffer)
b.Write(page.Source.Content)
page.SaveSource(b.Bytes(), path)

func (page *Page) saveSource(by []byte, inpath string) {
	writeToDisk(inpath, bytes.NewReader(by))
}
```

Instead use io.Reader interface
```go
b := new(bytes.Buffer)
b.Write(page.Source.Content)
page.SaveSource(b, path)

func (page *Page) saveSource(b io.Reader, inpath string) {
	writeToDisk(inpath, b)
}
```


### Not using io.Reader and io.Writer

> **io.Reader and io.Writer** 
> - Simple and flexible Interfaces for many operations around input and output
> - provides access to a huge wealth of functionality
> - Keeps operations extensible

Interfaces are really simple
```go
type Reader interface {
	Read(p []byte) (n int, err error)
}

type Write interface {
	Write(p []byte) (n int, err error)
}
```

Example from cobra CLI commander
```go
// If output is nil, os.Stderr is used.
func(c *Command) SetOutput(o io.Writer) {
	c.output = o
}
```
This line is extremely powerful, and allows us to define any kind of output, logging systems,

### 3 Requiring interfaces

> **interfaces are composable** 
> - Functions should only accept interfaces that require the functions they need
> - Functions should not accept interfaces when a narrow one would work
> - Compose broad interfaces from narrower ones

example afero fs, a composed interface for files

Inside afore, we can define a File interface
```go
type File interface {
	io.Closer
	io.Reader
	io.ReaderAt
	io.Seeker
	io.Writer
	io.WriterAt
}
```
And it is tempting, when we define a function, to just reuse this interface. E.g.
```go
func ReadIn(f File) {
	b := []byte{}
	n, err := f.Read(b)
	...
}	
```
but maybe we do not want to implement the entire File interface just to call this function, which does not really need other interfaces except the reader, so it is better to do this:

```go
func ReadIn(r Reader) {
	b := []byte{}
	n, err := r.Read(b)
	...
}	
```

### 4 Methods vs Functions

> **Too many methods** 
> - overuse of methods
> - natural draw to define everything via structs and methods

> **Function** 
> - Operation peformance on N1 inputs that result in N2 outputs
> - Same input give same results
> - Should not depend on state

> **Method** 
> - Defines behaviour of a type
> - function operating against a value
> - Should use state
> - Logically connected

> **Functions can be used with interfaces** 
> - Methods by definition are bound to a specific type
> - Functions can accept interfaces as input

Example, why should this not be a method?
```go

func extractShortCodes(s string, p *Page, t Template) (string, map[string]shortcode error) {
	//...
	for {
		switch currItem.typ {
		//...
		case tError:
			err := fmt.Errorf("%s: %d: %s",
				   p.BaseFileName(),
				   (p.lineNumRawContentStart() * pt.lexer.lineNum() - 1), currItem)
			//...
		}
	}
}
```
Answer, it does not alter the state.
Many times, creating methods is more about logical grouping, than actually being methods.

### 5 Pointers vs Values


> **Pointers vs Values** 
> - Not question of performance generally, but of shared access
> - pointer: if you want to share the value with a function or method
> - value: if you don't want to share it

Which one should we use?
```go
func (p *Page) do (b io.Reader)
func (p Page) do (b io.Reader)
```

> **Pointer Receivers** 
> - pointer: if you want to share a value with it's method
> - pointer: state management
> - Not safe!

> **Value Receivers** 
> - value: if you want to copy a value
> - value: if type is empty struct, just use value
> - safe for concurrent access

Example: We use pointer here because we modify state
```go
type InMemoryFile struct {
	at     int64
	name   string
	data   []byte
	closed bool
}

func (f *InMemoryFile) Close() error {
	atomic.StoreInt64(&f.at, 0)
	f.closed = true
	return nil
}
```

Example from the time package, use value, because time is ticking.

```go
type InMemoryFile struct {
	sec  int64
	nsec uintptr
	loc  *Location
}

func (t Time) IsZero() bool {
	return t.sec == 0 && t.nsec == 0
}
```

### 6 Thinking of errors as strings

Error is an Interface.
Error is NOT a type.

```go
type error interface {
	Error() string
}
```

Easy to create standard errors:
```go

import "errors"

// this is the issues
func Fail() error {
	myError := errors.New("Error here")
	return myError
}

```

Example from the hugo package
Easy to create standard errors:
```go

import "errors"

func NewPage(name string) (p *Page, err error) {
	if len(name) == 0 {
		// here is the issue:
		// this forces devs to than compare the string value
		return nil, errors.New("Zero length page name")
	}
	//...
}

```

Improvement:
```go
import "errors"

// provide exported Error, now devs can check value, and not the string of the error.
var ErrNoName = errors.New("Zero")

func NewPage(name string) (p *Page, err error) {
	if len(name) == 0 {
		// here is the issue:
		// this forces devs to than compare the string value
		return nil, ErrNoName
	}
	//...
}
```

> **Custom Errors** 
> - Guarantee Context, and consistent feedback
> - Provide type, which is  
> - safe for concurrent access

Internationalization of Errors.
Recent Pull Request for docker repository, decouple Error Code from error message.
Allow message in different languages.

And still adhere to Error interface by providing the Error() method providing a string.

```go
import (
	"fmt"
	"strconv"
	"strings"
)

type ErrorCode uint64

func (c ErrorCode) String() string {
	return strconv.Itoa(int(c))
}

type Error struct {
	Code    ErrorCode
	Message string
	Detail  interface{}
}

func (e Error) Error() string {
	return fmt.Sprintf("%s: %s",
		strings.ToLower(strings.Replace(e.Code.String(), "_", " ", -1)), e.Message)
}

```

The go Stdlib has really good examples.

### Example, Custom Errors in `os`

in `error.go` in package **os**

```go
import (
	"internal/oserror"
	"internal/poll"
	"io/fs"
)
var (
	// ErrInvalid indicates an invalid argument.
	// Methods on File will return this error when the receiver is nil.
	ErrInvalid = fs.ErrInvalid // "invalid argument"

	ErrPermission = fs.ErrPermission // "permission denied"
	ErrExist      = fs.ErrExist      // "file already exists"
	ErrNotExist   = fs.ErrNotExist   // "file does not exist"
	ErrClosed     = fs.ErrClosed     // "file already closed"

	ErrNoDeadline       = errNoDeadline()       // "file type does not support deadline"
	ErrDeadlineExceeded = errDeadlineExceeded() // "i/o timeout"
)
```

Or the defined PathError type in `io/fs` package, which automatically formats the error output using the operation, the path and the error:
```go
// PathError records an error and the operation and file path that caused it.
type PathError struct {
	Op   string
	Path string
	Err  error
}

func (e *PathError) Error() string { return e.Op + " " + e.Path + ": " + e.Err.Error() }

```
and its usage in **file.go**
```go
// ReadAt reads len(b) bytes from the File starting at byte offset off.
// It returns the number of bytes read and the error, if any.
// ReadAt always returns a non-nil error when n < len(b).
// At end of file, that error is io.EOF.
func (f *File) ReadAt(b []byte, off int64) (n int, err error) {
	if err := f.checkValid("read"); err != nil {
		return 0, err
	}

	if off < 0 {
		return 0, &PathError{Op: "readat", Path: f.name, Err: errors.New("negative offset")}
	}


```

Example 2:

Handle all Patherrors the same way.
Example function, showing how this could look like:
```go
func baa(f *file) error {
	n, err := f.WriteAt(x, 3)
	if _, ok := err.(*PathError) {
		//...
	} else {
		log.Fatalf(err)
	}
}
```
So we check if it possible to convert err into a type of pointer to a PathError, otherwise we will fail.

Seperating the error type from the error message gives great leverage for error handling.

### 7 To be safe or not to be

> **Consider Concurrency** 
> - If you provide a library, someone will use it concurrently
> - Data structures are not safe for concurrent access
> - Values are not safe, you need to create a safe behaviour around them.

> **Making it safe** 
> - sync package: atomic behaviour to make a value safe (atomic)
> - Channels, to avoid shared state

What is the problem with this code?
Example taken from afero:
```go
func (m *MMFS) Create(name string) (File, error) {
	m.getData()[name] = MemFileCreate(name)
	m.registerDirs(m.getData()[name])
	return m.getData()[name], nil
}
```
> Maps are not memory safe

This can file in a go routine, with invalid memory address

Simple solution with MutEx:
```go
func (m *MMFS) Create(name string) (File, error) {

	m.lock()
	m.getData()[name] = MemFileCreate(name)
	m.unlock()

	m.registerDirs(m.getData()[name])
	return m.getData()[name], nil
}
```

> **Keeping it unsafe** 
> - Safety comes at a cost
> - Imposes behaviours on consumers
> - Proper API allows consumers to add safety as needed
> - Consumers can use channels or mutexes

> **Maps are unsafe by design** 
> - Often safety is unnecessary
> - Enables consumers to implement safety as needed / desired

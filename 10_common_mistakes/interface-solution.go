package ex2

import (
	"io"
)

type Viper struct {
	// just placeholder
	config interface{}
}

// adjust argument to use interface
func (v *Viper) marshalReader(in *io.Reader, inter interface{}) {
	// just placeholder
}

// use a better interface which represents what is needed
func (v *Viper) ReadBufConfig(in *io.Reader) error {
	v.config = make(map[string]interface{})
	v.marshalReader(in, v.config)
	return nil
}

package main

import (
	"fmt"
	"time"
)

func main() {
	var timeout = 3
	fmt.Println(timeout)
	// this creates a type mismatch
	// how to solve this?
	fmt.Println(timeout * time.Millisecond)
}

func ThisWorks() {
	// because the timeout is now a constant, its type will be determined the first time it is used.
	// Duration is based on int64, so the timeout will be automatically transformed to a Duration type
	const timeout = 10
	fmt.Println(timeout)
	fmt.Println(timeout * time.Millisecond)
}

func Solution() {
	// define the type of timeout right away, if you do not want to define it as a constant.
	var timeout time.Duration
	timeout = 10
	fmt.Println(timeout * time.Millisecond)
}

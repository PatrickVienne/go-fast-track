package ex2

import (
	"errors"
	"fmt"
)

var newError = fmt.Errorf("error here")

// this is the issues
func Fail() error {
	myError := errors.New("Error here")
	return myError
}

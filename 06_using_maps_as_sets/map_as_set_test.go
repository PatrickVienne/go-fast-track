package map_as_set

import (
	"reflect"
	"testing"
)

func TestUniquifier(t *testing.T) {
	type args struct {
		numbers []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "test-1",
			args: args{numbers: []int{1, 1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 5, 8, 8, 8, 9}},
			want: []int{1, 2, 3, 4, 5, 8, 9},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Uniquifier(tt.args.numbers); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Uniquifier(%v) = %v, want %v", tt.args, got, tt.want)
			}
		})
	}
}

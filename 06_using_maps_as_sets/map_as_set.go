package map_as_set

import "sort"

func Uniquifier(numbers []int) []int {
	setOfNums := map[int]struct{}{}

	uniques := []int{}
	for idx := range numbers {
		setOfNums[numbers[idx]] = struct{}{}
	}

	for num := range setOfNums {
		uniques = append(uniques, num)
	}
	sort.Ints(uniques)
	return uniques

}

module github.com/PatrickVienne/examples/monorepo/services/app1

go 1.21

replace github.com/PatrickVienne/examples/monorepo/libs/greet => ../../libs/greet

require github.com/PatrickVienne/examples/monorepo/libs/greet v0.0.0-00010101000000-000000000000

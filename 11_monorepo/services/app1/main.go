package main

import (
	"fmt"

	"github.com/PatrickVienne/examples/monorepo/libs/greet"
)

func main() {
	fmt.Println("App 1")
	greet.Greet("World")
}

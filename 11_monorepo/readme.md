# MonoRepo

Each application is defined in their own module, with their main.go

libraries are kept seperately and can be accessed by all applications in the monorepo.

When working only locally, the go.mod file can be used to redirect references to the imported lib module to the local path in the monorepo.

**services/app1/go.mod**
```js
module github.com/PatrickVienne/examples/monorepo/services/app1

go 1.21

replace github.com/PatrickVienne/examples/monorepo/libs/greet => ../../libs/greet

require github.com/PatrickVienne/examples/monorepo/libs/greet v0.0.0-00010101000000-000000000000
```

run with
```shell
$ cd services/app1/
$ go run main.go 
App 1

$ cd services/app2/
$ go run main.go 
App 2
```

This example is an adjusted version of:
https://earthly.dev/blog/golang-monorepo/

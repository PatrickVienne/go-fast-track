package collection

import (
	"reflect"
	"strings"
	"testing"
)

func TestMap(t *testing.T) {
	type args struct {
		vs []string
		f  func(string) string
	}
	tests := []struct {
		name string
		args args
		want []string
	}{
		{
			name: "test-map",
			args: args{
				vs: []string{"peach", "apple", "pear", "plum"},
				f:  strings.ToUpper,
			},
			want: []string{"PEACH", "APPLE", "PEAR", "PLUM"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Map(tt.args.vs, tt.args.f); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Map() = %v, want %v", got, tt.want)
			}
		})
	}
}

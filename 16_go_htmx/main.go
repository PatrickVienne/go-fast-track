package main

import (
	"embed"
	"html/template"
	"log"
	"net/http"
	"time"
)

//go:embed frontend/templates
var templateFS embed.FS

//go:embed all:frontend/static
var assets embed.FS

func main() {
	type templateData struct {
		Ts string
	}
	router := http.NewServeMux()
	fs := http.FileServer(http.FS(assets))
	router.Handle("/frontend/static/", http.StripPrefix("", fs))

	router.HandleFunc("/htmx/time.html", func(w http.ResponseWriter, r *http.Request) {
		t := template.Must(template.ParseFS(templateFS, "frontend/templates/htmx_time.html"))
		t.Execute(w, templateData{Ts: time.Now().Format(time.RFC3339)})
	})
	router.HandleFunc("/click", func(w http.ResponseWriter, r *http.Request) {
		time.Sleep(3 * time.Second)
		t := template.Must(template.ParseFS(templateFS, "frontend/templates/click.html"))
		t.Execute(w, nil)
	})
	router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		t := template.Must(template.ParseFS(templateFS, "frontend/templates/index.html", "frontend/templates/htmx_time.html"))
		t.Execute(w, templateData{Ts: time.Now().Format(time.RFC3339)})
	})

	log.Fatal(http.ListenAndServe(":8080", router))

}

# Shadowing in Golang

Beware of using bad naming conventions.
In worst case, it is possible to shadow builtin functions of golang.

Some examples:
```go
func main() {
	make := 10    // built-in function - make()
	append := 13  // built-in - append()
	json := `{}`  // import path - encoding/json
	true := false // boolean value - true
	x := true
	fmt.Println(make)
	fmt.Println(append)
	fmt.Println(json)
	fmt.Println(true, x)
}
```


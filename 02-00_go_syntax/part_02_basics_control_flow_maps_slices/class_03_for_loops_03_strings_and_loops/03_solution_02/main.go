package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

func task2() map[string]int {
	content, err := ioutil.ReadFile(os.Args[1]) // the file is inside the local directory
	if err != nil {
		log.Fatalln(err.Error())
	}

	text := strings.ToLower(string(content))

	// option 1: get all words by replacing all punctuations by empty fields
	text = strings.ReplaceAll(text, "'", " ")
	text = strings.ReplaceAll(text, "\"", " ")
	text = strings.ReplaceAll(text, "-", "")
	text = strings.ReplaceAll(text, ".", "")
	text = strings.ReplaceAll(text, ",", "")
	text = strings.ReplaceAll(text, "?", "")
	text = strings.ReplaceAll(text, "!", "")
	words := strings.Fields(text)

	// option 2: simply use regex to get all words
	// re := regexp.MustCompile(`\w+`)
	// words := re.FindAllString(text, -1)

	counts := map[string]int{} // Empty map
	for _, word := range words {
		counts[word]++
	}
	return counts
}

func main() {
	counts := task2()
	fmt.Println(counts)
}

package main

import (
	"fmt"
	"regexp"
	"strings"
	"unicode/utf8"
)

func ForRangeOverString() {
	myWord := "I♥World"
	fmt.Println("Printing ", myWord, string([]byte(myWord)), " length: ", len(myWord), " runes: ", utf8.RuneCountInString(myWord))
	// Printing  I♥World  length:  9
	for _, v := range myWord {
		// hex values for the bytes
		fmt.Printf("% x", v) // 49 2665 57 6f 72 6c 64
	}

	for _, v := range myWord {
		// decimal values
		fmt.Printf("% d", v) // 73 9829 87 111 114 108 100
	}

	fmt.Println()
	fmt.Printf("% x", myWord) // 49 e2 99 a5 57 6f 72 6c 64
	fmt.Println()
	for idx, _ := range myWord {
		// looping through runes
		// and printing each first byte hex value
		fmt.Printf("% x", myWord[idx]) // 49 e2 57 6f 72 6c 64
	}
	fmt.Println()
	for i := 0; i < len(myWord); i++ {
		// looping through each byte
		// and printing the hex value
		fmt.Printf("% x", myWord[i]) // 49 e2 99 a5 57 6f 72 6c 64
	}
	fmt.Println()

	for i, w := 0, 0; i < len(myWord); i += w {
		// looping through each byte
		// and printing the hex value
		runeValue, width := utf8.DecodeRuneInString(myWord[i:])
		fmt.Printf("%#U starts at byte position %d\n", runeValue, i)
		w = width // add widths, so "i" can be moved forward with "w" bytes
	}
	fmt.Println()
}

func ForRangeOverMultilineString() {
	multiLineText := `This is a multiline text
	It starts somewhere
	And then ends on a completely different line
	Please not the backticks, which make it a raw string.
	So \n will not be a new line.
	`

	fmt.Println()

	for idx, v := range strings.Fields(multiLineText) {
		fmt.Println(idx, v)
	}

	fmt.Println()

	re := regexp.MustCompile(`\w{2,}`)
	for idx, v := range re.FindAllString(multiLineText, -1) {
		fmt.Println(idx, v)
	}

}

func main() {
	ForRangeOverString()
}

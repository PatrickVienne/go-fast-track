package main

import (
	"fmt"
	"strings"
)

func task1(text string) map[string]int {

	words := strings.Fields(text)
	counts := map[string]int{} // Empty map
	for _, word := range words {
		counts[strings.ToLower(word)]++
	}
	return counts

}

func main() {
	text := `
	Needles and pins
	Needles and pins
	Sew me a sail
	To catch me the wind
	`
	counts := task1(text)
	fmt.Println(counts)

}

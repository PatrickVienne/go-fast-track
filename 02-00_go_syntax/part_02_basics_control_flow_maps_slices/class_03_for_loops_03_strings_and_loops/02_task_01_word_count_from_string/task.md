# WordCount - using maps, strings and loops

## Step 1:

Read in the hardcoded text: 
```go
	text := `
	Needles and pins
	Needles and pins
	Sew me a sail
	To catch me the wind
	`
```
and output a count of each word which comes up in the text.

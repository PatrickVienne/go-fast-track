# WordCount - using maps, strings and loops

## Step 2:

Read in a text from the file "zauberlehrling.txt" and output a list of the most 10 most common words.
Make sure to remove any characters of words such as any of:
```bash
'"-.,?!
``` 
from the string of the word, to really only consider the words not any punctuation. 
Think of ways to make this as efficient as possible.

## Hint:

to get the input of a file and turn it into a string with only lowercase characters, you can use:
```go
	content, err := ioutil.ReadFile(os.Args[1]) // the file is inside the local directory
	if err != nil {
		log.Fatalln(err.Error())
	}
```
and then convert it to lowercase:
```go
	text := strings.ToLower(string(content))
```

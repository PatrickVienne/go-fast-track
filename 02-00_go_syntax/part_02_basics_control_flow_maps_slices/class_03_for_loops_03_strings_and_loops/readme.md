# Loops and Strings

## single characters

Strings are iterables, however, it is important to differentiate between bytes and runes.

This can be seen, when checking the length of a string, which also has non-ascii characters.
Characters which do nit fit in 1 byte.

```go
	myWord := "I♥World"
	fmt.Println("Printing ", myWord, " length: ", len(myWord), " runes: ", utf8.RuneCountInString(myWord)) 
	// Printing  I♥World  length:  9  runes:  7
```

So 9 bytes of this string are represented by 7 runes.

The string `"I♥World"` has 9 bytes, so len returns the length of the corresponding `[]byte`.
However the character `♥` has 3 bytes representing it.

For this, golang defines the type `rune`, which is 1-4 bytes, depending on how many bytes the character nereds to be displayed.

This is important when iterating over a string and its parts.

Let's look at the hex values of the string when iterating through it via for loop and range

```go
	myWord := "I♥World"
	for _, v := range myWord {
		// hex values for the bytes
		fmt.Printf("% x", v) // 49 2665 57 6f 72 6c 64
	}
```

So in a for loop, the string is automatically iterating through the `rune` types, and not the `[]byte`.

We can also display the decimal values of the characters
```go
	for _, v := range myWord {
		// hex values for the bytes
		fmt.Printf("% x", v) // 49 2665 57 6f 72 6c 64
	}
```

An important difference though is, what bytes are reachable when refering to characters in a string via index.

We can print all bytes separated by a space in this way:
```go
	fmt.Printf("% x", myWord) // 49 e2 99 a5 57 6f 72 6c 64
```

And we can also loop through the string and reference each of its bytes
```go
	for idx, _ := range myWord {
		fmt.Printf("% x", myWord[idx]) // 49 e2 57 6f 72 6c 64
	}
```
The difference is more apparent when directly compares:
- 1st case: 49 e2 99 a5 57 6f 72 6c 64
- 2nd case: 49 e2       57 6f 72 6c 64

There is a way however to find out how long a rune is of a certain string, with `utf8.DecodeRuneInString`, so we can move forward accordingly

```go
	for i, w := 0, 0; i < len(myWord); i += w {
		// looping through each byte
		// and printing the hex value
		runeValue, width := utf8.DecodeRuneInString(myWord[i:])
		fmt.Printf("%#U starts at byte position %d\n", runeValue, i)
		w = width // add widths, so "i" can be moved forward with "w" bytes
	}
```

## words

Now that we have seen how to loop over each character of a string, what about words?

For this, the `strings` package has a helpful Function called `Fields`

So it is possible to split a text into words, which are separated by 1 or more whitespace characters:
```go
	multiLineText := `This is a multiline text
	It starts somewhere
	And then ends on a completely different line
	Please not the backticks, which make it a raw string.
	So \n will not be a new line.
	`

	for idx, v := range strings.Fields(multiLineText) {
		fmt.Println(idx, v)
	}
```

Another way would be to use the regexp package, and e.g. get all words with at least 2 alphabetic characters:
```go
	re := regexp.MustCompile(`\w{2,}`)
	for idx, v := range re.FindAllString(multiLineText, -1) {
		fmt.Println(idx, v)
	}
```

`MustCompile` defines the pattern which we will be looking for.
`re.FindAllString(multiLineText, -1)` takes in the text, and `-1` tells it, that we do not limit the results.
This will return a slice of strings with all the results. `nil` would indicate no results at all.
# Unique strings

Given a list of strings, return a set of those strings, without any duplicate entries.

## Hint:
Golang does not have sets, so this exercise is for you to implement a function, which does the same as a set constructor in many other programming languages.

Example call of the function and expected result:
```go
	strSet := UniqStr([]string{"apple", "pear", "kiwi", "lemon", "apple", "lemon"})
	fmt.Println(strSet) // [apple pear kiwi lemon]
```
# Regex

## Task

Make a simple regex matcher, which checks for valid email address.
This is by no means a real email validator, it is just a simple case for this task.

A valid email address:

only uses alphanumeric characters in front of the "@" character

Must have 2 words after the "@" character which are seperated by 1 "." character. Each of the words should have at least 1 character.

Example input
```
$ go run main.go np@gmail.com
```
Example output:
```
The emailaddress np@gmail.com if valid? true
```

$ go run main.go abcd@gmail-yahoo.com 
The emailaddress abcd@gmail-yahoo.com if valid? false

$ go run main.go ab.cd@gmailyahoo.com
The emailaddress ab.cd@gmailyahoo.com if valid? false
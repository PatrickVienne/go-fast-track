package main

import (
	"fmt"
	"regexp"
)

func SimpleRegex() {
	matched, err := regexp.MatchString(`a.b`, "aaxbb")
	fmt.Println("regexp.MatchString(`a.b`, \"aaxbb\")", matched, err)

	matched, err = regexp.MatchString(`^a.b$`, "aaxbb")
	fmt.Println("regexp.MatchString(`^a.b$`, \"aaxbb\")", matched, err)
}

func forceRegexValidation() {
	// re1, err := regexp.Compile(`regexp`) // error if regexp invalid
	// re2 := regexp.MustCompile(`regexp`)  // panic if regexp invalid
}

func find() {
	re := regexp.MustCompile(`foo.?`)
	fmt.Printf("%q\n", re.FindAll([]byte(`seafood fool`), -1))
}

func findFirstRestaurantdate() {
	mcdonaldsText := "What is now the biggest fast-food chain in the world began as a small hamburger stand in San Bernardino, California, 77 years ago on 1940-05-15"

	re := regexp.MustCompile(`\d{4}-\d{2}-\d{2}`)

	fmt.Printf("Pattern: %v\n", re.String()) // print pattern

	fmt.Println(re.MatchString(mcdonaldsText)) // true

	submatchall := re.FindAllString(mcdonaldsText, -1)
	for _, element := range submatchall {
		fmt.Println(element)
	}
}

func main() {
	SimpleRegex()
	findFirstRestaurantdate()
}

package main

import (
	"fmt"
	"os"
	"regexp"
)

func main() {
	emailAdress := os.Args[1]
	re := regexp.MustCompile(`^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+`)
	fmt.Printf("The emailaddress %s if valid? %t", emailAdress, re.MatchString(emailAdress))
}

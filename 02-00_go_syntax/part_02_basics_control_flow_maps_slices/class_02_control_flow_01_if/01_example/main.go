package main

import (
	"fmt"
)

func IfExample() {
	var s = "Japan"
	x := true
	if x {
		fmt.Println(s)
	}
}

/*
	If ... Else ...

	if  condition {
	    // code to be executed if condition is true
	} else {

	    // code to be executed if condition is false
	}
*/
func IfElseExample() {
	x := 100

	if x == 100 {
		fmt.Println("Japan")
	} else {
		fmt.Println("Canada")
	}
}

func IfElseIfElseExample() {
	x := 100

	if x == 50 {
		fmt.Println("Germany")
	} else if x == 100 {
		fmt.Println("Japan")
	} else {
		fmt.Println("Canada")
	}
}

func IfStatementInitialization() {
	if x, ok := pricelist["banana"]; ok && x > 100 {
		fmt.Println(x)
	}
}

func main() {

}

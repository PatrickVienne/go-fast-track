package main

import "fmt"

func main() {
	fmt.Println("Please enter your age: ")

	var age uint64
	fmt.Scanf("%d", &age)

	if age%2 == 0 && age <= 75 {
		fmt.Println("Your age is special")
	} else if age <= 4 {
		fmt.Println("You are quite young")
	} else if age >= 10 && age <= 18 {
		fmt.Println("You're growing fast")
	} else if age > 18 && age <= 75 {
		fmt.Println("This world is now in your hands.")
	} else if age > 75 {
		fmt.Println("Thank you for all your contributions.")
	} else {
		fmt.Println("Enjoy this year.")
	}

}

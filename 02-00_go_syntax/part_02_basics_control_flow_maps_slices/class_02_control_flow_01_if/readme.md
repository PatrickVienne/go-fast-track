# Control Flow

## if-else

We will make some small functions to demonstrate how if else works.

In general, you can use any boolean expression after the `if` and the code to be executed conditionally has to be in braces `{}`
For example:

```go
func IfExample() {
	var s = "Japan"
	x := true
	if x {
		fmt.Println(s)
	}
}
```

It is possible to extend this via `if else` or `else` statements:
```go
if  condition {
    // code to be executed if condition is true
} else {
    // code to be executed if condition is false
}
```

Example:
```go
func IfElseExample() {
	x := 100

	if x == 100 {
		fmt.Println("Japan")
	} else {
		fmt.Println("Canada")
	}
}
```

Or with additional possibilities:
```go
func IfElseIfElseExample() {
	x := 100

	if x == 50 {
		fmt.Println("Germany")
	} else if x == 100 {
		fmt.Println("Japan")
	} else {
		fmt.Println("Canada")
	}
}
```

Another useful feature is, that it is possible to initialize a variable in the if statement, so that the variable can be reused within the code block if the if statement:
```go
func IfStatementInitialization() {
	if x := 100; x == 100 {
		fmt.Println("Germany")
	}
}
```

This is quite useful when checking errors, or when checking if a key is present or not in a map:
```go
m := make(map[string]float64) {
 "pi": 3.14,
}
if x, found := m["pi"]; found {
    fmt.Println(x)
}  
```


package main

import (
	"fmt"
)

func ForRangeOverMap() {
	// Example 1
	capitals := map[string]string{"Japan": "Tokyo", "China": "Beijing", "Canada": "Ottawa"}
	for key, value := range capitals {
		fmt.Println("Key :", key, " Element :", value)
	}

	// Example 2
	for key := range capitals {
		fmt.Println(key)
	}

	// Example 3
	for _, value := range capitals {
		fmt.Println(value)
	}
}

func ForRangeOverSlice() {
	for idx, val := range []int{1, 10, 20, 40, 50, 60} {
		fmt.Println("My Slice", idx, val)
	}
}

func ForRangeOverString() {
	for idx, v := range "Hello✏️" {
		fmt.Printf("%d %s\n", idx, string(v))
	}

	for range "Hello" {
		fmt.Println("Hello")
	}
}

func main() {
	ForRangeOverString()
}

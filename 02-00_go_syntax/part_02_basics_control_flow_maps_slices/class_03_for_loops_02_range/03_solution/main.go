// Calculate maximal value in a slice
package main

import (
	"fmt"
)

func MinMax(numbers []int) (min int, max int) {
	max = numbers[0]
	min = numbers[0]
	// [1:] skips the first value
	for _, value := range numbers[1:] {
		if value > max {
			max = value
		}
		if value < min {
			min = value
		}
	}
	return
}

func main() {
	min, max := MinMax([]int{1, 20, 300, 3, 400, 20})
	fmt.Println(min, max) // 1 400
}

# MinMax

Make a function MinMax() which takes in an array of integers and finds the minimum and maximum number of that array

Example call:
```go
	min, max := MinMax([]int{1, 20, 300, 3, 400, 20})
	fmt.Println(min, max) // 1, 400
```

## Bonus 1:
make the script take an undefined number of arguments, and can be run like this:
```bash
go run main.go 1 2 400 23 29
```

## Bonus 2:

What does this function do?
```go
func walk(v interface{}) {
	switch v := v.(type) {
	case []interface{}:
		for i, v := range v {
			fmt.Println("index:", i)
			walk(v)
		}
	case map[interface{}]interface{}:
		for k, v := range v {
			fmt.Println("key:", k)
			walk(v)
		}
	default:
		fmt.Println("default", v)
	}
}
```

Experiment with calls to it and try to explain how the information flows, and how the recursive calls work:

```go
	var numbers = []interface{}{
		[]interface{}{
			map[interface{}]interface{}{"name": "coffee", "price": 10.0},
			map[interface{}]interface{}{"name": "bubble tea", "price": 18.0},
			map[interface{}]interface{}{"name": "water", "price": 1.0},
		},
		map[interface{}]interface{}{"address": "Washington D.C.", "IntIds": []uint{1238, 48192, 1}},
		map[interface{}]interface{}{"name": 10},
	}
```
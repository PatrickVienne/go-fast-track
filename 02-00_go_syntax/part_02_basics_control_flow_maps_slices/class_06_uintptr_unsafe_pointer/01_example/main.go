package main

import (
	"fmt"
	"unsafe"
)

type sample struct {
	a int
	b string
}

func main() {
	s := &sample{a: 1, b: "test"}

	//Getting the address of field b in struct s
	p := unsafe.Pointer(uintptr(unsafe.Pointer(s)) + unsafe.Offsetof(s.b))
	// VERY IMPORTANT:
	// casting a pointer to uintptr loses the reference to the object, so the object may be garbage collected.
	// this is why the above operation has to be made in 1 line, to remain atomic!

	// Typecasting it to a string pointer and printing the value of it
	fmt.Println(*(*string)(p))
}

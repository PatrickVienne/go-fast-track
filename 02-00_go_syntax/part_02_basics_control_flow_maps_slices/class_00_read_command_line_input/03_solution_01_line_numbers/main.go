package main

import (
	"bufio"
	"fmt"
	"os"
)

func addFilenumberToFile() {
	// by default splits at newline characters, and can take up to 64kB entry at once
	scanner := bufio.NewScanner(os.Stdin)
	i := 1
	for scanner.Scan() {
		fmt.Println(i, ": ", scanner.Text())
		i++
	}
}

func main() {
	addFilenumberToFile()
}

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func readSimpleText() {
	fmt.Println("Type your sentence, and finish your input with ENTER (newline): ")

	var word1, word2, word3 string
	// Scanln(a ...interface{}) calls: Fscanln(os.Stdin, a...)
	// Fscanln(os.Stdin, a...), this copies the vlaues of standard in to an interface
	fmt.Scanln(&word1, &word2, &word3)
	fmt.Println("Thank you for entering: ", word1, word2, word3)

	// This will only read 3 words, experiment with entering longer sentences
}

func readFormattedText() {

	var word1, word2 string
	var number1, number2 int
	// Scanln(a ...interface{}) calls: Fscanln(os.Stdin, a...)
	// Fscanln(os.Stdin, a...), this copies the vlaues of standard in to an interface
	fmt.Println("Type a number, and ENTER: ")
	fmt.Scanf("%d", &number1)
	fmt.Println("2 words, 1 number and ENTER: ")
	fmt.Scanf("%s %s %d", &word1, &word2, &number2)
	fmt.Println("Thank you for entering: ", word1, word2, number1, number2)

	// again, experiment with adding more input
}

func readFullSentence() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Simple Shell")
	fmt.Println("---------------------")

	for {
		fmt.Print("-> ")
		text, _ := reader.ReadString('\n')

		// convert CRLF to LF

		// windows
		text = strings.Replace(text, "\r\n", "", -1)
		// unix
		// text = strings.Replace(text, "\n", "", -1)

		if strings.Compare("hi", text) == 0 {
			fmt.Println("hello, Yourself")
		} else {
			fmt.Println("echo: ", text)
		}

	}
}

func readSingleCharacter() {
	reader := bufio.NewReader(os.Stdin)
	char, _, err := reader.ReadRune()
	// still need to press enter to finish input

	if err != nil {
		fmt.Println(err)
	}

	// print out the unicode value i.e. A -> 65, a -> 97
	fmt.Println(char)

	switch char {
	case 'A':
		fmt.Println("A Key Pressed")
		break
	case 'a':
		fmt.Println("a Key Pressed")
		break
	}
}

func readWithScanner() {
	scanner := bufio.NewScanner(os.Stdin) // by default splits at newline characters, and can take up to 64kB entry at once
	for scanner.Scan() {
		fmt.Println("echo", scanner.Text())
	}
	// this can also read a file line by line, e.g. by piping the file as an input to this script:
	// go run main.go < surprise.txt
}

func main() {
	// readSimpleText()
	// readFormattedText()
	// readFullSentence()
	// readSingleCharacter()
	readWithScanner()
}

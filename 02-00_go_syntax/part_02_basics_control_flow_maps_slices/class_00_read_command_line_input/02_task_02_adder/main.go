// Golang program to demonstrate the
// example of fmt.Scanf() function
// Input two numbers & find their sum

package main

import (
	"fmt"
)

func main() {
	var num1 int
	var num2 int
	var sum int

	// Input numbers
	fmt.Print("Enter first number: ")
	/* TODO: read a single entered integer value */

	fmt.Print("Enter second number: ")
	/* TODO: read a single entered integer value */

	// Calculate sum
	/* TODO: calculate the sum */

	// Do not change the code below this line
	// Printing the values & sum
	fmt.Printf("%d + %d = %d\n", num1, num2, sum)
}

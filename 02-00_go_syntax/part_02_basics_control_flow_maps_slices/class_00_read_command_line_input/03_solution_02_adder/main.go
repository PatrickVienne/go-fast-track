// Golang program to demonstrate the
// example of fmt.Scanf() function
// Input two numbers & find their sum

package main

import (
	"fmt"
)

func main() {
	var num1 int
	var num2 int
	var sum int

	// Input numbers
	fmt.Print("Enter first number: ")
	fmt.Scanf("%d", &num1)

	fmt.Print("Enter second number: ")
	fmt.Scanf("%d", &num2)

	// Calculate sum
	sum = num1 + num2

	// Printing the values & sum
	fmt.Printf("%d + %d = %d\n", num1, num2, sum)
}

# Read-in

## Task 1
Write a script, which can take in a file from the command line when called with
```
go run main.go < filename
```

It should read the file line by line, and output each line, adding the current line number to the output.


### Example:

Example call:
```
go run main.go < elephants.txt
```


Example solution:
```
1 :  Elephants are the largest living animals on land. 
2 :  The three known species are the African bush elephant, the African Forest elephant and the Asian elephant. 
3 :  The African bush elephants are by far the largest, growing up to 3.36 m tall and weighing up to 6,914 kg.
4 :
5 :  The African elephants are quite distinct in appearance from the Asian elephants.
6 :  African elephants tend to be physically larger (weighing 4,000-7,500kg), with larger ears and concave backs.
7 :  Asian elephants tend to be smaller (weighing 3000-6,000kg), with smaller ears and convex backs.
8 :  African elephants also have full, rounded heads with a single dome at the top of their head. Asian elephants, meanwhile have twin-domed heads with a middle indentation.
```

###  Hint

`Scanner` class can be helpful

## Task 2

Fill the gaps in the code, and make a script which asks for 2 integer input and prints the result of the addition to the terminal
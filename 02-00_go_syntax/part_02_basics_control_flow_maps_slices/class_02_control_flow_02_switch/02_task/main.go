package main

import "fmt"

func printType(/* variable t, use a type which could stand for any other type*/) {
	switch passedType := /* */ {
		/* TODO: Define all switch cases based on the passed type, and a case which handles unknown types
			Each case should make a printout to the terminal, stating the passed variables type
		*/
	}
}

func main() {
	var something interface{}
	number := uint64(64)

	printType(something)       // value is of nil type
	printType(42)              // value is of int type
	printType(float32(10.10))  // value is of float type
	printType(float64(-40.42)) // value is of float type
	printType(number)          // value is of type: uint64
}

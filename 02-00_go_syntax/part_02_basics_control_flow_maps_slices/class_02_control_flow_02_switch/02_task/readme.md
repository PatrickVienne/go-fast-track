# Task

Use a switch statement to define a function a function `printType`.

Print type should be able to use any type passed to it. You can use the general `interface` to do that.

Furthermore, the function should get the type of the passed variable.

This can be done with `.(type)` after the passed variable, e.g.
```go
	myType := myVar.(type)
```

There should be a case for the most known types:
- bool
- float64
- int
- nil

There should also be a case which handles all other cases.

you can group types which belong together in one case.

The expected outcomes are given in comments behind the function calls

```go
func main() {
	var something interface{}
	number := uint64(64)

	printType(something)       // value is of nil type
	printType(42)              // value is of int type
	printType(float32(10.10))  // value is of float type
	printType(float64(-40.42)) // value is of float type
	printType(number)          // value is of type: uint64
}
```
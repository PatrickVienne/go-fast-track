package main

import (
	"fmt"
	"time"
)

func SwitchStatements() {
	myNumber := 13

	switch myNumber {
	case 5: // block for value being 5
		fmt.Println("Today is 5th. Clean your house.")
	case 10, 11, 12: // select common block of code for many similar cases.
		fmt.Println("Today is 10th/11th/12th. Exercise.")
	case 13:
		fmt.Println("Buy some wine.")
		fallthrough // keyword used to force the execution flow to fall through the successive case block.
	case 15:
		fmt.Println("Today is 15th. Visit a doctor.")
	case 25:
		fmt.Println("Today is 25th. Buy some food.")
	case 31:
		fmt.Println("Party tonight.")
	default:
		fmt.Println("No information available for that day.")
	}
}

func SwitchConditionalStatements() {
	today := time.Now()

	switch {
	case today.Day() < 5:
		fmt.Println("Clean your house.")
	case today.Day() <= 10:
		fmt.Println("Buy some wine.")
	case today.Day() > 15:
		fmt.Println("Visit a doctor.")
	case today.Day() == 25:
		fmt.Println("Buy some food.")
	default:
		fmt.Println("No information available for that day.")
	}
}

func SwitchInitializedStatement() {
	switch today := time.Now(); {
	case today.Day() < 5:
		fmt.Println("Clean your house.")
	case today.Day() <= 10:
		fmt.Println("Buy some wine.")
	case today.Day() > 15:
		fmt.Println("Visit a doctor.")
	case today.Day() == 25:
		fmt.Println("Buy some food.")
	default:
		fmt.Println("No information available for that day.")
	}
}

func main() {
	SwitchStatements()
}

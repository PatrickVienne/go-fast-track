package main

import "fmt"

func printType(t interface{}) {
	switch passedType := t.(type) {
	case bool:
		fmt.Println("value is of boolean type")
	case float64, float32:
		fmt.Println("value is of float type")
	case int:
		fmt.Println("value is of int type")
	case nil:
		fmt.Println("value is of nil type")
	default:
		fmt.Printf("value is of type: %T", passedType)
	}
}

func main() {
	var something interface{}
	number := uint64(64)

	printType(something)       // value is of nil type
	printType(42)              // value is of int type
	printType(float32(10.10))  // value is of float type
	printType(float64(-40.42)) // value is of float type
	printType(number)          // value is of type: uint64
}

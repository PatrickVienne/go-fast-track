package main

import (
	"fmt"
	"time"
)

func DateOfAgeInSeconds(birthday time.Time, seconds int) {
	dateSecondsAfterBirth := birthday.Add(time.Duration(seconds) * time.Second)
	fmt.Println("Your birthday was on a ", birthday.Weekday())
	fmt.Println(dateSecondsAfterBirth.Format(time.RFC1123)) // "Mon, 02 Jan 2006 15:04:05 MST"
}

func PrintSecondsSince(dateTime time.Time) {
	currentTime := time.Now()
	diff := currentTime.Sub(dateTime)
	//In seconds
	fmt.Printf("Seconds: %f\n", diff.Seconds())
}

func timePassed(startTime time.Time) {
	fmt.Printf("==PrintCurrentSecondOfLife ran for: %f\n seconds", time.Since(startTime).Seconds())
}

func PrintCurrentSecondOfLife(birthday time.Time) {
	start := time.Now()
	defer timePassed(start) // print will only be evaluated and made once the function is fininshed

	// the following statement will not work, because the print statement will be evaluated immediately, and will thus always give 0
	// defer fmt.Printf("==PrintCurrentSecondOfLife ran for: %f\n seconds", time.Since(start).Seconds())

	for i := 0; i < 5; i++ {
		fmt.Printf("Alive for %d seconds.\n", int(time.Since(birthday).Seconds()))
		time.Sleep(1 * time.Second)
	}
}

func main() {
	birthdate := time.Date(2020, 1, 2, 0, 0, 0, 0, time.UTC)
	DateOfAgeInSeconds(birthdate, 1e9)
	PrintSecondsSince(birthdate)
	PrintCurrentSecondOfLife(birthdate)
}

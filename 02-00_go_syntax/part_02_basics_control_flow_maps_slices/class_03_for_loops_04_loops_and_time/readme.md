# Loops and Time

## Main functiionalities

The main functionalities of time are to do any parsing, formatting and operations around time objects.

To find out, when we will be 1 billion seconds old, simple:
```go
	birthdate := time.Date(2020, 1, 2, 0, 0, 0, 0, time.UTC)
	dateSecondsAfterBirth := birthday.Add(time.Duration(seconds) * time.Second)
```

To get a formatted output:
```go
	fmt.Println(dateSecondsAfterBirth.Format(time.RFC1123)) // "Mon, 02 Jan 2006 15:04:05 MST"
```

It is quite simple to generate the current time:

```go
	currentTime := time.Now()
```

which is also a `time.Time` object.

And the `time.Since` can return the `Duration` passed since a special event.

```go
	time.Since(birthday)
```

Finally, we can see how our life is passing just like sands in an hourglass using a loop, which sleeps 1 second and then continues running
```go
func PrintCurrentSecondOfLife(birthday time.Time) {
	for {
		fmt.Printf("Alive for %d seconds.\n", int(time.Since(birthday).Seconds()))
		time.Sleep(1 * time.Second)
	}
}
```

package main

import (
	"io/ioutil"
	"log"
	"os"
	"strings"
)

/* TODO: Implement your types for sorting here, which implement the `sort.Interface` */

func SortData(freq map[string]int) /* TODO */ {
	/* TODO */
}

// do not change this functions
func task2(filename string) map[string]int {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalln(err.Error())
	}

	text := strings.ToLower(string(content))

	text = strings.ReplaceAll(text, "'", " ")
	text = strings.ReplaceAll(text, "\"", " ")
	text = strings.ReplaceAll(text, "-", "")
	text = strings.ReplaceAll(text, ".", "")
	text = strings.ReplaceAll(text, ",", "")
	text = strings.ReplaceAll(text, "?", "")
	text = strings.ReplaceAll(text, "!", "")
	words := strings.Fields(text)

	counts := map[string]int{}
	for _, word := range words {
		counts[word]++
	}

	return counts
}

func main() {

	task2(os.Args[1])

	// call here `SortData(counts)`
	// and give an appropriate return value and print it to the terminal
	// print the 5 most common words and their counts
}

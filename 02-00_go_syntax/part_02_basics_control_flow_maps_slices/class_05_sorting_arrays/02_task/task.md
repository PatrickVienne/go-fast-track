# WordCount - sorting interface

Build on the previous task of reading all the words from the "zauberlehrling.txt" text, and now sort the words by the times they appear.

The **main.go** already has parts filled out, complete the script.

Give out the 5 most common words and their counts.

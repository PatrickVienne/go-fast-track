package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
	"strings"
)

type Pair struct {
	Key   string
	Value int
}

type PairList []Pair

func (p PairList) Len() int           { return len(p) }
func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p PairList) Less(i, j int) bool { return p[i].Value < p[j].Value }

func SortData(freq map[string]int) PairList {
	p := make(PairList, len(freq))

	i := 0
	for k, v := range freq {
		p[i] = Pair{k, v}
		i++
	}

	sort.Sort(sort.Reverse(p))
	return p
}

// do not change this functions
func task2(filename string) map[string]int {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatalln(err.Error())
	}

	text := strings.ToLower(string(content))

	text = strings.ReplaceAll(text, "'", " ")
	text = strings.ReplaceAll(text, "\"", " ")
	text = strings.ReplaceAll(text, "-", "")
	text = strings.ReplaceAll(text, ".", "")
	text = strings.ReplaceAll(text, ",", "")
	text = strings.ReplaceAll(text, "?", "")
	text = strings.ReplaceAll(text, "!", "")
	words := strings.Fields(text)

	counts := map[string]int{}
	for _, word := range words {
		counts[word]++
	}

	return counts
}

func main() {
	counts := task2(os.Args[1])
	sortedCounts := SortData(counts)
	fmt.Println(sortedCounts[:5])
}

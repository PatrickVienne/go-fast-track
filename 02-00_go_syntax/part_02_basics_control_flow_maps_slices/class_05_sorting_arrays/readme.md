# Sorting

## Sorting Slices of integers or Strings

`sort.Ints` is a convenient function to sort a couple of ints.
Generally you need to implement the `sort.Interface` interface if you want to sort something and `sort.Reverse` just returns a different implementation of that interface that redefines the `Less` method.

Luckily the sort package contains a predefined type called IntSlice that implements `sort.Interface`
```go
```

## Sorting by implementing the interface

For Sorting, if you do not know where to start, the documentation helps how to implement sorting:
https://pkg.go.dev/sort?utm_source=gopls#example-Reverse

Sorting of maps is a little bit complicated, and needs additional steps.

There need to be defined 2 types, which are containers for the key-value pairs

```go
type Pair struct {
	Key   string
	Value int
}

type PairList []Pair
```

And since we are going to use the `sort` package's functions on them, the `PairList` needs to implement the `sort.Interface`.

The definition of the `sort.Interface` in **sort.go** is the following:
```go
type Interface interface {
	// Len is the number of elements in the collection.
	Len() int

	// Less reports whether the element with index i
	// must sort before the element with index j.
	//
	// If both Less(i, j) and Less(j, i) are false,
	// then the elements at index i and j are considered equal.
	// Sort may place equal elements in any order in the final result,
	// while Stable preserves the original input order of equal elements.
	//
	// Less must describe a transitive ordering:
	//  - if both Less(i, j) and Less(j, k) are true, then Less(i, k) must be true as well.
	//  - if both Less(i, j) and Less(j, k) are false, then Less(i, k) must be false as well.
	//
	// Note that floating-point comparison (the < operator on float32 or float64 values)
	// is not a transitive ordering when not-a-number (NaN) values are involved.
	// See Float64Slice.Less for a correct implementation for floating-point values.
	Less(i, j int) bool

	// Swap swaps the elements with indexes i and j.
	Swap(i, j int)
}
```

So the three methods have to be implemented:
- `Len() int`
- `Less(i, j int) bool`
- `Swap(i, j int)`

Try to think how these 3 should look like, before you continue reading.

This can simply be done with these 3 simple methods:
```go
func (p PairList) Len() int           { return len(p) }
func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p PairList) Less(i, j int) bool { return p[i].Value < p[j].Value }
```

And lastly, now the Container `PairList` has to be created based on the wordcounts, and then sorted with the sort functions

`sort.Sort()` and, if we want a descending order, additional `sort.Reverse` has to be applied.

This leads to a line similar to this:
```go
sort.Sort(sort.Reverse(myPairlist))
```
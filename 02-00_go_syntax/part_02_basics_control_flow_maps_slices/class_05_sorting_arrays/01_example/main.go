package main

import (
	"fmt"
	"sort"
	"time"
)

type Birthday struct {
	Date time.Time
	Name string
}

type Birthdays []Birthday

func (a Birthdays) Len() int           { return len(a) }
func (a Birthdays) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a Birthdays) Less(i, j int) bool { return a[i].Date.Before(a[j].Date) }
func (a Birthdays) Print() {
	for _, birthday := range a {
		fmt.Printf("%#v\n", birthday)
	}
}

func SortBirthdays() {
	birthdays := Birthdays{
		{time.Date(1707, 4, 15, 0, 0, 0, 0, time.UTC), "Leonhard Euler"},
		{time.Date(1777, 4, 30, 0, 0, 0, 0, time.UTC), "Johann Carl Friedrich Gauss"},
		{time.Date(1879, 3, 14, 0, 0, 0, 0, time.UTC), "Albert Einstein"},
		{time.Date(1858, 4, 23, 0, 0, 0, 0, time.UTC), "Max Karl Ernst Ludwig Planck"},
		{time.Date(1942, 1, 8, 0, 0, 0, 0, time.UTC), "Stephen William Hawking"},
	}

	fmt.Println("== SORTED Birthdays ==")
	fmt.Printf("Slice of Birthdays BEFORE        sort:\n")
	birthdays.Print()

	fmt.Printf("Slice of Birthdays AFTER         sort: \n")
	sort.Sort(birthdays)
	birthdays.Print()

	fmt.Printf("Slice of Birthdays AFTER Reverse sort: \n")
	sort.Sort(sort.Reverse(birthdays))
	birthdays.Print()
}

func SortMap() {
	unSortedMap := map[string]int{"India": 20, "Canada": 70, "Germany": 15}

	keys := make([]string, 0, len(unSortedMap))
	for k := range unSortedMap {
		keys = append(keys, k)
	}

	// unneeded, since by default the keys are sorted in a map.
	// sort.Strings(keys)
	fmt.Println("== SORTED MAP ==")
	for _, k := range keys {
		fmt.Println(k, unSortedMap[k])
	}

	fmt.Println("== REVERSE SORTED MAP ==")
	// reverse sorting to iterate in reverse order
	sort.Sort(sort.Reverse(sort.StringSlice(keys)))

	// Print values of reverse sorted Slice.
	for _, k := range keys {
		fmt.Println(k, unSortedMap[k])
	}
}

func SortArray() {
	fmt.Println("== SORTED ARRAY ==")
	intSlice := []int{10, 5, 25, 351, 14, 9} // unsorted
	fmt.Println("Slice of integer BEFORE        sort:", intSlice)
	sort.Ints(intSlice)
	fmt.Println("Slice of integer AFTER         sort:", intSlice)
	sort.Sort(sort.Reverse(sort.IntSlice(intSlice)))
	fmt.Println("Slice of integer AFTER Reverse sort:", intSlice)
}

func main() {
	SortBirthdays()
	SortMap()
	SortArray()
}

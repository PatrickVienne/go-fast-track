package main

import (
	"fmt"
	"strings"
)

func concatSlices() {
	fmt.Println("==concatSlices==")
	a := []int{1, 2, 3, 4, 5}
	b := []int{6, 7, 8, 9, 10}
	c := []int{11, 12, 13, 14, 15}

	fmt.Printf("a: %v\n", a)
	fmt.Printf("cap(a): %v\n", cap(a))

	fmt.Printf("b: %v\n", b)
	fmt.Printf("cap(c): %v\n", cap(b))

	fmt.Printf("c: %v\n", c)
	fmt.Printf("cap(c): %v\n", cap(c))

	x := []int{}
	x = append(a, b...) // Can't concatenate more than 2 slice at once
	x = append(x, c...) // concatenate x with c
	fmt.Printf("\n######### After Concatenation #########\n")
	fmt.Printf("x: %v\n", x)
	fmt.Printf("cap(x): %v\n", cap(x))

}

func sliceDeleteElementFast() {
	fmt.Println("==sliceDeleteElementFast==")

	a := []string{"A", "B", "C", "D", "E"}

	fmt.Printf("a: %v\n", a)
	fmt.Printf("cap(a): %v\n", cap(a))

	i := 2

	// Remove the element at index i from a.
	a[i] = a[len(a)-1] // Copy last element to index i.
	a[len(a)-1] = ""   // Erase last element (write zero value).
	a = a[:len(a)-1]   // Truncate slice.

	fmt.Printf("\n######### After Concatenation #########\n")
	fmt.Printf("a: %v\n", a)
	fmt.Printf("cap(a): %v\n", cap(a))
}

func sliceDeleteElementSlow() {
	fmt.Println("==sliceDeleteElementSlow==")

	a := []string{"A", "B", "C", "D", "E"}
	fmt.Printf("a: %v\n", a)
	fmt.Printf("cap(a): %v\n", cap(a))

	i := 2

	// Remove the element at index i from a.
	copy(a[i:], a[i+1:]) // Shift a[i+1:] left one index.
	a[len(a)-1] = ""     // Erase last element (write zero value).
	a = a[:len(a)-1]     // Truncate slice.

	fmt.Printf("\n######### After Concatenation #########\n")
	fmt.Printf("a: %v\n", a)
	fmt.Printf("cap(a): %v\n", cap(a))
}

func sliceThirdIndex() {
	// For slice[ i : j : k ] the
	// Length:   j - i
	// Capacity: k - i

	people := []string{
		"John",
		"Steve",
		"Jane",
	}

	//p := people[1:2]
	fmt.Println("==p := people[1:2:2]==")
	p := people[1:2:2] // [low:high:max] => cap = max-low
	fmt.Println("capacity:", cap(p))
	p = append(p, "Mike")

	inspect("people: ", people)
	inspect("sub people: ", p)
	fmt.Println("people", people)
	fmt.Println("p", p)
	// fmt.Println(p[2]) this would be a panic

	// pure clone
	fmt.Println("==peopleClone := append(people[:0:0], people...)==")
	peopleClone := append(people[:0:0], people...) // [0:0:0]
	inspect("people: ", people)
	inspect("peopleClone: ", peopleClone)
	fmt.Println("people: ", people)
	fmt.Println("peopleClone: ", peopleClone)

	// no clone, referencing the same objects as people
	fmt.Println("==peopleClone2 := append(people[:0], people...)==")
	peopleClone2 := append(people[:0], people...) // [0:0], no max capacity, so capacity still goes over all the people addresses
	inspect("people: ", people)
	inspect("peopleClone: ", peopleClone2)
	fmt.Println("people: ", people)
	fmt.Println("peopleClone: ", peopleClone2)

	fmt.Println("==peopleClone3 := people[:0]==")
	peopleClone3 := people[:0]
	inspect("people: ", people)
	inspect("peopleClone: ", peopleClone3)
	fmt.Println("people: ", people)
	fmt.Println("peopleClone: ", peopleClone3)
}

func inspect(label string, people []string) {
	f := strings.Repeat("%p | ", len(people))
	f = label + f + "\n"
	var args []interface{}
	for i := range people {
		args = append(args, &people[i])
	}
	fmt.Printf(f, args...)
}

func main() {
	concatSlices()
	sliceDeleteElementSlow()
	sliceDeleteElementFast()
	sliceThirdIndex()
}

package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
)

func GetNewMixedRandomQuote(quotes [][]string) string {
	rand.Intn(len(quotes))
	used := map[int]bool{}
	firstQuoteId := rand.Intn(len(quotes))
	used[firstQuoteId] = true

	var nextQuoteId int
	for nextQuoteId = firstQuoteId; used[nextQuoteId]; nextQuoteId = rand.Intn(len(quotes)) {

	}

	firstQuote := quotes[firstQuoteId]
	secondQuote := quotes[nextQuoteId]

	final1 := append(firstQuote[:len(firstQuote)/2], secondQuote[len(secondQuote)/2:]...)

	return strings.Join(final1, " ")
}

func main() {
	rand.Seed(time.Now().Unix())

	var laoTzu = []string{"Doing", "nothing", "is", "better", "than", "being", "busy", "doing", "nothing"}
	var confuscius = []string{"Study", "the", "past", "if", "you", "would", "divine", "the", "future."}
	var freud = []string{"From", "error", "to", "error", "one", "discovers", "the", "entire", "truth."}
	var benjaminFranklin = []string{"Well", "done", "is", "better", "than", "well", "said."}
	var napoleonHill = []string{"Ideas", "are", "the", "beginning", "points", "of", "all", "fortunes."}

	newQuote := GetNewMixedRandomQuote([][]string{laoTzu, confuscius, freud, benjaminFranklin, napoleonHill})
	fmt.Println(newQuote)
}

# Fizzbuzz

Read a number value inputted, the input value should be between 1 and 100 and validated.
If the input is not a number, print an error message and panic

Then, print the numbers between 1 and the inputted number.

But:
- if the number is divisible by 3 print "fizz" instead
- if the number is divisible by 5 print "buzz" instead
- if the number is divisible by 15 print "fizzbuzz" instead

example input: 
```
15
```
example output
```
1
2
fizz
4
buzz
fizz
7
8
fizz
buzz
11
fizz
13
14
fizzbuzz

```

## Hints

When scanning the input number, you can use

```go
	var num int
	n, err := fmt.Scanln(&num)
```

And you can make some error output, in case something went wrong:
```go
	if err != nil {
		log.Panicf(err.Error())
	}
	fmt.Println(n, e
```

## Bonus:

Make some validation. The input number should be an integer between 10 and 100.

If the user fails to enter a valid number, ask to user to either input a valid number, or type "q" (and ENTER) to quit the program.

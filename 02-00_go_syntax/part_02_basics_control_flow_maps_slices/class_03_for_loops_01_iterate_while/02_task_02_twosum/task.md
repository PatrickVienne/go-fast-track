# Find Twosum

The program should take in a list of integers.
The first is the target sum.
The next numbers should have 2 among them which add up to the target.
Return the 2 numbers which add up to the first number in the list.

Example run:

```bash
$ go run . 14 1 2 5 8 12 15
Values which add up to 14: 2 and 12
```
package main

import (
	"fmt"
)

func twoSum(nums []int, target int) []int {
	/* TODO */
	return []int{}
}

func ParseNumber() (nums []int, target int) {
	/* TODO */
	return []int{}, 0
}

func main() {
	nums, target := ParseNumber()
	sol := twoSum(nums, target)
	fmt.Printf("Values which add up to %d: %d and %d", target, nums[sol[0]], nums[sol[1]])
}

package main

import "fmt"

func ForIteration1(num int) {
	for i := 1; i <= num; i++ {
		fmt.Printf("Current i: %d\n", i)
	}
}

func ForIteration2(num int) {
	i := 1
	for ; i <= num; i++ {
		fmt.Printf("Current i: %d\n", i)
	}
}

func ForIteration3(num int) {
	i := 1
	for i <= num {
		fmt.Printf("Current i: %d\n", i)
		i++
	}
}

func ForIteration4(num int) {
	i := 1
	for ; ; i++ {
		if !(i <= num) {
			break
		}
		fmt.Printf("Current i: %d\n", i)
	}
}

func ForIteration5(num int) {
	i := 1
	for {
		if !(i <= num) {
			break
		}
		fmt.Printf("Current i: %d\n", i)
		i++
	}
}

func main() {
	ForIteration1(4)
}

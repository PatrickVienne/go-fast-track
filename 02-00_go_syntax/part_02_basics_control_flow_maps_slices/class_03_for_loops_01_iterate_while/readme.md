# For loops - Iteration and While

There are few different kind of loops.
// https://www.golangprograms.com/for-range-loops.html

## for loop as iterators with break condition

The first kind of loop is very similar to the C++ or JAVA style loop.

```go
func ForIterationUntil(num int) {
	for i := 1; i <= num; i++ {
		fmt.Printf("Current i: %d\n", i)
	}
}
```

Calling `ForIterationUntil(4)` we expect following output

```
$ go run main.go 
Current i: 1
Current i: 2
Current i: 3
Current i: 4
```

As you can see, the line 
```go
for i := 1; i <= num; i++
```
is split in 3 parts.
- the initialization `i := 1`
- the condition under which this loop continues `i <= num`
- the final step after each loop: `i++`

However the `for` loop in Go has some more flexibility.
Following code blocks are equivalent:

Option 1:
```go
	i := 1
	for ; i <= num; i++ {
		fmt.Printf("Current i: %d\n", i)
	}
```

Option 2:
```go
	i := 1
	for ; ; i++ {
		if !(i <= num) {
			break
		}
		fmt.Printf("Current i: %d\n", i)
	}
```

Option 3 (while loop):
```go
	i := 1
	for i <= num {
		fmt.Printf("Current i: %d\n", i)
		i++
	}
```

And lastly, writing this as an endless loop with break condition:
```go
	i := 1
	for {
		if !(i <= num) {
			break
		}
		fmt.Printf("Current i: %d\n", i)
		i++
	}
```

## While loop

As stated above, the for loop also fulfills the function of a while loop if used as such.
An infitinite loop is just a for without conditions:
```go
for {

}
```

A while loop with a condition just has a condition added. 
For example at each iteration we could just check if the routine is still alive.
```go
for isAlive() {

}
```


func ForRangeOverMap() {
	// Example 1
	strDict := map[string]string{"Japan": "Tokyo", "China": "Beijing", "Canada": "Ottawa"}
	for index, element := range strDict {
		fmt.Println("Index :", index, " Element :", element)
	}

	// Example 2
	for key := range strDict {
		fmt.Println(key)
	}

	// Example 3
	for _, value := range strDict {
		fmt.Println(value)
	}
}

func ForRangeOverString() {
	for idx, v := range "Hello" {
		fmt.Println(idx, v)
	}

	for range "Hello" {
		fmt.Println("Hello")
	}
}

func ForInfiniteLoopWithBreak() {
	i := 5
	for {
		fmt.Println("Hello")
		if i >= 10 {
			break
		}
		i++
	}
}

func main() {
	ForInfiniteLoopWithBreak()
}

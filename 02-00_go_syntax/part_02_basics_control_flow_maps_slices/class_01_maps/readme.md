# Maps

Maps are a not-race-safe datastructure, they belong to the reference types, and thereby when passed to a function and modified inside, the original map's values also can get modified.

Declaring a variable as a map is simple.

```go
    var prices map[int]string    // name map has int keys and string values
```

And initializing it can be done with "make"

```go
    var prices = make(map[int]string)
```

New values can be added with 

```go
    var prices = make(map[string]float64)
    prices["pancake"] = 1.64
```

Or added when the map is initialized

```go
    prices := map[string]float64 {
        "pancake": 1.64,
        "strawberry": 2.20,,   // last comma is a must
    }
```

The values can be read in 2 ways:

Either by simply reading the value
```go
    // simply read the value
	pancakePrice := prices["pancake"]
	fmt.Println("pancakePrice", pancakePrice)
```

or by reading the value, and getting a check whether this value was in the map or not

Compare the 2 outputs of this:
```go
	strawberryPrice, strawberryExists := prices["strawberry"]
	fmt.Println("strawberryPrice, strawberryExists: ", strawberryPrice, strawberryExists)

	blueberryPrice, blueberryExists := prices["blueberry"]
	fmt.Println("blueberryPrice, blueberryExists", blueberryPrice, blueberryExists)
```

Please note:
If a key is not present, the map will still return the zero value of the value's type.
For a float this is 0.0.

We can use this to create a counter.

For deleting, simply use `delete`

```go
    delete(prices, "pancake")
```

And lastly, it is possible to iterate over a map by using a for loop:
```go
    for food, price := range prices {
        fmt.Println(food, price)
    }
```

To get the keys or values, you need to iterate over the map and you can just save all the keys or values in a new slice.

In control flows, it is possible to check a key and continue from that on in the same line.

Example:
```go
m := map[string]float64{"pi": 3.14}
if v, found := m["pi"]; found {
    fmt.Println(v)
}
```

Overview of what a map is and how it operates:

- A map (or dictionary) is an unordered collection of key-value pairs, where each key is unique.
- You create a new map with a make statement or a map literal.
- The default zero value of a map is nil. A nil map is equivalent to an empty map except that elements can’t be added.
- The len function returns the size of a map, which is the number of key-value pairs.
- Starting with Go 1.12, the fmt package prints maps in key-sorted order to ease testing.
- Maps are backed by hash tables.
- Add, get and delete operations run in constant expected time. The time complexity for the add operation is amortized.
- The comparison operators == and != must be defined for the key type.
- Maps are not safe for concurrent use

We will get into the last point later, just an example for a code, which can lead to race-conditions:

```go
package main

var (
   allData = make(map[string]string)
)

func get(key string) string {
    return allData[key]
}

func set(key string, value string) {
    allData[key] = value
}

func main() {
    go set("a", "Some Data 1")
    go set("b", "Some Data 2")
    go get("a")
    go get("b")
    go get("a")
}
```

The correct way here would be to use Mutex of the `sync` package:
```go
package main

import (
    "fmt"
    "sync"
)

var (
    allData = make(map[string]string)
    rwm     sync.RWMutex
)

func get(key string) string {
    rwm.RLock()
    defer rwm.RUnlock()
    return allData[key]

}

func set(key string, value string) {
    rwm.Lock()
    defer rwm.Unlock()
    allData[key] = value

}

func main() {
    set("a", "Some Data")
    result := get("a")
    fmt.Println(result)
}
```

This example can be found on:
https://golangbyexample.com/maps-in-golang/#Functions_on_Maps


### Merging maps

Merging maps is possible, however it has to be done element-by-element. And for these operations it has to be kept in mind, that maps are not safe for cuncurrent access.
```go
func MergeMaps() {
	first := map[string]int{"a": 1, "b": 2, "c": 3}
	second := map[string]int{"a": 1, "e": 5, "c": 3, "d": 4}

	for k, v := range second {
		first[k] = v
	}

	fmt.Println(first)
}
```

### Maps are not threadsafe

```go
var (
   allData = make(map[string]string)
)

func get(key string) string {
    return allData[key]
}

func set(key string, value string) {
    allData[key] = value
}

func main() {
    go set("a", "Some Data 1")
    go set("b", "Some Data 2")
    go get("a")
    go get("b")
    go get("a")
}
```

### Antipatterns:

What is wrong about this?
```go
m2 := map[string]int{}
if _, ok := m2["k"]; ok {
    m2["k"] += 4
} else {
    m2["k"] = 4
}
```
Answer:
unnecessary check, it is better to just do the following, since the map will anyways returns the zero value for a non present entry:
```go
m2 := map[string]int{}
m2["k"] += 4
```
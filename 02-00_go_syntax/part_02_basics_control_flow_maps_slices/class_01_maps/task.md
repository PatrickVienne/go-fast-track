# Task

Make 2 maps, 1 contains the price list of articles on a website

Shoes: $1.54
Carpet: $100
Frypan: $64.50
TV-Set: $520.10
Tomato: $1

The other one is the shopping cart, showing the item name and the number of items bought

2 Shoes
1 Carpet
10 Tomato

Task: Write a script which outputs the total amount to be paid on checkout.

Bonus Task: Make it failsafe, i.e. make checks if the item really is in the price list, and give an output if the item is not.


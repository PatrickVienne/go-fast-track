package main

import "fmt"

func main() {
	// initializing
	prices := map[string]float64{
		"Shoes":  1.54,
		"Carpet": 100,
		"Frypan": 64.50,
		"TV-Set": 520.10,
		"Tomato": 1,
	}

	cart := map[string]uint64{
		"Shoes":  2,
		"Carpet": 1,
		"Tomato": 10,
	}

	totalPrice := 0.0
	for item, number := range cart {
		totalPrice += float64(number) * prices[item]
	}

	fmt.Println("Total Price: ", totalPrice) //Total Price:  113.08
}

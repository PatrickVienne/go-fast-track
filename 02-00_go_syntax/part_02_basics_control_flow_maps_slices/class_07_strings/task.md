# Strings

## Task

Make a function, which can trim a string, and print the sentence, but each word has all its letters reverted

You can ignore punctuations for this task, only mind the word order.


Example call:
```
$ go run main.go  "The quick bròwn 狐 jumped over the lazy 犬"
```

Example output:
```
The quick bròwn 狐 jumped over the lazy 犬
ehT kciuq nwòrb 狐 depmuj revo eht yzal 犬 
```

## Hint

There is many ways to do this, it is totally OK to have a different way than the solution.

The result can be built by either concatanating strings.

concatanating `[]rune` arrays.

Or using a `string.Builder` object.
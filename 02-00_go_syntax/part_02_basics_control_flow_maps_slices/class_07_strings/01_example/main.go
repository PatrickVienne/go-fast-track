package main

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

func changeCase() {
	fmt.Println("==changeCase==")
	text := "Golang 4ever!"
	fmt.Println(`strings.ToUpper(text)`, strings.ToUpper(text))
	fmt.Println(`strings.ToLower(text)`, strings.ToLower(text))

	// for special alphabets, ToUpperSpecial with added special cases can convert correctly to uppercase versions
	fmt.Println(`strings.ToUpper("örnek iş")`, strings.ToUpper("örnek iş"))
	fmt.Println(`strings.ToUpperSpecial(unicode.TurkishCase, "örnek iş")`, strings.ToUpperSpecial(unicode.TurkishCase, "örnek iş"))

}

func searchContent() {
	fmt.Println("==searchContent==")
	text := "Golang 4ever!"
	fmt.Println(`strings.HasPrefix(text, "Golang")`, strings.HasPrefix(text, "Golang"))
	fmt.Println(`strings.HasSuffix(text, "4ever!")`, strings.HasSuffix(text, "4ever!"))
	fmt.Println(`strings.Contains(text, "lang")`, strings.Contains(text, "lang"))
	fmt.Println(`strings.Count(text, "e")`, strings.Count(text, "e"))
	fmt.Println(`strings.Count(text, "E")`, strings.Count(text, "E"))
}

func textLength() {
	fmt.Println("==textLength==")
	myWord := "We♥Go"
	fmt.Println(`fmt.Sprintf("% x", myWord)`, fmt.Sprintf("% x", myWord))
	fmt.Println(`myWord[:3]`, myWord[:3])
	fmt.Println(`string([]rune(myWord)[:3])`, string([]rune(myWord)[:3]))
	fmt.Println(`len(myWord)`, len(myWord))
	fmt.Println(`len([]byte(myWord))`, len([]byte(myWord)))
	fmt.Println(`utf8.RuneCountInString(myWord)`, utf8.RuneCountInString(myWord))
}

func simpleManipulation() {
	fmt.Println("==simpleManipulation==")

	saturnMoons := []string{"Tethys", "Dione", "Rhea", "Iapetus", "Enceladus", "Titan"}
	fmt.Println(`strings.Join(saturnMoons, ",")`, strings.Join(saturnMoons, ","))

	jupiterMoons := "Metis,Adrastea,Amalthea,Thebe,Io,Europa,Ganymede,Callisto"
	fmt.Println(`strings.Split(jupiterMoons, ",")`, strings.Split(jupiterMoons, ","))

	quote := "Thomas Edison: Genius is one percent inspiration and ninety-nine percent perspiration."
	fmt.Println(`strings.Fields(quote)`, strings.Fields(quote))

	fmt.Println(`"ba" + strings.Repeat("na", 2)`, "ba"+strings.Repeat("na", 2))

	f := func(c rune) bool {
		return !unicode.IsLetter(c) && !unicode.IsNumber(c)
	}
	fmt.Printf("Fields of `foo1;bar2,baz3...` are: %q\n", strings.FieldsFunc("  foo1;bar2,baz3...", f))
}

func replace() {
	fmt.Println("==replace==")
	// func NewReplacer(oldnew ...string) *Replacer
	r := strings.NewReplacer("<", "&lt;", ">", "&gt;") // pairs of old string and new string
	fmt.Println(`r.Replace("This is <b>HTML</b>!")`, r.Replace("This is <b>HTML</b>!"))
}

func trim() {
	fmt.Println("==trim==")
	fmt.Println(`strings.Trim("¡¡¡Hola, Gophers!!!", "!¡")`, strings.Trim("¡¡¡Hola, Gophers!!!", "!¡"))
	fmt.Println(`strings.TrimSpace(" \t\n Hello, Gophers \n\t\r\n")`, strings.TrimSpace(" \t\n Hello, Gophers \n\t\r\n"))
}

func substring() {
	fmt.Println("==substring==")
	fmt.Println(`"Japan"[2]`, "Japan"[2])
	fmt.Println(`"Japan"[1:3]`, "Japan"[1:3])
	fmt.Println(`"Japan"[:2]`, "Japan"[:2])
	fmt.Println(`"Japan"[2:]`, "Japan"[2:])
}

func ignoreError(val interface{}, err error) interface{} {
	return val
}

func conversion() {
	fmt.Println("==conversion==")
	fmt.Println(`To integer: strconv.Atoi("42")`, ignoreError(strconv.Atoi("42")))
	fmt.Println(`To string: strconv.Itoa(-42)`, strconv.Itoa(-42))
	fmt.Println(`strconv.FormatInt(32, 2)`, strconv.FormatInt(255, 2))
	fmt.Println(`strconv.FormatInt(255, 16)`, strconv.FormatInt(255, 16))

	fmt.Println(`strings.Map(func(r rune) rune { return r + 1 }, "ab")`, "=>", strings.Map(func(r rune) rune { return r + 1 }, "ab"))

}

func comparisons() {
	fmt.Println("==comparisons==")
	fmt.Println(`"Go" == "Go"`, "Go" == "Go")
	fmt.Println(`"Go" == "go"`, "Go" == "go")
	fmt.Println(`strings.Compare("Go", "Go")`, strings.Compare("Go", "Go"))
	fmt.Println(`strings.Compare("Go", "go")`, strings.Compare("Go", "go"))
	fmt.Println(` "Go">"go"`, "Go" > "go")
	fmt.Println(`strings.EqualFold("Go", "Go")`, strings.EqualFold("Go", "Go"))
	fmt.Println(`strings.EqualFold("Go", "go")`, strings.EqualFold("Go", "go"))
}

func builder() {
	fmt.Println("==builder==")
	var b strings.Builder
	for i := 3; i >= 1; i-- {
		fmt.Fprintf(&b, "%d...", i) // write string to the buffer of b
	}

	// can also directly write to the buffer
	b.WriteString("ignition ") // writes string to the buffer
	b.WriteString("lift off!") // writes string to the buffer
	b.Write([]byte("🚀"))

	fmt.Println(b.String()) // write buffer to string

}

func main() {
	changeCase()
	searchContent()
	textLength()
	simpleManipulation()
	replace()
	trim()
	substring()
	conversion()
	comparisons()
	builder()
}

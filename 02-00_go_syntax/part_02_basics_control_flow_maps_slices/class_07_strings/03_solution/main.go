package main

import (
	"fmt"
	"os"
	"strings"
)

func revertWord(word string) string {
	r := []rune(word)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return string(r)
}

func reverseSentence(text string) string {
	var b strings.Builder
	for _, word := range strings.Fields(text) {
		b.WriteString(revertWord(word) + " ")
	}
	return b.String()
}

func main() {
	result := reverseSentence(strings.TrimSpace(os.Args[1]))
	fmt.Println(os.Args[1])
	fmt.Println(result)
}

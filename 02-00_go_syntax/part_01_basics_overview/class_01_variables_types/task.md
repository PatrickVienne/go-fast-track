# Instructions For Task

- navigate to the exercise folder with a terminal, 
    and initiate a project with 
    ```bash
    go mod init <your project>
    ```

- Create a Go script declares multiple variables: 
    `"name" (string)`, `"age" (int)`, `"isVegan" (bool)`

- Import the `fmt` package and print the variables you created.



# Creating a new project

## Steps

- Step 1: Add variables in **main.go**
    ```go
    var (
        name, location string
        age            int
        points         int = 32
    )
    ```
- Step 2: Adjust the main function, to assign values to the variables and print them with fmt.Println
    ```go
    func main() {
        name = "Spongebob"
        location = "under the sea"
        age = 10
        fmt.Println(name, location, age, points)
    }
    ```
- Step 3: Add the necessary import for the "fmt" package
    ```go
    import (
        "fmt"
    )
    ```
- Step 4: run with the terminal
    ```bash
    go run .
    ```

- Step 5: add global constants to your script after the imports
    ```go
    const Pi = 3.14
    const (
        StatusOK      = 200
        StatusCreated = 201
    )
    ```

- Step 6: you can also add additional constants in the main function
    ```go
    const FixedGreeting string = "Salve"
    ```

## Info about datatypes
Overview of types in golang code:
```
bool
string

Numeric types:

uint        either 32 or 64 bits
int         same size as uint
uintptr     an unsigned integer large enough to store the uninterpreted bits of
            a pointer value
uint8       the set of all unsigned  8-bit integers (0 to 255)
uint16      the set of all unsigned 16-bit integers (0 to 65535)
uint32      the set of all unsigned 32-bit integers (0 to 4294967295)
uint64      the set of all unsigned 64-bit integers (0 to 18446744073709551615)

int8        the set of all signed  8-bit integers (-128 to 127)
int16       the set of all signed 16-bit integers (-32768 to 32767)
int32       the set of all signed 32-bit integers (-2147483648 to 2147483647)
int64       the set of all signed 64-bit integers
            (-9223372036854775808 to 9223372036854775807)

float32     the set of all IEEE-754 32-bit floating-point numbers
float64     the set of all IEEE-754 64-bit floating-point numbers

complex64   the set of all complex numbers with float32 real and imaginary parts
complex128  the set of all complex numbers with float64 real and imaginary parts

byte        alias for uint8
rune        alias for int32 (represents a Unicode code point)
```

(examples: https://hackthedeveloper.com/golang-data-types-operations/)

- Basic Types
    - Integers
        - Signed
            - int
            - int8
            - int16 
            - int32 
            - int64
        - Unsigned
            - uint
            - uint8
            - uint16
            - uint32
            - uint64
            - uintptr
    - Floats
        - float32
        - float64
    - Complex Numbers
        - complex64
        - complex128
    - Byte (byte is used to represent the ASCII character)
    - Rune  (rune in Go is an alias for int32, rune is used to represent all UNICODE, e.g. var rbyte byte := 'a')
    - String
    - Boolean
- Composite Types
    - Collection/Aggregation or Non-Reference Types
        - Arrays
        - Structs
    - Reference Types
        - Slices
        - Maps
        - Channels
        - Pointers
        - Function/Methods
    - Interface
        - Special case of empty Interface
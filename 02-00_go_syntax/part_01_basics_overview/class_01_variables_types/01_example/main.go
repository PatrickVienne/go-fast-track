package main

import (
	"fmt"
)

// declare variables with or without initial values
var (
	name, location string
	age            int
	points         int = 32
	points := 10
)

const E := 10000
const Pi = 3.14
const (
	StatusOK      = 200
	StatusCreated = 201
)

func main() {
	// variables which have no values assigned, are initialized with default values, "" and 0
	fmt.Println(name, location, age, points)

	// assign values with "="
	name = "Spongebob"
	location = "under the sea"
	age = 10

	// declare and assign
	greeting := "Hi"

	// add const example
	const FixedGreeting string = "Salve"
	// this also works:
	// `const FixedGreeting = "Salve"``

	// now see all assigned values
	fmt.Println(greeting, name, location, age, points, Pi, FixedGreeting)

	greeting := "Hi"

	const NoonTime = 12

	name = "Golang"
	println(name)
	println(age)
	println(points)
	println(isVegan)
	println(greeting)
	println(StatusOK)

	fmt.Println(NoonTime)
}

package main

import "fmt"

type Human struct {
	FirstName string
	LastName  string
	birthYear int
}

type Artist struct {
	Human
	Genre string
	Songs int
}

// uses named type, pass by value, copies artist on every call
func (a Artist) newReleaseA() int {
	a.Songs++
	return a.Songs
}

// uses pointer, pass by reference, does not copy artist on every call
func (a *Artist) newReleaseB() int {
	a.Songs++
	return a.Songs
}

func main() {
	// creating an instance
	person := Human{"Billy", "Joel", 1949}
	// alternatively
	// person := Human{FirstName: "Billy", LastName: "Joel", birthYear: 1949}

	fmt.Printf("%#v\n", person)
	fmt.Printf("First Name: %#v\n", person.FirstName)

	// changing a field of the instance
	person.FirstName = "William Martin"
	fmt.Printf("%#v\n", person)
	fmt.Printf("First Name: %#v\n", person.FirstName)

	// create an artist, which inherits from "Human"
	billy := Artist{person, "Rock & Pop", 10}
	fmt.Printf("%#v\n", billy)

	// use a method to update
	fmt.Printf("%s released their %dth song\n", billy.FirstName, billy.newReleaseB())
	fmt.Printf("%s has a total of %d songs\n", billy.FirstName, billy.Songs)

	// or use pointer to change value in place in a method
	me := &billy
	fmt.Printf("%s released their %dth song\n", me.FirstName, me.newReleaseB())
	fmt.Printf("%s has a total of %d songs\n", me.FirstName, me.Songs)

	// what if you use `newReleaseA` ?
}

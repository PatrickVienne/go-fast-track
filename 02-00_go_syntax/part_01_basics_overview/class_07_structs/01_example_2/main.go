package main

import "fmt"

type Human struct {
	FirstName string
	LastName  string
	birthYear int
}

type Artist struct {
	Human
	Genre string
	Songs int
}

// // call-by-value
// func (a Artist) newRelease() Artist {
// 	a.Songs++
// 	return a
// }

// call-by-ref
func (a *Artist) newRelease() {
	a.Songs++
}

func main() {
	person := Human{FirstName: "Billy", LastName: "Joel", birthYear: 1949}
	billy := Artist{person, "Rock&Pop", 10}

	// fmt.Println(person)
	// fmt.Printf("%#v\n", person)
	// fmt.Printf("%#v\n", billy)
	// println()
	// person.FirstName = "William Martin"
	// fmt.Println(person)
	// fmt.Printf("%#v\n", person)
	// fmt.Printf("%#v\n", billy)

	fmt.Printf("Billy has %d songs \n", billy.Songs)
	billy.newRelease()
	fmt.Printf("Billy has %d songs \n", billy.Songs)
}

# Instructions For Task

Make a `Vertex` class and give it 2 methods: `Scale` and `Abs`.
`Vertex` is already defined for you:
```go
type Vertex struct {
    X, Y float64
}
```

Expected outcomes:
- Scaling: Vertex(1, 2) scaled with factor 2 should change the vertex to Vertex(2, 4)
- Abs: should return the length of the vector pointing to x,y of the vertex. (sqrt(x^2 + y^2)).



## hints
- You can use the math package's `math.Sqrt` function to calculate the square root

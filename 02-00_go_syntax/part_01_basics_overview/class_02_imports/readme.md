# Creating a new project

## Steps

- Step 1: import the `fmt` and the `"math/rand"` in **main.go**
    ```go
    import "fmt"
    import "math/rand"
    ```
- Step 2: Print a random variable each time you run the script
    ```go
	// return a random number between [0, 10)
	fmt.Printf("Random number is %d", rand.Intn(10))
    ```
- Step 3: Run it a few times and check the results, Is there something odd?

- Step 4: Add a random seed, initialized by the unix time. 
    ```go
    import "fmt"
    import "math/rand"
    import "time"
    ```

- Step 5: Now we set the seed in the main function:
    ```go
    rand.Seed(time.Now().UnixNano())
    ```

- Step 6: Try it again

## Info about string formatting
Reference: https://golangdocs.com/string-formatting-in-golang

The formatted strings can either be printed directly with `fmt.Printf` or returned as a string with `s1 := fmt.Sprintf("%d", 42)`

- To print the ints we need to use %d.
    ```go
    fmt.Printf("%d", 42)    // 42
    ```
- To print floats we can use %f as shown below.
    ```go
    fmt.Printf("%f", 12.5)    // 12.500000
    ```
- Floats can also use scientific notation like this.
    ```go
    fmt.Printf("%e\n", 23000000.45)    // 2.300000e+07
    fmt.Printf("%E\n", 23000000.45)    // 2.300000E+07
    ```
- Booleans are formatted using the %t formatter.
    ```go
    fmt.Printf("%t", true)    // true
    ```
- The characters can be formatted using the %c formatter.
    ```go
    fmt.Printf("%c", 'a')    // a
    ```
- To format a string we need the %s formatter.
    ```go
    fmt.Printf("%s", "a string")    // a string
    ```
- format the size of string
    ```go
    fmt.Printf("|%8s|", "apple")   // |   apple|
    ```
- To format the binary, we need %b.
    ```go
    fmt.Printf("%b", 12)    // 1100
    ```
- The hex value can simply be formatted using the %x formatter.
    ```go
    fmt.Printf("%x", 123)    // 7b
    ```
- Get the type of value: To print the type of value we use the %T formatter (observe the capital T).
    ```go
    fmt.Printf("%T", 42)    // int
    ```

- About Pointers:
```go
package main
 
import (
    "fmt"
)
 
func main() {
    var p *int        // declare as pointer
    var q int
     
    q = 42
    p = &q  // pointer to q
     
    // prints the memory address
    fmt.Printf("%p", p)    // 0x40e020
}
```
- About structs
```go
package main
 
import (
    "fmt"
)
 
type Tree struct{
    name string
}
 
func main() {
    cherry := Tree{"cherry"}
     
    // value representation of the struct
    fmt.Printf("%v\n", cherry)    // {cherry}
     
    // includes names inside struct
    fmt.Printf("%+v\n", cherry)   // {name:cherry}
     
    // the Go representation of the struct
    fmt.Printf("%#v\n", cherry)   // main.Tree{name:"cherry"}
}
```

## Info about random
Random package of math generates pseudo-random numbers.
This package's outputs might be easily predictable regardless of how it's seeded. For random numbers suitable for security-sensitive work, see the crypto/rand package.

Examples:
https://pkg.go.dev/math/rand

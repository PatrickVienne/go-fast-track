package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// Let's generate a random number

	// By default, the seed is set to 1
	rand.Seed(time.Now().UnixNano())

	// return a random number between [0, 10)
	fmt.Printf("Random number is %d", rand.Intn(10))
}

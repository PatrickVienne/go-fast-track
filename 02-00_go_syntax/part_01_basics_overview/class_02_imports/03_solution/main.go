package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// By default, the seed is set to 1
	rand.Seed(time.Now().UnixNano())

	// return a random number between [0, 10)
	dice1 := rand.Intn(6) + 1
	dice2 := rand.Intn(6) + 1
	fmt.Printf("Dice1: %d, Dice2: %d", dice1, dice2)
}

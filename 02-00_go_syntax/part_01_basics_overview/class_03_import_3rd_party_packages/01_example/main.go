package main

import (
	"fmt"
	"math"

	"github.com/mattetti/goRailsYourself/crypto"
)

func main() {
	fmt.Println(crypto.GenerateRandomKey(1))
	// variables starting with uppercase letters are exported and available
	fmt.Println(math.Pi)
	// variables starting with lowercase letters are not exported and not available
	// this would fail:
	// fmt.Println(math.pi)
}

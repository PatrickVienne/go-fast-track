# Instructions For Task

- Make a script and use a 3rd party library of `github.com/google/go-cmp/cmp` to make following comparison run:
    ```go
    func main() {
        fmt.Println(cmp.Equal(1, 2))
    }
    ```

Detailed info on the solution on:
https://developpaper.com/go-import-third-party-package/

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().Unix())
	quotes := [8]string{
		"Genius is one percent inspiration and ninety-nine percent perspiration.",
		"You can observe a lot just by watching.",
		"A house divided against itself cannot stand.",
		"Difficulties increase the nearer we get to the goal.",
		"Fate is in your hands and no one elses",
		"Be the chief but never the lord.",
		"Nothing happens unless first we dream.",
		"Well begun is half done.",
	}
	selectedQuoteIdx := rand.Intn(len(quotes))
	fmt.Println(quotes[selectedQuoteIdx])
}

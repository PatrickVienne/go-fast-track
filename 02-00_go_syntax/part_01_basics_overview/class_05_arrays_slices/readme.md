# Creating a new project

## Steps

- Step 1: Building on previous knowledge, various examples to create an array, e.g. of strings:
    - declareFirstThenSet
    ```go
    func declareFirstThenSet() {
        var a [2]string
        a[0] = "Hello"
        a[1] = "World"
        // retrieving element by index
        fmt.Println(a[0], a[1])
        fmt.Println(a)
    }
    ```
    - declareFirstThenSet
    ```go
    func declareAndSet() {
        a := [2]string{"hello", "world!"}
        fmt.Printf("%q", a)
    }
    ```
    - declare with implicit length
    ```go
    func declareWithImplicitLength() {
        a := [...]string{"hello", "world!"}
        fmt.Printf("%q", a)
    }
    ```
- Step 2: Print the array
    ```go
    func printArrays() {
        a := [2]string{"hello", "world!"}
        fmt.Println(a)
        // [hello world!]
        fmt.Printf("%s\n", a)
        // [hello world!]
        fmt.Printf("%q\n", a)
        // ["hello" "world!"]
    }
    ```
- Step 3: appending and reassigning slices:
    ```go
    func appendingToSlices() {
        a := make([]string, 0)
        a = append(a, "Hello")
        a = append(a, "World")
        fmt.Printf("%q\n", a)

        a[0] = "Goodbye"
        fmt.Printf("%q\n", a)
    }
    ```
- Step 4: call the functions and run it.

## Info

While arrays have fixed length and cannot grow, slices can be expanded and can grow.

Rob Pike about arrays and slices
https://go.dev/blog/slices

https://gobyexample.com/arrays

https://gobyexample.com/slices

### init functions
https://golangdocs.com/init-function-in-golang

> The init functions are extremely useful due to the fact that they are executed immediately before any other functions from the package and they maintain the order of declaration and execute in the same order. This helps create programs that maintain a steady flow of execution.

```go
package main
 
import (
    "fmt"
)
 
func init() {
    fmt.Println("Runs first")
}
 
func init() {
    fmt.Println("Runs second")
}
 
func main() {
    fmt.Println("Hello, World")
     
    // output:
    // Runs first
    // Runs second
    // Runs last
    // Hello, World
     
}
 
func init() {
    fmt.Println("Runs last")
}
```


### logging with logrus

> log.Fatalf("Failed to send event %s to topic %s with key %d"), you should log the much more discoverable: 
```go
log.WithFields(log.Fields{
  "event": event,
  "topic": topic,
  "key": key,
}).Fatal("Failed to send event")
```

Example: 
```go
package main

import (
  "os"
  log "github.com/sirupsen/logrus"
)

func init() {
  // Log as JSON instead of the default ASCII formatter.
  log.SetFormatter(&log.JSONFormatter{})

  // Output to stdout instead of the default stderr
  // Can be any io.Writer, see below for File example
  log.SetOutput(os.Stdout)

  // Only log the warning severity or above.
  log.SetLevel(log.WarnLevel)
}

func main() {
  log.WithFields(log.Fields{
    "animal": "walrus",
    "size":   10,
  }).Info("A group of walrus emerges from the ocean")

  log.WithFields(log.Fields{
    "omg":    true,
    "number": 122,
  }).Warn("The group's number increased tremendously!")

  log.WithFields(log.Fields{
    "omg":    true,
    "number": 100,
  }).Fatal("The ice breaks!")

  // A common pattern is to re-use fields between logging statements by re-using
  // the logrus.Entry returned from WithFields()
  contextLogger := log.WithFields(log.Fields{
    "common": "this is a common field",
    "other": "I also should be logged always",
  })

  contextLogger.Info("I'll be logged with common and other field")
  contextLogger.Info("Me too")
}
```
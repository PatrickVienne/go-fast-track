# Instructions For Task

- Create an array with the following entries taken from https://type.fit/api/quotes
    ```go
    [
        "Genius is one percent inspiration and ninety-nine percent perspiration.",
        "You can observe a lot just by watching.",
        "A house divided against itself cannot stand.",
        "Difficulties increase the nearer we get to the goal.",
        "Fate is in your hands and no one elses",
        "Be the chief but never the lord.",
        "Nothing happens unless first we dream.",
        "Well begun is half done."
    ]
    ```

- Create a script which picks a random quote from this array and prints it. 

## hint

- Use what you learned about generating a random number with the math package.

- watch out not to get an index error. Choose the range wisely.
package main

import (
	"fmt"
	"os"
	"strconv"
)

func square(x float64) float64 {
	return x * x
}

func main() {
	programName := os.Args[0]
	firstArgument := os.Args[1]
	value, _ := strconv.ParseFloat(firstArgument, 64) // parse 64bit float
	fmt.Printf("programName: %s calculated the square of %s to be: %f", programName, firstArgument, square(value))
}

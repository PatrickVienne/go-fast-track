package main

func main() {
	// let's create some example for passing valuetypes as copies and as references.
	// reference types are always passed as reference.

	// Values types: you should care about pointers
	// Refenerce types: you don't care about pointers

	// value types: int, float, string, bool, struct
	// reference types: slice, map, channel, pointer, function
}

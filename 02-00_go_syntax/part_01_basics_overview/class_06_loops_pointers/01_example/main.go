package main

import "fmt"

// pass-by-value
func texter(planets [3]string) [3]string {
	fmt.Println("==texter==")
	for idx, elem := range planets {
		fmt.Println(idx, elem)
		planets[idx] += fmt.Sprintf(" is the %dth planet", idx+1)
	}
	return planets
}

// pass-by-reference
func modifyText(planets *[3]string) {
	fmt.Println("==modifyText==")
	for idx, elem := range planets {
		fmt.Println(idx, elem)
		planets[idx] += fmt.Sprintf(" is the %dth planet", idx+1)
	}
}

func forLoop() {
	fmt.Println("==forLoop==")
	for idx, elem := range [...]int{10, 20, 30, 40, 50} {
		fmt.Println(idx, elem)
	}
}

func makeSquares(array [10]int) {
	fmt.Println("==makeSquares==")
	for idx, elem := range array {
		array[idx] = elem * elem
	}
}

func makeSquaresInplace(array *[10]int) {
	fmt.Println("==makeSquaresInplace==")
	for idx, elem := range array {
		array[idx] = elem * elem
	}
}

func main() {
	// first for loop example
	forLoop()

	a := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	// pass by value
	// passing the array, passes a full copy of the array
	// so the original array will not be changed
	makeSquares(a)
	fmt.Println(a)

	// pass by reference
	// however if specifically the pointer is passed,
	// then the values of the array are changed
	makeSquaresInplace(&a)
	fmt.Println(a)

	// And some additional examples with strings!
	// planets := [3]string{"Mercury", "Venus", "Earth"}
	// modifyText(&planets)
	// planets = texter(planets)
	// fmt.Println(planets)
}

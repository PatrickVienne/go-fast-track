# Creating a new project

In this lesson we will make use of for loops
```go
for idx, elem := range array {
    fmt.Println(idx, elem)
}
```

## Steps

- Step 1: 
    ```go
    func forLoop() {
        for idx, elem := range [...]int{10, 20, 30, 40, 50} {
            fmt.Println(idx, elem)
        }
    }
    ```
    and call the function with the for loop in the main function:
    ```go
    forLoop()
    ```
    

- Step 2: We check how arrays can be changed by functions. Add a function in **main.go**:
    ```go
    func makeSquares(array [10]int) {
        for idx, elem := range array {
            array[idx] = elem * elem
        }
    }
    ```

- Step 3: declare an array and add a function call in `main` and print the result:
    ```go
	a := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	makeSquares(a)
	fmt.Println(a)
    ```

- Step 4: Run it. What is surprising?

- Step 5: Add a function which takes a pointer as argument
    ```go
    func makeSquaresInplace(array *[10]int) {
        for idx, elem := range array {
            array[idx] = elem * elem
        }
    }
    ```

- Step 6: pass the reference to this new function, and print the result:
    ```go
	makeSquaresInplace(&a)
	fmt.Println(a)
    ```

- Step 7: Run it!

## Info

http://www.golangbootcamp.com/book/basics

By default Go passes arguments by value (copying the arguments), if you want to pass the arguments by reference, you need to pass pointers (or use a structure using reference values like slices (Section 4.2) and maps (Section 4.4).

To get the pointer of a value, use the & symbol in front of the value; to dereference a pointer, use the * symbol.

Important!
Do not confuse arrays (fixed length, cannot append elements) with slices (no fixed length, can append elements)

https://gobyexample.com/arrays

https://gobyexample.com/slices


package loggerinit

import (
	"fmt"

	"go.uber.org/zap"
)

const (
	LOG_PROFILE = "example"
)

var logger *zap.Logger

func GetLogger() *zap.Logger {
	if logger == nil {
		var err error
		logger, err = Logger()
		if err != nil {
			zap.S().Fatal(err)
		}
	}
	return logger
}

func Logger() (logger *zap.Logger, err error) {

	if LOG_PROFILE == "prod" {
		logger, err = zap.NewProduction()
		if err != nil {
			return nil, fmt.Errorf("failed to create production zap logger: %w", err)
		}
	} else if LOG_PROFILE == "dev" {
		logger, err = zap.NewDevelopment()
		if err != nil {
			return nil, fmt.Errorf("failed to create development zap logger: %w", err)
		}
	} else {
		logger = zap.NewExample()
	}

	zap.ReplaceGlobals(logger)
	return
}

package persistence

import "gitlab.com/PatrickVienne/go-advanced/12_webserver_gomigrate/models"

type FakeDB struct {
}

func (f FakeDB) GetBooks() []models.Book {
	return []models.Book{
		{Author: "Stanisław Lem", Title: "The Star Diaries"},
		{Author: "Michael Ende", Title: "Momo"},
		{Author: "Fyodor Dostoevsky", Title: "The Idiot"},
		{Author: "The Invention of Nature: Alexander von Humboldt's New World", Title: "Andrea Wulf"},
	}
}

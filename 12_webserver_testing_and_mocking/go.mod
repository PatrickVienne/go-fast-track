module gitlab.com/PatrickVienne/go-advanced/12_webserver_gomigrate

go 1.21

require (
	github.com/gorilla/mux v1.8.0
	go.uber.org/zap v1.24.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
